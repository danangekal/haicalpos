<?php

//Koneksi database
include('core/cukang.inc.php');
//Library
include('core/core.php');

$query  = "
	SELECT 
		*
	FROM
		company
	WHERE
		c_id = '1'
";
$result = mysql_query($query)or die(mysql_error());
if(mysql_num_rows($result)){
	extract(mysql_fetch_assoc($result));
}

$query  = "
	SELECT  user_foto
	FROM
		user
	WHERE
		user_id = '1'
";
$result = mysql_query($query)or die(mysql_error());
if(mysql_num_rows($result)){
	extract(mysql_fetch_assoc($result));
}
?>

<!doctype html>
<html class="no-js" lang="en-US">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- SITE TITLE -->
     <title><?php echo strtoupper($c_nama)?> POSIS | HOME</title>

    <!-- =========================
       Meta Information
    ============================== -->
    <meta name="description" content="Point Of Sale Information System By danang-ekal">
    <meta name="keywords" content="pos, posis, danang-ekal.com, danang-ekal">
    <meta name="author" content="Danang Eko Alfianto">

    <!-- =========================
       favicon and app touch icon
    ============================== -->
	<link rel="shortcut icon" href="images/favicon.ico" />

    <!-- =========================
       Bootstrap
    ============================== -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
	
    <!-- =========================
       Fonts, typography and icons
    ============================== -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">

    <!-- ***** Custom Stylesheet ***** -->
    <link rel="stylesheet" href="css/main-blue-nutshell.css">

    <!-- ***** Responsive fixes ***** -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- Header scripts -->
    <script src="js/queryloader2.min.js"></script>

    <!-- =========================
       Preloader
    ============================== -->
    <script>
        window.addEventListener('DOMContentLoaded', function() {
            "use strict";
            new QueryLoader2(document.querySelector("body"), {
                barColor: "#3498db",
                backgroundColor: "#111",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });
        var alert_color_success_background = '#3498db';
        var alert_color_error_background = '#CF000F';

    </script>
</head>
<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- =========================
       Fullscreen menu
    ============================== -->
    <div class="mobilenav">
        <ul>
            <li data-rel="#header">
                <span class="nav-label">Home</span>
            </li>
			<li data-rel="#kontak-kami">
                <span class="nav-label">Kontak Kami</span>
            </li>
            <li data-rel="login.php">
                <a href="login.php">
					<span class="nav-label">Login Posis</span>
				</a>
			</li>
        </ul>
    </div>  <!-- *** end Full Screen Menu *** -->

    <!-- *****  hamburger icon ***** -->
    <a href="javascript:void(0)" class="menu-trigger">
       <div class="hamburger">
         <div class="menui top-menu"></div>
         <div class="menui mid-menu"></div>
         <div class="menui bottom-menu"></div>
       </div>
    </a>


    <!-- =========================
       Header
    ============================== -->
    <header id="header">
        <div id="myCarousel" class="carousel slide">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for Slides -->
            <div class="carousel-inner">

                <!-- *****  Logo ***** -->
                <div class="logo-container">
                    <a href="#">
                        <img src="images/logo-header.jpg" height="37" width="197" alt="Logo Header">
                    </a>
                </div>

                <!-- =========================
                   Header item 1
                ============================== -->
                <div class="item active">

                    <!-- Set the first background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('images/slider/slide1.jpg');">
                    </div>
                    <div class="carousel-caption">
                        <h1 class="light mab-none">This is <strong class="bold-text"><?php echo ucwords($c_nama);?></strong></h1>
                        <h1 class="light margin-bottom-medium mat-none">And We Are <strong class="bold-text">Awesome</strong></h1>
                        <p class="light margin-bottom-medium"><?php echo ucwords($c_slogan);?></p>
                        <div class="call-button">
                            <div class="row">
								<div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-12">
                                    <a href="#kontak-kami" class="button pull-right internal-link bold-text light hvr-grow" data-rel="#kontak-kami">Kontak Kami</a>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <a href="login.php" class="button pull-left internal-link bold-text main-bg hvr-grow" data-rel="login.php">Login Posis</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="overlay"></div>
                </div>

                <!-- =========================
                   Header item 2
                ============================== -->
                <div class="item">

                    <!-- Set the second background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('images/slider/slide2.jpg');"></div>
                    <div class="carousel-caption">
                        <h1 class="light mab-none">This is <strong class="bold-text"><?php echo ucwords($c_nama);?></strong></h1>
                        <h1 class="light margin-bottom-medium mat-none">And We Are <strong class="bold-text">Awesome</strong></h1>
                        <p class="light margin-bottom-medium"><?php echo ucwords($c_slogan);?></p>
                        <div class="call-button">
                            <div class="row">
								<div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-12">
                                    <a href="#kontak-kami" class="button pull-right internal-link bold-text light hvr-grow" data-rel="#kontak-kami">Kontak Kami</a>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <a href="login.php" class="button pull-left internal-link bold-text main-bg hvr-grow" data-rel="login.php">Login Posis</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="overlay"></div>
                </div>

                <!-- =========================
                   Header item 3
                ============================== -->
                <div class="item">

                    <!-- Set the third background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('images/slider/slide3.jpg');"></div>
                    <div class="carousel-caption">
                        <h1 class="light mab-none">This is <strong class="bold-text"><?php echo ucwords($c_nama);?></strong></h1>
                        <h1 class="light margin-bottom-medium mat-none">And We Are <strong class="bold-text">Awesome</strong></h1>
                        <p class="light margin-bottom-medium"><?php echo ucwords($c_slogan);?></p>
                        <div class="call-button">
                            <div class="row">
								<div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-12">
                                    <a href="#kontak-kami" class="button pull-right internal-link bold-text light hvr-grow" data-rel="#kontak-kami">Kontak Kami</a>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <a href="login.php" class="button pull-left internal-link bold-text main-bg hvr-grow" data-rel="login.php">Login Posis</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="overlay"></div>
                </div>
				
            </div> <!-- *** end wrapper *** -->

            <!-- Carousel Controls -->
            <a class="left carousel-control hidden-xs" href="#myCarousel" data-slide="prev">
                <span class="icon-prev icon-arrow-left"></span>
            </a>
            <a class="right carousel-control hidden-xs" href="#myCarousel" data-slide="next">
                <span class="icon-next icon-arrow-right"></span>
            </a>
        </div>
    </header> <!-- *** end Header *** -->

	<!-- =========================
       Kontak Kami
    ============================== -->
    <section id="kontak-kami" class="about-us">
        <div class="overlay">
            <div class="container padding-top-large">
                <h2>
                    <strong class="bold-text">Kontak</strong>
                    <span class="light-text main-color">Kami</span>
                </h2>
                <div class="line main-bg"></div>
				<p class="margin-bottom-medium">
				<?php echo ucwords($c_alamat);?><br>
				<?php echo ucwords($c_kontak);?>
				</p>
				
				<!-- =========================
                       Social icons
                    ============================== -->
                    <ul class="social margin-bottom-medium">
                        <li class="facebook hvr-pulse"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="twitter hvr-pulse"><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="g-plus hvr-pulse"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li class="linkedin hvr-pulse"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li class="youtube hvr-pulse"><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li class="instagram hvr-pulse"><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li class="behance hvr-pulse"><a href="#"><i class="fa fa-behance"></i></a></li>
                        <li class="dribbble hvr-pulse"><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    </ul>
                    <p class="copyright">
                        &copy; Copyright 2015 <?php echo ucwords($c_nama);?> By <a href="#danang-info" data-toggle="modal">danang-ekal</a> - All Rights reserved
                    </p>
					
					<div class="modal fade contact-form" id="danang-info" tabindex="-1" role="dialog" aria-labelledby="team-member-2" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<div class="modal-body member-info">
									<div class="row">
										<div class="col-md-5 col-sm-5">
											<figure>
												<img src="images/users/<?php echo $user_foto;?>" class="img-circle img-thumbnail" alt="Danang Eko Alfianto">
											</figure>
										</div>
										<div class="col-md-7 col-sm-7">
											<div class="description">
												<h3><strong class="bold-text">Danang Eko Alfianto</strong></h3>
												<div class="light-text">IT & Web Developer</div>
												<div class="about margin-top-medium">
													<p>
														Aplikasi ini adalah sistem aplikasi penjualan terintegrasi (point of sale integrated system) khusus untuk <?php echo ucwords($c_nama);?> dibuat dan dikembangkan oleh <b>Danang Eko Alfianto.</b><br>
														Apabila menemukan kesalahan aplikasi (bugs) atau membutuhkan perubahan alur sistem aplikasi, silahkan hubungi kontak di bawah ini:<br>
														Website: <a href="http://danang-ekal.com" target="_blank">danang-ekal.com</a><br>
														Email : danangekal@gmail.com<br>
														Hp : 085624503065/089655797756<br>
														Fb : Danang Eko Alfianto<br>
														BBM: 570D21FE
													</p>
												</div>
											</div> <!-- *** end description *** -->
										</div> <!-- *** end col-md-7 *** -->
									</div> <!-- *** end row *** -->
								</div> <!-- *** end modal-body *** -->
							</div> <!-- *** end modal-content *** -->
						</div> <!-- *** end modal-dialog *** -->
					</div> <!-- *** end Contact Form modal *** -->
            </div>
        </div>
    </section> <!-- *** end Kontak Kami *** -->

    <!-- =========================
       Back to top button
    ============================== -->
    <div class="back-to-top" data-rel="header">
        <i class="fa fa-chevron-up"></i>
    </div>

    <!-- =========================
     JavaScripts
    ============================== -->
    <!-- jQuery 2.1.4 -->
	<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
	<!-- Main -->
    <script src="js/main.js"></script>
</body>
</html>