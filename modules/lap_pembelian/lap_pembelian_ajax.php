<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$sdate			= $_POST['sdate'];
	$edate			= $_POST['edate'];
	$kategori_id	= $_POST['kategori'];
	
	if ($kategori_id<=0){
		$query="
		SELECT  
			pbd_p_k_id
		FROM    
			pembelian,		
			pembelian_detail 
		WHERE
			pembelian.pb_kode 	= pembelian_detail.pbd_pb_kode
			&& pb_tanggal	>= '$sdate'
			&& pb_tanggal	<= '$edate'
			&& pb_sisa_bayar = 0
		GROUP BY 
			pbd_p_k_id
		";
		$result = mysql_query($query) or die(mysql_error());
	} else {
		$query="
		SELECT  
			pbd_p_k_id,
			pbd_p_kode,
            pbd_p_nama,
            pbd_p_harga_beli,
			pbd_p_satuan,
            SUM(pbd_beli) as beli,
            SUM(pbd_jumlah) as jumlah
		FROM    
			pembelian,		
			pembelian_detail 
		WHERE
			pembelian.pb_kode 	= pembelian_detail.pbd_pb_kode
			&& pb_tanggal	>= '$sdate'
			&& pb_tanggal	<= '$edate'
			&& pbd_p_k_id	= '$kategori_id'
			&& pb_sisa_bayar = 0
		GROUP BY 
			pbd_p_kode
		";
		$result = mysql_query($query) or die(mysql_error());
	}
	$content = '';
    if(mysql_num_rows($result)>0){
		if ($kategori_id<=0) {
			$que="
			SELECT  
				SUM(pb_jumlah) as jtotal_all,
				SUM(pb_diskon) as jtdiskon,
				SUM(pb_ppn) as jtppn,
				SUM(pb_total) as gtotal
			FROM    
				pembelian 
			WHERE
				pb_tanggal		>= '$sdate'
				&& pb_tanggal	<= '$edate'
				&& pb_sisa_bayar = 0
			";
			$resu = mysql_query($que) or die(mysql_error());
			$rw = mysql_fetch_assoc($resu);
			extract($rw);
			
			while($row = mysql_fetch_assoc($result)){
				extract($row);
				$content .= '
				<h5>Tanggal Pembelian '.showdt($sdate, 2).' - '.showdt($edate, 2).' ('.get_category($pbd_p_k_id).')</h5>
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr class="success">
						<th>No</th>
						<th>Kode</th>
						<th>Nama</th>
						<th>Beli</th>
						<th>Harga Beli</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody>
				';
				$q="
				SELECT  
					pbd_p_kode,
					pbd_p_nama,
					pbd_p_harga_beli,
					pbd_p_satuan,
					SUM(pbd_beli) as beli,
					SUM(pbd_jumlah) as jumlah
				FROM    
					pembelian,		
					pembelian_detail 
				WHERE
					pembelian.pb_kode 	= pembelian_detail.pbd_pb_kode
					&& pb_tanggal	>= '$sdate'
					&& pb_tanggal	<= '$edate'
					&& pbd_p_k_id	= '$pbd_p_k_id'
					&& pb_sisa_bayar = 0
				GROUP BY 
					pbd_p_kode
				";
				$res		= mysql_query($q) or die(mysql_error());
				$no			= 0;
				$jtotal		= 0;
				while($r = mysql_fetch_assoc($res)){
					extract($r);
					$no++;
					$content	.='
								<tr>
									<td>'.$no.'</td>
									<td><b>'.$pbd_p_kode.'</b></td>
									<td>'.ucwords($pbd_p_nama).'</td>
									<td>'.$beli.' / '.$pbd_p_satuan.'</td>
									<td>'.rupiah($pbd_p_harga_beli).'</td>
									<td>'.rupiah($jumlah).'</td>
								</tr>
								'; 
				$jtotal	+= $jumlah;
				}			
				
				$content	.= '
							<tr>
								<td colspan="4"></td>
								<td class="success"><b>Jumlah Pengeluaran Kotor</b></td>
								<td class="success"><b>'.rupiah($jtotal).'</b></td>
							</tr>
							</tbody>
							</table>
							</div>
							';
			}
			$omtppn		= $jtotal_all-$jtdiskon;
			$content	.= '
						<div class="table-responsive">
						<table class="table table-bordered">
						<tr class="success">
							<td ><b>Total Pengeluaran Kotor</b></td>
							<td><b>'.rupiah($jtotal_all).'</b></td>
						</tr>	
						<tr class="success">
							<td ><b>Total Pemotongan Diskon</b></td>
							<td><b>'.rupiah($jtdiskon).'</b></td>
						</tr>						
						<tr class="success">
							<td ><b>Total Pengeluaran Bersih (Total Pengeluaran Kotor - Total Pemotongan Diskon)</b></td>
							<td><b>'.rupiah($omtppn).'</b></td>
						</tr>
						<tr class="success">
							<td ><b>Total Tambahan PPn</b></td>
							<td><b>'.rupiah($jtppn).'</b></td>
						</tr>
						<tr class="success">
							<td ><b>Total Pengeluaran Keseluruhan (Total Pengeluaran Bersih + Total Tambahan PPn)</b></td>
							<td><b>'.rupiah($gtotal).'</b></td>
						</tr>
						</table>
						</div>
						<br>
						<br>
						<a data-toggle="tooltip" title="Export Laporan Pembelian (PDF)" class="btn btn-danger" href="page.php?p='.paramEncrypt('lap_pembelian_pdf').'&sdt='.paramEncrypt($sdate).'&edt='.paramEncrypt($edate).'&sid='.paramEncrypt($kategori_id).'" target="_blank" role="button"><i class="fa fa-download"></i> PDF</a>
						';
		} else {
			$content .= '
					<h5>Tanggal Pembelian '.showdt($sdate, 2).' - '.showdt($edate, 2).' '.get_category($kategori_id).'</h5>
					<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr class="success">
							<th>No</th>
							<th>Kode</th>
							<th>Nama</th>
							<th>Beli</th>
							<th>Harga Beli</th>
							<th>Jumlah</th>
					</thead>
					<tbody>
					';
				$no=0;
				$jtotal = 0;
				while($row = mysql_fetch_assoc($result)){
					extract($row);
					$no++;
					$content	.='
								<tr>
									<td>'.$no.'</td>
									<td><b>'.$pbd_p_kode.'</b></td>
									<td>'.ucwords($pbd_p_nama).'</td>
									<td>'.$beli.' / '.$pbd_p_satuan.'</td>
									<td>'.rupiah($pbd_p_harga_beli).'</td>
									<td>'.rupiah($jumlah).'</td>
								</tr>
								'; 
				$jtotal += $jumlah;
				}
				$content	.= '
							<tr>
								<td colspan="4"></td>
								<td class="success"><b>Jumlah Pengeluaran Kotor </b></td>
								<td class="success"><b>'.rupiah($jtotal).'</b></td>
							</tr>
							</tbody>
							</table>
							</div>
							<br>
							<br>
							<a data-toggle="tooltip" title="Export Laporan Pembelian (PDF)" class="btn btn-danger" href="page.php?p='.paramEncrypt('lap_pembelian_pdf').'&sdt='.paramEncrypt($sdate).'&edt='.paramEncrypt($edate).'&sid='.paramEncrypt($kategori_id).'" target="_blank" role="button"><i class="fa fa-download"></i> PDF</a>
							';
		}
		
    } else {
		$content    .= '
				<h5>Tanggal Pembelian '.showdt($sdate, 2).' - '.showdt($edate, 2).' '.get_category($kategori_id).'</h5>
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
				<tbody>
				<tr><td colspan="6" class="text-center"> -- Not Found Content --</td></tr>
				</tbody>
				</table>
				</div>
				';
	}
    echo $content;
?>