<?php
defined('_IEXEC')or die('');

if(!isset($_REQUEST['id'])){
	$title 			= 'New';
	
	//user data
	$id 			= '';
	$user_name		= '';
	$user_password	= '';
	$user_fullname	= '';
	$user_foto		= '';
	$user_salt		= '';
	$user_aktif		= '';
	
	//jabatan
	$utj_j_id		= '';
}else{
	$title 		= 'Edit';
	$id 		= paramDecrypt($_REQUEST['id']);
	$query  = "
		SELECT  *
		FROM
			user
		INNER JOIN user_to_jabatan ON user_id = utj_user_id	
		WHERE
			user_id = '$id'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
	}
}
?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $user_fullname;?></h3>
	</div><!-- /.box-header -->

	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-8">
			<div class="box box-info">
			<div class="box-body">
				<input type="hidden" id="id" name="id" value="<?php echo $id; ?>"/>
				<div class="form-group">
					<label class="col-sm-2 control-label">User Name</label>
					<div class="col-sm-3">
						<input type="text" id="username" name="username" placeholder="User Name" class="form-control input-sm" required="true" value="<?php echo $user_name;?>" <?php if ($id>0){ ?> readonly <?php } else {?> autofocus <?php } ?>/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Password</label>
					<input type="hidden" id="salt" name="salt" value="<?php echo $user_salt; ?>"/>
					<div class="col-sm-3">
						<input type="password" id="password" name="password" placeholder="Password" class="form-control input-sm" <?php if ($id>0){ ?> autofocus <?php } ?>/>
					</div>
					<?php if ($id>0) { ?>
					<label class="col-sm-6 control-label">* Kosongkan password bila tidak diubah</label>
					<?php } ?>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Full Name</label>
					<div class="col-sm-4">
						<input type="text" id="fullname" name="fullname" placeholder="Full Name" class="form-control input-sm" required="true" value="<?php echo $user_fullname;?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Jabatan</label>
					<div class="col-sm-4">
						<select class="form-control input-sm" id="jabatan" name="jabatan" required="true">	
						<?php opt_jabatan($jabatan, $utj_j_id);?>
						</select>	
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Aktif</label>
					<div class="col-sm-3">
						<select class="form-control input-sm" id="aktif" name="aktif" required="true">	
						<?php opt_aktif($user_aktif);?>
						</select>	
					</div>
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
				</div> 
			</div> <!-- End box body isi-->
			</div> <!-- End box isi-->
			</div> <!-- End col-md-8 isi-->
		<?php if ($id>0){ ?>
		<div class="col-md-3">
			<center><a class="btn btn-default btn-lg" href="javascript:del(<?php echo $user_id;?>)" role="button">Delete My Account</a></center>
			<img id="previewing" src="images/users/<?php echo $user_foto; ?>" class="img-circle img-thumbnail"/>
			<center><a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('user_change_foto');?>&id=<?php echo paramEncrypt($id);?>" role="button">Change Foto Profile</a></center>
		</div>
		<?php } ?>
		</div> <!-- End box body utama-->
	</form>
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('user');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div>
</div><!-- End box-->

<script>
// Main Function		
$(function(){ 
	$("#btn-submit").click(function(){
		var id	    	= $("#id").val(),
			username   	= $("#username").val(),
			password    = $("#password").val(),
			salt	    = $("#salt").val(),
			fullname   	= $("#fullname").val(),
			jabatan   	= $("#jabatan").val(),
			aktif		= $("#aktif").val();
		
		// validasi
		if(!id && !username){
		   window.alert('Ooops!\nPassword harus diisi');
		   $("#password").focus();
		   return;
		}
		
		if(!username){
		   window.alert('Ooops!\nUser Name harus diisi');
		   $("#username").focus();
		   return;
		}
		
		if(!fullname){
		   window.alert('Ooops!\nFull Name harus diisi');
		   $("#fullname").focus();
		   return;
		}
		
		if(jabatan=="" || !jabatan || jabatan==0){
		   window.alert('Ooops!\nJabatan harus dipilih');
		   $("#jabatan").focus();
		   return;
		}	
		
		if(aktif=="" || !aktif){
		   window.alert('Ooops!\nAktif harus dipilih');
		   $("#aktif").focus();
		   return;
		}	
			
		var query   =   'type=save'+
        				'&id='+id+
						'&username='+username+
						'&password='+password+
						'&salt='+salt+
						'&fullname='+fullname+
						'&jabatan='+jabatan+
						'&foto=noimage.jpg'+
						'&aktif='+aktif;
		
		$.ajax({
			url     : 'modules/user_form/user_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Berhasil Disimpan");
               	} else {
               		window.alert("Data Berhasil Diperbaharui");
				}      
				window.location = '?p=<?php echo paramEncrypt('user');?>';
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		});

	})
})//Main Function

</script>