<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$sdate		= $_POST['sdate'];
	$edate		= $_POST['edate'];
	
	$query="
	SELECT  
		*
	FROM    
		keuangan 
	WHERE
		keu_tanggal		>= '$sdate'
		&& keu_tanggal	<= '$edate'
	ORDER BY 
		keu_tanggal
	ASC	
	";
	$result = mysql_query($query) or die(mysql_error());
	
	$content = '';
    if(mysql_num_rows($result)>0){			
		$content .= '
		<h5>Periode Keuangan '.showdt($sdate, 2).' - '.showdt($edate, 2).'</h5>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr class="success">
				<th>#</th>
				<th>Tanggal</th>
				<th>Kode</th>				
				<th>Transaksi</th>
				<th>Debet</th>
				<th>Kredit</th>					
				<th>Saldo</th>
				<th>Keterangan</th>
				<th>Input By</th>
			</tr>
		</thead>
		<tbody>
		';
		
		$no			= 0;
		while($r = mysql_fetch_assoc($result)){
			extract($r);
			$no++;
			$content	.='
						<tr>
							<td>'.$no.'</td>
							<td>'.showdt($keu_tanggal, 2).'</td>
							<td><b>'.$keu_kode.'</b></td>
							<td>'.$keu_transaksi.'</td>
							<td>'.rupiah($keu_mutasi_debet).'</td>
							<td>'.rupiah($keu_mutasi_kredit).'</td>
							<td>'.rupiah($keu_saldo).'</td>
							<td>'.$keu_keterangan.'</td>
							<td>'.get_fullname($keu_user_id).'</td>
						</tr>
						'; 	
		}			
		
		$content	.= '
		</tbody>
		</table>
		</div>
		<br>
		<br>
		<a data-toggle="tooltip" title="Export Laporan Keuangan (PDF)" class="btn btn-danger" href="page.php?p='.paramEncrypt('lap_keuangan_pdf').'&sdt='.paramEncrypt($sdate).'&edt='.paramEncrypt($edate).'" target="_blank" role="button"><i class="fa fa-download"></i> PDF</a>	
		';
		
    } else {
		$content    .= '
				<h5>Periode Keuangan '.showdt($sdate, 2).' - '.showdt($edate, 2).'</h5>
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
				<tbody>
				<tr><td colspan="9" class="text-center"> -- Not Found Content --</td></tr>
				</tbody>
				</table>
				</div>
				';
	}
    echo $content;
?>