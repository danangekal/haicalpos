<?php
defined('_IEXEC')or die('');

$title 		= 'Edit';
if(!isset($_REQUEST['id'])){
	$id 			= '';

}else{
	$id 		= paramDecrypt($_REQUEST['id']);
	$query  = "
		SELECT  *
		FROM
			user
		WHERE
			user_id = '$id'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
	}
}

?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $user_fullname;?></h3>
	</div><!-- /.box-header -->
	
	<div class="box-body">
		<form class="form-horizontal" id="uploadimage" action="profile_change_foto_ajax.php" method="post" enctype="multipart/form-data">
			<div class="col-md-6">
			<div class="box box-info">
			<div class="box-body">	
				<input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
				<input type="hidden" id="nfoto" name="nfoto" value="<?php echo $user_foto; ?>" />
				<div class="form-group" id="image_preview">
					<img id="previewing" src="images/users/<?php echo $user_foto; ?>" class="img-circle img-thumbnail"/>
				</div>
				<div class="form-group">
					<input type="file" name="file" id="file" required />
					<input type="submit" value="Upload" class="submit" />
				</div>
				<div class="form-group" id="message">
				
				</div>
			</div> <!-- End box body isi-->
			</div> <!-- End box isi-->
			</div> <!-- End col-md-6 isi-->
		</form>
	</div><!-- End box body utama-->
	
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('profile_form');?>&id=<?php echo paramEncrypt($id);?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div>		
</div><!-- End box utama-->

<script>
// Main Function		
//$(function(e){ 
$(document).ready(function (e) {
	var id	   	= $("#id").val(),
		nfoto  	= $("#nfoto").val();
	var query	=	'id='+id+
					'&nfoto='+nfoto;
	
	
	$("#uploadimage").on('submit',(function(e) {
	e.preventDefault();
	$("#message").empty();
	$.ajax({
		url: "modules/profile_change_foto/profile_change_foto_ajax.php", // Url to which the request is send
		type: "POST",             // Type of request to be send, called as method
		data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data) {   // A function to be called if request succeeds
			$("#message").html(data);
			window.location = '?p=<?php echo paramEncrypt('logout');?>';
			window.alert('Silahkan Log In Kembali untuk melihat perubahan Foto Profile');
		}
	});
}));

// Function to preview image after validation
$(function() {
	$("#file").change(function() {
	$("#message").empty(); // To remove the previous error message
	var file = this.files[0];
	var imagefile = file.type;
	var match= ["image/jpeg","image/jpg"];
	if(!((imagefile==match[0]) || (imagefile==match[1])))
	{
	$('#previewing').attr('src','images/users/noimage.jpg');
	$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg and jpg Images type allowed</span>");
	return false;
	}
	else
	{
	var reader = new FileReader();
	reader.onload = imageIsLoaded;
	reader.readAsDataURL(this.files[0]);
	}
	});
});

function imageIsLoaded(e) {
	$("#file").css("color","green");
	$('#image_preview').css("display", "block");
	$('#previewing').attr('src', e.target.result);
	$('#previewing').attr('width', '230px');
	$('#previewing').attr('height', '230px');
	};
});

</script>