<?php
defined('_IEXEC')or die('');

if(!isset($_REQUEST['kd'])){
	$title 			= 'New';
	//data sale
	$pg_kode		= gen_pgid();
	$pg_tanggal		= '';
	$pg_nama		= '';
	$pg_jenis		= '';
	$pg_jumlah		= '';
	$pg_keterangan	= '';
	
	//data sale detail
	
	
}else{
	$title 		= 'Edit';
	$pg_kode	= paramDecrypt($_REQUEST['kd']);
	$query  = "
		SELECT  
			*
		FROM
			pengeluaran
		WHERE
			pg_kode = '$pg_kode'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
		
	}
}
?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $pg_kode;?></h3>
	</div><!-- /.box-header -->

	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-10">
			<div class="box box-info">
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-4 control-label">Kode Pengeluaran</label>
					<div class="col-sm-3">
						<input type="text" id="kode" name="kode" class="form-control input-sm" value="<?php echo $pg_kode;?>" readonly />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Penanggung Jawab/Nama Pengeluar</label>
					<div class="col-sm-4">
						<input type="text" id="nama" name="nama" placeholder="Penanggung Jawab/Nama Pengeluar" class="form-control input-sm" required="true" value="<?php echo $pg_nama;?>" autofocus/>						
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Jenis/Nama Pengeluaran</label>
					<div class="col-sm-6">
						<input type="text" id="jenis" name="jenis" placeholder="Jenis/Nama Pengeluaran" class="form-control input-sm" required="true" value="<?php echo $pg_jenis;?>" />						
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Jumlah Pengeluaran</label>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="jumlah" name="jumlah" placeholder="0" class="form-control input-sm number-mask" required="true" value="<?php echo $pg_jumlah;?>" />
						</div>
					</div>
				</div>    
				<div class="form-group">
					<label class="col-sm-4 control-label">Keterangan Pengeluaran</label>
					<div class="col-sm-6">
						<input type="text" id="keterangan" name="keterangan" placeholder="Keterangan Pengeluaran" class="form-control input-sm" required="true" value="<?php echo $pg_keterangan;?>" />						
					</div>
				</div>

				<div class="panel-footer">
					<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
				</div> 
			</div>
			</div>
			</div>	
		</div>
	</form>
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('pengeluaran');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div>
</div><!-- End Box-->

<script>
// Main Function		
$(function(){ 
	var userid  	= <?php echo $userid;?>;
	
	function set_mask(){
        $(".number-mask").inputmask(
            "decimal",
            {
                radixPoint: ".",
                autoGroup: true,
                groupSeparator: ",",
                groupSize:3,
                rightAlignNumerics: true,
                "oncleared":function(){
                    $(this).val('0');
                }
            }
        );
    }
	
	//initial
    set_mask();
	
	$("#btn-submit").click(function(){
		var kode    	= $("#kode").val(),
			nama    	= $("#nama").val(),
			jenis	    = $("#jenis").val(),
			jumlah		= $("#jumlah").val(),
			keterangan	= $("#keterangan").val();
		
		// validasi
		if(!nama){
		   window.alert('Ooops!\nPenanggung Jawab/Nama Pengeluar harus diisi');
		   $("#nama").focus();
		   return;
		}
		
		if(!jenis){
		   window.alert('Ooops!\nJenis Pengeluaran harus diisi');
		   $("#jenis").focus();
		   return;
		}

		if(!jumlah){
		   window.alert('Ooops!\nJumlah Pengeluaran harus diisi');
		   $("#jumlah").focus();
		   return;
		}
		
			
		var query   =   'type=save'+
						'&userid='+userid+
						'&kode='+kode+
						'&nama='+nama+
						'&jenis='+jenis+
						'&jumlah='+jumlah+
						'&keterangan='+keterangan;
		$.ajax({
			url     : 'modules/pengeluaran_form/pengeluaran_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {      
				//window.alert("Data Berhasil Disimpan");
				window.location = '?p=<?php echo paramEncrypt('pengeluaran');?>';
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		});

	})
})//ready

</script>