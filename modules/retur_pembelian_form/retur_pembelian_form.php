<?php
defined('_IEXEC')or die('');

	//data penjualan
	$pj_kode		= paramDecrypt($_REQUEST['kdpj']);
	
	$query  = "
		SELECT  
			*
		FROM
			penjualan
		WHERE
			pj_kode = '$pj_kode'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));		
	}

if(!isset($_REQUEST['kdrpj']) && !isset($_REQUEST['pjdid'])){
	$title 			= 'New';
	
	//data retur penjualan	
	$rpj_kode		= gen_rjid();
	$rpj_jumlah		= 0;
	$rpj_potongan	= 0;
	$rpj_total		= 0;
	
	//data item retur penjualan detail
	$idp			= '';
	$kategori		= '';
	$kodep			= '';
	$namap			= '';
	$harga_jual		= '';
	$harga_beli		= '';
	$satuan			= '';
	$beli			= '';
	$retur			= '';
	$jumlah			= '';
	
	$stok			= '';
	
}else{
	$title 		= 'Edit';
	$pjd_id		= paramDecrypt($_REQUEST['pjdid']);
	$rpj_kode	= paramDecrypt($_REQUEST['kdrpj']);
		
	$q  = "
		SELECT  
			*
		FROM
			retur_penjualan
		WHERE
			rpj_kode = '$rpj_kode'
	";
	$r = mysql_query($q)or die(mysql_error());
	if(mysql_num_rows($r)){
		extract(mysql_fetch_assoc($r));		
	} else {
		//data retur penjualan	
		$rpj_kode		= gen_rjid();
		$rpj_jumlah		= 0;
		$rpj_potongan	= 0;
		$rpj_total		= 0;
	}
	
	//data item penjualan detail
	if($pjd_id > 0){
		$query2  = "
			SELECT  
				pjd_id as idp, 
				pjd_p_k_id as kategori,
				pjd_p_kode as kodep, 
				pjd_p_nama as namap, 
				pjd_p_harga_jual as harga_jual,
				pjd_p_harga_beli as harga_beli,
				pjd_p_satuan as satuan,
				pjd_beli as beli,
				pjd_beli as retur,
				pjd_jumlah as jumlah,
				p_stok as stok
			FROM
				penjualan_detail
			INNER JOIN
				produk
			ON
				pjd_p_kode = p_kode
			WHERE
				pjd_id = '$pjd_id'
		";
		$result2 = mysql_query($query2)or die(mysql_error());
		if(mysql_num_rows($result2)){
			extract(mysql_fetch_assoc($result2));
		}
	} else {
		$idp			= '';
		$kategori		= '';
		$kodep			= '';
		$namap			= '';
		$harga_jual		= '';
		$harga_beli		= '';
		$satuan			= '';
		$beli			= '';
		$retur			= '';
		$jumlah			= '';
		
		$stok			= '';
	}	
}
?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $rpj_kode;?></h3>
	</div><!-- /.box-header utama -->
	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-8"><!-- div col md 8 data penjualan -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Data Retur Penjualan</h3>
			</div><!-- /.box-header data penjualan -->
			<div class="box-body">	
				<div class="form-group">
					<label class="col-sm-3 control-label">Kode Retur Penjualan</label>
					<div class="col-sm-4">
						<input type="text" id="koderetur" name="koderetur" class="form-control input-sm" value="<?php echo $rpj_kode;?>" readonly />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Kode Penjualan</label>
					<div class="col-sm-4">
						<input type="text" id="kodejual" name="kodejual" class="form-control input-sm" value="<?php echo $pj_kode;?>" readonly />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Customer</label>
					<div class="col-sm-4">
						<input type="text" id="customer" name="customer" class="form-control input-sm" placeholder="Nama Customer" value="<?php echo $pj_customer;?>" readonly />
					</div>
				</div>
			</div><!-- end div box-body data penjualan-->
			</div><!-- end div box data penjualan -->
			</div><!-- end div col md 8 data penjualan -->
			
			<div class="col-md-12"><!-- div col md 12 item penjualan -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Item Retur Penjualan Product</h3>
			</div><!-- /.box-header item penjualan -->
			<div class="box-body">
				<div class="form-group">
					<div class="col-sm-14">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" style="margin-bottom:2px;">
							<thead>
								<tr class="success">
								<th></th>
								<th></th>
								<th>Kode Product</th>
								<th>Nama Product</th>
								<th>Harga Product/Satuan</th>
								<th></th>
								<th></th>
								<th>Beli Product</th>
								<th>Retur Product</th>
								<th>Jumlah Harga</th>
								<th></th>
								</tr>
								<tr>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="hidden" id="pjd_id" name="pjd_id" value="<?php echo $idp;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="hidden" id="kategori" name="kategori" value="<?php echo $kategori;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="text" id="kodeprod" name="kodeprod" value="<?php echo $kodep;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="text" id="nama" name="nama" value="<?php echo $namap;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><span class="input-group-addon">Rp</span><input class="form-control input-sm number-mask" type="text" id="hargajual" name="hargajual" value="<?php echo $harga_jual;?>" readonly /><span class="input-group-addon">/</span><span class="input-group-addon" id="satuan" name="satuan"><?php echo $satuan;?></span></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="hidden" id="hargabeli" name="hargabeli" value="<?php echo $harga_beli;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="hidden" id="stok" name="stok" value="<?php echo $stok;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="text" id="beli" name="beli" value="<?php echo $beli;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="text" id="retur" name="retur" onclick="clearInput(this)" value="<?php echo $retur;?>" /></div></th>
								<th><div class="input-group input-group-sm"><span class="input-group-addon">Rp</span><input class="form-control input-sm number-mask" type="text" id="jumlah" name="jumlah" value="<?php echo $jumlah;?>" readonly /></div></th>
								<th>
									<?php 
									if($idp>0){
										echo '
											<button type="button" class="btn btn-success btn-xs" id="btn-add" name="btn-add">Update</button>
											<button type="button" class="btn btn-danger btn-xs" id="btn-can" name="btn-can">Cancel</button>
										';
									}
									?>
								</th>     
								<tr>
							</thead>
							<tbody id="item-returpenjualan">
							</tbody>
						</table>
					</div>	
					</div>
				</div>
			</div><!-- end div box body item penjualan -->
			</div><!-- end div box item penjualan -->
			</div><!-- end div col md 12 item penjualan -->
			
			<div class="col-md-12"><!-- div col md 12 daftar penjualan -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Daftar Penjualan Product</h3>
			</div><!-- /.box-header item penjualan -->
			<div class="box-body">
				<div class="form-group">
					<div class="col-sm-14">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" style="margin-bottom:2px;">
								<tr class="success">
								<th>No</th>
								<th>Kode Product</th>
								<th>Nama Product</th>
								<th>Harga Product/Satuan</th>
								<th>Beli Product</th>
								<th>Jumlah</th>
								<th>...</th>
								</tr>
							<tbody>
								<?php
									//data penjualan detail
									$query4  = "
										SELECT  
											*
										FROM
											penjualan_detail
										WHERE
											pjd_pj_kode = '$pj_kode'
									";
									$result4 = mysql_query($query4)or die(mysql_error());
	
									if(mysql_num_rows($result4)){
										$no = 0;
										while($r = mysql_fetch_array($result4)){
											extract($r);
											$no++;
											echo '
												<tr>
													<td>'.$no.'</td>
													<td>'.$pjd_p_kode.'</td>
													<td>'.$pjd_p_nama.'</td>
													<td>'.rupiah($pjd_p_harga_jual).' / '.$pjd_p_satuan.'</td>
													<td>'.$pjd_beli.'</td>
													<td>'.rupiah($pjd_jumlah).'</td>
											';
											
											//cek ada di retur penjualan tidak
											$query5  = "
												SELECT  
													*
												FROM
													retur_penjualan_detail
												WHERE
													rpjd_pjd_id = '$pjd_id'
											";
											$result5 = mysql_query($query5)or die(mysql_error());
			
											if(!mysql_num_rows($result5)){
												echo '	
														<td>
															<a data-toggle="tooltip" title="Edit Retur Penjualan" class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit Item Penjualan" href="?p='.paramEncrypt('retur_penjualan_form').'&kdpj='.paramEncrypt($pj_kode).'&kdrpj='.paramEncrypt($rpj_kode).'&pjdid='.paramEncrypt($pjd_id).'"><i class="fa fa-edit"></i></a>
														</td>
													</tr>
												';
											} else {
												echo '	
														<td>
															...
														</td>
													</tr>
												';
											}											
										}
									} else {
										echo '<tr><td colspan="7" class="text-center">Data Kosong</td></tr>';
									}									
								?>
							</tbody>							
						</table>
					</div>	
					</div>
				</div>
			</div><!-- end div box body daftar penjualan -->
			</div><!-- end div box daftar penjualan -->
			</div><!-- end div col md 12 daftar penjualan -->
			
			<div class="col-md-12"><!-- div col md 12 daftar retur penjualan -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Daftar Retur Penjualan Product</h3>
			</div><!-- /.box-header item penjualan -->
			<div class="box-body">
				<div class="form-group">
					<div class="col-sm-14">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" style="margin-bottom:2px;">
								<tr class="success">
								<th>No</th>
								<th>Kode Product</th>
								<th>Nama Product</th>
								<th>Harga Product/Satuan</th>
								<th>Beli Product</th>
								<th>Retur Product</th>
								<th>Jumlah</th>
								<th>...</th>
								</tr>
							<tbody>
								<?php
									//data penjualan detail
									$query6  = "
										SELECT  
											*
										FROM
											retur_penjualan_detail
										WHERE
											rpjd_rpj_kode = '$rpj_kode'
									";
									$result6 = mysql_query($query6)or die(mysql_error());
	
									if(mysql_num_rows($result6)){
										$no = 0;
										while($r = mysql_fetch_array($result6)){
											extract($r);
											$no++;
											echo '
												<tr>
													<td>'.$no.'</td>
													<td>'.$rpjd_p_kode.'</td>
													<td>'.$rpjd_p_nama.'</td>
													<td>'.rupiah($rpjd_p_harga_jual).' / '.$rpjd_p_satuan.'</td>
													<td>'.$rpjd_beli.'</td>
													<td>'.$rpjd_retur.'</td>
													<td>'.rupiah($rpjd_jumlah).'</td>
													<td>
														<a data-toggle="tooltip" title="Delete Retur Penjualan" class="btn btn-xs btn-danger" href="javascript:RetDel('.$rpjd_id.')" role="button"><i class="fa fa-trash-o"></i></a>
													</td>
												</tr>
											';
										}
									} else {
										echo '<tr><td colspan="7" class="text-center">Data Kosong</td></tr>';
									}									
								?>
							</tbody>							
						</table>
					</div>	
					</div>
				</div>
			</div><!-- end div box body daftar retur penjualan -->
			</div><!-- end div box daftar retur penjualan -->
			</div><!-- end div col md 12 daftar retur penjualan -->
			
			<div class="col-md-6"><!-- div col md 6 pembayaran -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Pembayaran Retur Penjualan</h3>
			</div><!-- /.box-header pembayaran -->
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-3 control-label">Jumlah Total</label>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="jtotal" name="jtotal"  class="form-control input-sm number-mask" value="<?php echo $rpj_jumlah;?>" readonly />
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Potongan</label>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="potongan" name="potongan"  class="form-control input-sm number-mask" value="<?php echo $rpj_potongan;?>" />
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Grand Total</label>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="gtotal" name="gtotal"  class="form-control input-sm number-mask" value="<?php echo $rpj_total;?>" readonly />
						</div>
					</div>
				</div>
			</div><!-- end div box-body pembayaran -->
			</div><!-- end div box pembayaran -->
			</div><!-- end div col md 6 pembayaran -->
			
			<div class="panel-footer">
				<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
			</div> 
			
		</div><!-- end div body utama -->
	</form><!-- end form-->
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('log_penjualan');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div> <!-- end div box-footer utama -->
</div> <!-- End box utama-->

<script>
//fungsi hapus item penjualan
function RetDel(id) {
	var id		= id,
		koderetur= $("#koderetur").val(),
		query	= 'type=delete-item'+
				  '&koderetur='+koderetur+
				  '&rpjdid='+id;
	var pilih	= confirm('Yakin akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/retur_penjualan_form/retur_penjualan_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
               	window.alert("Data Berhasil Dihapus");      
				location.reload();
			}
		});
	}
}


// Main Function		
$(function(){ 	
	var	userid  	= <?php echo $userid;?>,
		jt			= toNumber($("#jtotal").val());
	
	//initial
    set_mask();
	
	$("#gtotal").val(jt);
	
	$("#retur").keyup(function(){
		var hargajual	= toNumber($("#hargajual").val()),
			retur  		= toNumber($("#retur").val()),
			beli  		= toNumber($("#beli").val()),
			jumlah		= hargajual*(retur/1);
			
		if(retur>beli){
			window.alert('Ooops!\nRetur tidak boleh lebih dari beli! \nSesuaikan retur dengan jumlah beli');
			$("#retur").val(beli);
			$("#retur").focus();
			exit;
		}
		
		$("#jumlah").val(jumlah);
	});
	
	$("#btn-can").click(function(e){
        e.preventDefault();
		window.location = '?p=<?php echo paramEncrypt('retur_penjualan_form');?>&kdpj=<?php echo paramEncrypt($pj_kode);?>';
    })
	
	$("#btn-add").click(function(e){
		e.preventDefault();
		
		var koderetur		= $("#koderetur").val(),
			kodejual		= $("#kodejual").val(),
			pjdid			= $("#pjd_id").val(),
			kategori		= $("#kategori").val(),
			kodeprod		= $("#kodeprod").val(),
			nama   			= $("#nama").val(),
			hargajual		= toNumber($("#hargajual").val()),
			hargabeli		= toNumber($("#hargabeli").val()),
			satuan			= $("#satuan").text(),
			stok   			= toNumber($("#stok").val()),
			beli   			= toNumber($("#beli").val()),
			retur  			= toNumber($("#retur").val()),
			jumlah 			= toNumber($("#jumlah").val()),
			
			query			= 'userid='+userid+
							'&type=save-item'+
							'&koderetur='+koderetur+
							'&kodejual='+kodejual+
							'&pjdid='+pjdid+
							'&kategori='+kategori+
							'&kodeprod='+kodeprod+
							'&nama='+nama+
							'&hargajual='+hargajual+
							'&hargabeli='+hargabeli+
							'&satuan='+satuan+
							'&stok='+stok+
							'&beli='+beli+
							'&retur='+retur+
							'&jumlah='+jumlah;					
		// validate 
		if(!kategori && !kodeprod && !nama && !hargajual && !beli && !jumlah){
			window.alert('Ooops!\nItem Retur Penjualan Belum ada!');
			$("#cari").focus();
			exit;
		}
		
		if(!retur){
			window.alert('Ooops!\nRetur tidak boleh kosong!');
			$("#retur").focus();
			exit;
		}
		
		if(retur>beli){
			window.alert('Ooops!\nRetur tidak boleh lebih dari beli! \nSesuaikan retur dengan jumlah beli');
			$("#retur").val(beli);
			$("#retur").focus();
			exit;
		}
		
		if(retur==0 || !retur){
			window.alert('Ooops!\nRetur tidak boleh kosong Minimanl 1');
			$("#retur").focus();
			exit;
		}
		
		// save via ajax
        $.ajax({
			url     : 'modules/retur_penjualan_form/retur_penjualan_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
				window.alert("Data Berhasil Disimpan");		
				window.location = '?p=<?php echo paramEncrypt('retur_penjualan_form');?>&kdpj=<?php echo paramEncrypt($pj_kode);?>&kdrpj=<?php echo paramEncrypt($rpj_kode);?>&pjdid=<?php echo paramEncrypt(0);?>';					
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		}); 
	})
	
	$("#potongan").keyup(function(){
		var jtotal		= toNumber($("#jtotal").val()),
			potongan	= toNumber($("#potongan").val()),
			gtotal		= jtotal-potongan;
			
		if(potongan>jtotal){
			window.alert('Ooops!\nPotongan tidak boleh lebih dari jumlah total! \nSesuaikan potongan dengan jumlah total');
			$("#potongan").val(0);
			$("#potongan").focus();
			exit;
		}
		
		$("#gtotal").val(gtotal);
	});
	
	$("#btn-submit").click(function(e){
		e.preventDefault();
		
        var koderetur		= $("#koderetur").val(),
			jtotal			= toNumber($("#jtotal").val()),
			potongan		= toNumber($("#potongan").val()),
			gtotal			= toNumber($("#gtotal").val());
			
            query		= 'userid='+userid+
						'&type=save'+
						'&koderetur='+koderetur+
						'&jtotal='+jtotal+
						'&potongan='+potongan+
						'&gtotal='+gtotal;

		// validate	
		if(jtotal==0 || !jtotal){
			window.alert('Ooops!\nItem Retur Penjualan Belum ada!');
			exit;
		}
		
        // save via ajax
        $.ajax({
            url     : 'modules/retur_penjualan_form/retur_penjualan_form_ajax.php',
            data    : query,
            cache   : false,
            type    : 'post',
            success : function(data) {
				window.alert("Data Berhasil Disimpan");
				window.location = '?p=<?php echo paramEncrypt('log_penjualan');?>';
            },
            error : function(xhr, textStatus, errorThrown) {
                alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
                return false; //exit();
            }
        });
    })
	
})// End Main Function

</script>