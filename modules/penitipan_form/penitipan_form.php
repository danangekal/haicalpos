<?php
defined('_IEXEC')or die('');

if(!isset($_REQUEST['kd']) && !isset($_REQUEST['pndid'])){
	$title 			= 'New';
	
	//data penitipan
	$pn_kode		= gen_pnid();
	$pn_suplier		= '';
	$pn_nota		= '';
	$pn_tanggal		= '';
	$jbayar			= 0;	
	
	$cari			= '';

	$idp			= '';
	$kategori		= '';
	$kodep			= '';
	$namap			= '';
	$harga_beli		= '';
	$satuan			= '';
	$expire			= '';
	$titip			= '';
	$jumlah			= '';	
	$stok			= '';
	
}else{
	$title 		= 'Edit';
	$pn_kode	= paramDecrypt($_REQUEST['kd']);
	$pnd_id		= paramDecrypt($_REQUEST['pndid']);
	
	//data penitipan
	$query  = "
		SELECT  
			*
		FROM
			penitipan
		WHERE
			pn_kode = '$pn_kode'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));		
	}
	
	//data item penitipan detail
	if($pnd_id > 0){
		$query3  = "
			SELECT  
				pnd_id as idp, 
				pnd_p_k_id as kategori,
				pnd_p_kode as kodep, 
				pnd_p_nama as namap, 
				pnd_p_harga_beli as harga_beli,
				pnd_p_satuan as satuan,
				pnd_p_tgl_expire as expire,
				pnd_titip as titip,
				pnd_jumlah as jumlah,
				p_stok as stok
			FROM
				penitipan_detail
			INNER JOIN
				produk
			ON
				pnd_p_kode = p_kode
			WHERE
				pnd_id = '$pnd_id'
		";
		$result3 = mysql_query($query3)or die(mysql_error());
		if(mysql_num_rows($result3)){
			extract(mysql_fetch_assoc($result3));
			$cari = $kodep.' - '.ucwords($namap).' - '.rupiah($harga_beli).' / '.$satuan;
		}
	} else {
		$cari			= '';
		$idp			= '';
		$kategori		= '';
		$kodep			= '';
		$namap			= '';
		$harga_beli		= '';
		$satuan			= '';
		$expire			= '';
		$titip			= '';
		$jumlah			= '';		
		$stok			= '';
	}
	
}
	// loading data untuk auto complete
    $q = mysql_query("
        SELECT 
			*
        FROM 
			produk
        WHERE 
			p_aktif = 'Y'
			&& p_jenis = 'TITIPAN'
        ORDER BY
			p_id ASC         
    ")or die(mysql_error());
    
    $data       = '[';
    $num_data   = mysql_num_rows($q);
    $no         = 0;
    while($r = mysql_fetch_array($q)){
        $no++;
        extract($r);

        $data .= '
            {
                "value":"'.$p_kode.' - '.ucwords($p_nama).' - '.rupiah($p_harga_beli).' / '.$p_satuan.'",
                "kode":"'.$p_kode.'",
                "nama":"'.ucwords($p_nama).'",
				"hargabeli":"'.$p_harga_beli.'",
				"satuan":"'.$p_satuan.'",
				"stok":"'.$p_stok.'",
				"expire":"'.$p_tgl_expire.'",
				"kategori":"'.$p_k_id.'"
            }    
        ';
               
        if($no < $num_data){
            $data .=',';    
        }
    }
    $data .=']';
?>

<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $pn_kode;?></h3>
	</div><!-- /.box-header utama -->
	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-8"><!-- div col md 8 data penitipan -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Data Penitipan</h3>
			</div><!-- /.box-header data penitipan -->
			<div class="box-body">	
				<div class="form-group">
					<label class="col-sm-3 control-label">Kode Penitipan</label>
					<div class="col-sm-4">
						<input type="text" id="kodetitip" name="kodetitip" class="form-control input-sm" value="<?php echo $pn_kode;?>" readonly />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Suplier</label>
					<div class="col-sm-4">
						<input type="text" id="suplier" name="suplier" class="form-control input-sm" value="<?php echo $pn_suplier;?>" autofocus />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Nota Penitipan</label>
					<div class="col-sm-4">
						<input type="text" id="nota" name="nota" class="form-control input-sm" value="<?php echo $pn_nota;?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Cari Product</label>
					<div class="col-sm-8">
						<input type="text" id="cari" name="cari" class="form-control input-sm" onclick="clearInput(this)" placeholder="Nama Product" value="<?php echo $cari;?>" />
					</div>
				</div>
			</div><!-- end div box-body data penitipan-->
			</div><!-- end div box data penitipan -->
			</div><!-- end div col md 8 data penitipan -->
			
			<div class="col-md-12"><!-- div col md 12 item penitipan -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Item Penitipan Product</h3>
			</div><!-- /.box-header item penitipan -->
			<div class="box-body">
				<div class="form-group">
					<div class="col-sm-14">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" style="margin-bottom:2px;">
							<thead>
								<tr class="success">
								<th></th>
								<th></th>
								<th>Kode Product</th>
								<th>Nama Product</th>
								<th>Harga Product/Satuan</th>
								<th></th>
								<th>Tanggal Expire</th>
								<th>Titip Product</th>
								<th>Jumlah Harga</th>
								<th></th>
								</tr>
								<tr>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="hidden" id="pnd_id" name="pnd_id" value="<?php echo $idp;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="hidden" id="kategori" name="kategori" value="<?php echo $kategori;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="text" id="kodeprod" name="kodeprod" value="<?php echo $kodep;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="text" id="nama" name="nama" value="<?php echo $namap;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><span class="input-group-addon">Rp</span><input class="form-control input-sm number-mask" type="text" id="hargabeli" name="hargabeli" value="<?php echo $harga_beli;?>" readonly /><span class="input-group-addon">/</span><span class="input-group-addon" id="satuan" name="satuan"><?php echo $satuan;?></span></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="hidden" id="stok" name="stok" value="<?php echo $stok;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="datepicker" type="text" id="tglexpire" name="tglexpire" placeholder="yyyy-mm-dd" value="<?php echo $expire;?>" /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="text" id="titip" name="titip" value="<?php echo $titip;?>" /></div></th>
								<th><div class="input-group input-group-sm"><span class="input-group-addon">Rp</span><input class="form-control input-sm number-mask" type="text" id="jumlah" name="jumlah" value="<?php echo $jumlah;?>" readonly /></div></th>
								<th>
									<?php 
									if($idp==0 || !$idp){
										echo '
											<button type="button" class="btn btn-success btn-xs" id="btn-add" name="btn-add">Add</button>
											<button type="button" class="btn btn-danger btn-xs" id="btn-del" name="btn-del">Remove</button>
										';
									} else {
										echo '
											<button type="button" class="btn btn-success btn-xs" id="btn-add" name="btn-add">Update</button>
											<button type="button" class="btn btn-danger btn-xs" id="btn-can" name="btn-can">Cancel</button>
										';
									}
									?>
								</th>     
								<tr>
							</thead>
							<tbody id="item-penitipan">
							</tbody>
						</table>
					</div>	
					</div>
				</div>
			</div><!-- end div box body item penitipan -->
			</div><!-- end div box item penitipan -->
			</div><!-- end div col md 12 item penitipan -->
			
			<div class="col-md-12"><!-- div col md 12 daftar penitipan -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Daftar Penitipan Product</h3>
			</div><!-- /.box-header item penitipan -->
			<div class="box-body">
				<div class="form-group">
					<div class="col-sm-14">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" style="margin-bottom:2px;">
								<tr class="success">
								<th>No</th>
								<th>Product</th>
								<th>Harga Product/Satuan</th>
								<th>Tanggal Expire</th>
								<th>Titip Product</th>
								<th>Jumlah</th>
								<th>Terjual</th>
								<th>Sisa</th>
								<th>Bayar</th>
								<th>Action</th>
								</tr>
							<tbody>
								<?php
									//data penitipan detail
									$query2  = "
										SELECT  
											*
										FROM
											penitipan_detail
										INNER JOIN
											produk
										ON
											pnd_p_kode = p_kode
										WHERE
											pnd_pn_kode = '$pn_kode'
									";
									$result2 = mysql_query($query2)or die(mysql_error());
	
									if(mysql_num_rows($result2)>0){
										$no			= 0;
										$jbayar		= 0;
										while($r = mysql_fetch_array($result2)){
											extract($r);
											$no++;
											if($pnd_titip>$p_stok){
												$terjual	= $pnd_titip-$p_stok;
											} else {
												$terjual	= 0;
											}
											$sisa		= $p_stok;
											$bayar		= $terjual*$pnd_p_harga_beli;
											$jbayar		+= $bayar;
											echo '
												<tr>
													<td>'.$no.'</td>
													<td>'.$pnd_p_kode.' '.$pnd_p_nama.'</td>
													<td>'.rupiah($pnd_p_harga_beli).' / '.$pnd_p_satuan.'</td>
													<td>'.showdt($pnd_p_tgl_expire, 2).'</td>
													<td>'.$pnd_titip.'</td>
													<td>'.rupiah($pnd_jumlah).'</td>
													<td>'.$terjual.'</td>
													<td>'.$sisa.'</td>
													<td>'.rupiah($bayar).'</td>
											';
											if($pnd_titip==$p_stok){
												echo '
														<td>
															<a class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit Item Penitipan" href="?p='.paramEncrypt('penitipan_form').'&kd='.paramEncrypt($pn_kode).'&pndid='.paramEncrypt($pnd_id).'"><i class="fa fa-edit"></i></a>
															<a class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete Item Penitipan" href="javascript:ItemDel('.$pnd_id.')" role="button"><i class="fa fa-trash-o"></i></a>
														</td>
													</tr>
												';
											} else {
												echo '
													<td>
														...
													</td>
												</tr>
												';
											}
										}
									} else {
										echo '<tr><td colspan="10" class="text-center">Data Kosong</td></tr>';
									}									
								?>
							</tbody>							
						</table>
					</div>	
					</div>
				</div>
			</div><!-- end div box body daftar penitipan -->
			</div><!-- end div box daftar penitipan -->
			</div><!-- end div col md 12 daftar penitipan -->
			
			<div class="col-md-6"><!-- div col md 4 detail pembayaran -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Detail Pembayaran</h3>
			</div><!-- /.box-header detail pembayaran -->
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-3 control-label">Jumlah Total</label>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="jbayar" name="jbayar"  class="form-control input-sm number-mask" value="<?php echo $jbayar;?>" readonly />
						</div>
					</div>
				</div>
			</div><!-- end div box-body detail pembayaran -->
			</div><!-- end div box detail pembayaran -->
			</div><!-- end div col md 6 detail pembayaran -->
						
			<div class="panel-footer">
				<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
			</div> 
			
		</div><!-- end div body utama -->
	</form><!-- end form-->
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('penitipan');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div> <!-- end div box-footer utama -->
</div> <!-- End box utama-->

<script>
//fungsi hapus item penitipan
function ItemDel(id) {
	var id		= id,
		kodetitip= $("#kodetitip").val(),
		query	= 'type=delete-item'+
				  '&pndid='+id;
	var pilih	= confirm('Yakin akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/penitipan_form/penitipan_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
               	//window.alert("Data Berhasil Dihapus");      
				//location.reload();
				window.location = '?p=<?php echo paramEncrypt('penitipan_form');?>&kd=<?php echo paramEncrypt($pn_kode);?>&pndid=<?php echo paramEncrypt(0);?>&pndid2=<?php echo paramEncrypt(0);?>';
			}
		});
	}
}

// Main Function		
$(function(){ 	
	var	userid  	= <?php echo $userid;?>,
		hasil		= <?php echo $data;?>;
	//initial
    set_mask();
	
	$(".datepicker").datepicker({
		format: 'yyyy-mm-dd'
	})
	
	$('#cari').autocomplete({
		lookup: hasil,
		onSelect: function (suggestion) {			
				$("#kategori").val(suggestion.kategori);
				$("#kodeprod").val(suggestion.kode);
				$("#nama").val(suggestion.nama);
				$("#hargabeli").val(suggestion.hargabeli);
				$("#stok").val(suggestion.stok);
				$("#tglexpire").val(suggestion.expire);
				$("#titip").val(1);
				$("#satuan").text(suggestion.satuan);
			
				var hargabeli	= toNumber($("#hargabeli").val()),
					titip   		= toNumber($("#titip").val()),
					jumlah 		= hargabeli*(titip/1);
					
				$("#jumlah").val(jumlah);
				$("#titip").focus();				
		}
	});
		
	$("#titip").keyup(function(){
		var hargabeli	= toNumber($("#hargabeli").val()),
			titip   	= toNumber($("#titip").val()),
			stok   		= toNumber($("#stok").val()),
			jumlah		= hargabeli*(titip/1);
		
		$("#jumlah").val(jumlah);
	});
	
	$("#btn-del").click(function(e){
        e.preventDefault();
		$("#kategori, #kodeprod, #nama, #hargabeli, #stok, #tglexpire, #titip, #jumlah, #cari").val('');
		$("#cari").focus();
    })
	
	$("#btn-can").click(function(e){
        e.preventDefault();
		window.location = '?p=<?php echo paramEncrypt('penitipan_form');?>&kd=<?php echo paramEncrypt($pn_kode);?>&pndid=<?php echo paramEncrypt(0);?>&pndid2=<?php echo paramEncrypt(0);?>';
    })
	
	$("#btn-add").click(function(e){
		e.preventDefault();
		
		var kodetitip		= $("#kodetitip").val(),
			suplier			= $("#suplier").val(),
			nota			= $("#nota").val(),
			pndid			= $("#pnd_id").val(),
			kategori		= $("#kategori").val(),
			kodeprod		= $("#kodeprod").val(),
			nama   			= $("#nama").val(),
			hargabeli		= toNumber($("#hargabeli").val()),
			satuan			= $("#satuan").text(),
			stok   			= toNumber($("#stok").val()),
			tglexpire		= $("#tglexpire").val(),
			titip   		= toNumber($("#titip").val()),
			jumlah 			= toNumber($("#jumlah").val()),
			
			query			= 'userid='+userid+
							'&type=save-item'+
							'&suplier='+suplier+
							'&nota='+nota+
							'&kodetitip='+kodetitip+
							'&pndid='+pndid+
							'&kategori='+kategori+
							'&kodeprod='+kodeprod+
							'&nama='+nama+
							'&hargabeli='+hargabeli+
							'&satuan='+satuan+
							'&stok='+stok+
							'&tglexpire='+tglexpire+
							'&titip='+titip+
							'&jumlah='+jumlah;
							
		// validate 
		if(!kategori && !kodeprod && !nama && !hargabeli && !jumlah){
			window.alert('Ooops!\nItem Penitipan Belum ada!');
			$("#cari").focus();
			exit;
		}
		
		if(!titip || titip==0){
			window.alert('Ooops!\nTitip tidak boleh kosong!');
			$("#titip").focus();
			exit;
		}
		
		if(!tglexpire){
			window.alert('Ooops!\nTanggal Expire tidak boleh kosong!');
			$("#tglexpire").focus();
			exit;
		}
		
		// save via ajax
        $.ajax({
			url     : 'modules/penitipan_form/penitipan_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
				//window.alert("Data Berhasil Disimpan");		
				window.location = '?p=<?php echo paramEncrypt('penitipan_form');?>&kd=<?php echo paramEncrypt($pn_kode);?>&pndid=<?php echo paramEncrypt(0);?>&pndid2=<?php echo paramEncrypt(0);?>';					
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		}); 
	})	
		
	$("#btn-submit").click(function(e){
		e.preventDefault();
		
        var kodetitip		= $("#kodetitip").val(),
			suplier			= $("#suplier").val(),
			nota			= $("#nota").val(),
			jbayar			= toNumber($("#jbayar").val());
			
            query		= 'userid='+userid+
						'&type=save'+
						'&kodetitip='+kodetitip+
						'&suplier='+suplier+
						'&nota='+nota+
						'&jbayar='+jbayar;
		
		// validate	
		if(!suplier){
			window.alert('Ooops!\nSuplier tidak boleh kosong!');
			$("#suplier").focus();
			exit;
		}
		
		if(!nota){
			window.alert('Ooops!\nNo nota tidak boleh kosong!');
			$("#nota").focus();
			exit;
		}
		
		if(!jbayar || jbayar==0){
			window.alert('Ooops!\nBelum bisa submit sebelum ada produk yang terjual!');
			exit;
		}
		
        // save via ajax
        $.ajax({
            url     : 'modules/penitipan_form/penitipan_form_ajax.php',
            data    : query,
            cache   : false,
            type    : 'post',
            success : function(data) {
				//window.alert("Data Berhasil Disimpan");
				window.location = '?p=<?php echo paramEncrypt('penitipan');?>';
            },
            error : function(xhr, textStatus, errorThrown) {
                alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
                return false; //exit();
            }
        });
    })
	
})// End Main Function

</script>