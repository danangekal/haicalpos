<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$sdate			= $_POST['sdate'];
	$edate			= $_POST['edate'];
	$kategori_id	= $_POST['kategori'];
	
	if ($kategori_id<=0){
		$query="
		SELECT  
			pjd_p_k_id
		FROM    
			penjualan,		
			penjualan_detail 
		WHERE
			penjualan.pj_kode 	= penjualan_detail.pjd_pj_kode
			&& pj_tanggal	>= '$sdate'
			&& pj_tanggal	<= '$edate'
			&& pj_bayar > 0
		GROUP BY 
			pjd_p_k_id
		";
		$result = mysql_query($query) or die(mysql_error());
	} else {
		$query="
		SELECT  
			pjd_p_k_id,
			pjd_p_kode,
            pjd_p_nama,
            pjd_p_harga_jual,
			pjd_p_satuan,
			SUM(pjd_profit) as profit,
            SUM(pjd_beli) as beli,
            SUM(pjd_jumlah) as jumlah
		FROM    
			penjualan,		
			penjualan_detail 
		WHERE
			penjualan.pj_kode 	= penjualan_detail.pjd_pj_kode
			&& pj_tanggal	>= '$sdate'
			&& pj_tanggal	<= '$edate'
			&& pjd_p_k_id	= '$kategori_id'
			&& pj_bayar > 0
		GROUP BY 
			pjd_p_kode
		";
		$result = mysql_query($query) or die(mysql_error());
	}
	$content = '';
    if(mysql_num_rows($result)>0){
		if ($kategori_id<=0) {
			$que="
			SELECT  
				SUM(pj_jumlah) as jtotal_all,
				SUM(pj_diskon) as jtdiskon,
				SUM(pj_ppn) as jtppn,
				SUM(pj_total) as gtotal,
				SUM(pj_profit) as jtprofit
			FROM    
				penjualan 
			WHERE
				pj_tanggal		>= '$sdate'
				&& pj_tanggal	<= '$edate'
				&& pj_bayar > 0
			";
			$resu = mysql_query($que) or die(mysql_error());
			$rw = mysql_fetch_assoc($resu);
			extract($rw);
			
			while($row = mysql_fetch_assoc($result)){
				extract($row);
				$content .= '
				<h5>Tanggal Penjualan '.showdt($sdate, 2).' - '.showdt($edate, 2).' ('.get_category($pjd_p_k_id).')</h5>
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr class="success">
						<th>No</th>
						<th>Kode</th>
						<th>Nama</th>
						<th>Beli</th>
						<th>Harga Jual</th>
						<th>Jumlah</th>
						<th>Profit</th>
					</tr>
				</thead>
				<tbody>
				';
				$q="
				SELECT  
					pjd_p_kode,
					pjd_p_nama,
					pjd_p_harga_jual,
					pjd_p_satuan,
					SUM(pjd_profit) as profit,
					SUM(pjd_beli) as beli,
					SUM(pjd_jumlah) as jumlah
				FROM    
					penjualan,		
					penjualan_detail 
				WHERE
					penjualan.pj_kode 	= penjualan_detail.pjd_pj_kode
					&& pj_tanggal	>= '$sdate'
					&& pj_tanggal	<= '$edate'
					&& pjd_p_k_id	= '$pjd_p_k_id'
					&& pj_bayar > 0
				GROUP BY 
					pjd_p_kode
				";
				$res		= mysql_query($q) or die(mysql_error());
				$no			= 0;
				$jtotal		= 0;
				$jprofit	= 0;
				while($r = mysql_fetch_assoc($res)){
					extract($r);
					$no++;
					$content	.='
								<tr>
									<td>'.$no.'</td>
									<td><b>'.$pjd_p_kode.'</b></td>
									<td>'.ucwords($pjd_p_nama).'</td>
									<td>'.$beli.' / '.$pjd_p_satuan.'</td>
									<td>'.rupiah($pjd_p_harga_jual).'</td>
									<td>'.rupiah($jumlah).'</td>
									<td>'.rupiah($profit).'</td>
								</tr>
								'; 
				$jtotal	+= $jumlah;
				$jprofit+= $profit;
				}			
				
				$content	.= '
							<tr>
								<td colspan="4"></td>
								<td class="success"><b>Jumlah Penerimaan Kotor & Profit</b></td>
								<td class="success"><b>'.rupiah($jtotal).'</b></td>
								<td class="success"><b>'.rupiah($jprofit).'</b></td>
							</tr>
							</tbody>
							</table>
							</div>
							';
			}
			$omtppn		= $jtotal_all-$jtdiskon;
			$content	.= '
						<div class="table-responsive">
						<table class="table table-bordered">
						<tr class="success">
							<td ><b>Total Penerimaan Kotor</b></td>
							<td><b>'.rupiah($jtotal_all).'</b></td>
						</tr>	
						<tr class="success">
							<td ><b>Total Profit</b></td>
							<td><b>'.rupiah($jtprofit).'</b></td>
						</tr>	
						<tr class="success">
							<td ><b>Total Pemotongan Diskon</b></td>
							<td><b>'.rupiah($jtdiskon).'</b></td>
						</tr>						
						<tr class="success">
							<td ><b>Total Penerimaan Bersih (Total Penerimaan Kotor - Total Pemotongan Diskon)</b></td>
							<td><b>'.rupiah($omtppn).'</b></td>
						</tr>
						<tr class="success">
							<td ><b>Total Penerimaan PPn</b></td>
							<td><b>'.rupiah($jtppn).'</b></td>
						</tr>
						<tr class="success">
							<td ><b>Total Penerimaan Keseluruhan (Total Penerimaan Bersih + Total Penerimaan PPn)</b></td>
							<td><b>'.rupiah($gtotal).'</b></td>
						</tr>
						</table>
						</div>
						<br>
						<br>
						<a data-toggle="tooltip" title="Export Laporan Penjualan (PDF)" class="btn btn-danger" href="page.php?p='.paramEncrypt('lap_penjualan_pdf').'&sdt='.paramEncrypt($sdate).'&edt='.paramEncrypt($edate).'&sid='.paramEncrypt($kategori_id).'" target="_blank" role="button"><i class="fa fa-download"></i> PDF</a>
						';
		} else {
			$content .= '
					<h5>Tanggal Penjualan '.showdt($sdate, 2).' - '.showdt($edate, 2).' '.get_category($kategori_id).'</h5>
					<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr class="success">
							<th>No</th>
							<th>Kode</th>
							<th>Nama</th>
							<th>Beli</th>
							<th>Harga Jual</th>
							<th>Jumlah</th>
							<th>Profit</th>
					</thead>
					<tbody>
					';
				$no=0;
				$jtotal = 0;
				$jprofit	= 0;
				while($row = mysql_fetch_assoc($result)){
					extract($row);
					$no++;
					$content	.='
								<tr>
									<td>'.$no.'</td>
									<td><b>'.$pjd_p_kode.'</b></td>
									<td>'.ucwords($pjd_p_nama).'</td>
									<td>'.$beli.' / '.$pjd_p_satuan.'</td>
									<td>'.rupiah($pjd_p_harga_jual).'</td>
									<td>'.rupiah($jumlah).'</td>
									<td>'.rupiah($profit).'</td>
								</tr>
								'; 
				$jtotal += $jumlah;
				$jprofit+= $profit;
				}
				$content	.= '
							<tr>
								<td colspan="4"></td>
								<td class="success"><b>Jumlah Penerimaan Kotor & Profit</b></td>
								<td class="success"><b>'.rupiah($jtotal).'</b></td>
								<td class="success"><b>'.rupiah($jprofit).'</b></td>
							</tr>
							</tbody>
							</table>
							</div>
							<br>
							<br>
							<a data-toggle="tooltip" title="Export Laporan Penjualan (PDF)" class="btn btn-danger" href="page.php?p='.paramEncrypt('lap_penjualan_pdf').'&sdt='.paramEncrypt($sdate).'&edt='.paramEncrypt($edate).'&sid='.paramEncrypt($kategori_id).'" target="_blank" role="button"><i class="fa fa-download"></i> PDF</a>
							';
		}
		
    } else {
		$content    .= '
				<h5>Tanggal Penjualan '.showdt($sdate, 2).' - '.showdt($edate, 2).' '.get_category($kategori_id).'</h5>
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
				<tbody>
				<tr><td colspan="7" class="text-center"> -- Not Found Content --</td></tr>
				</tbody>
				</table>
				</div>
				';
	}
    echo $content;
?>