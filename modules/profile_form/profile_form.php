<?php
defined('_IEXEC')or die('');

if(!isset($_REQUEST['id'])){
	$title 			= 'New';
	$id 			= '';
	$user_name		= '';
	$user_password	= '';
	$user_fullname	= '';
	$user_salt		= '';
}else{
	$title 		= 'Edit';
	$id 		= paramDecrypt($_REQUEST['id']);
	$query  = "
		SELECT  *
		FROM
			user
		WHERE
			user_id = '$id'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
	}
}
?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $user_fullname;?></h3>
	</div><!-- /.box-header -->

	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-8">
			<div class="box box-info">
			<div class="box-body">	
				<div class="form-group">
					<input type="hidden" id="id" name="id" value="<?php echo $id; ?>"/>
					<label class="col-sm-2 control-label">User Name</label>
					<div class="col-sm-3">
						<input type="text" id="username" name="username" placeholder="User Name" class="form-control input-sm" required="true" value="<?php echo $user_name;?>" autofocus/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Password</label>
					<input type="hidden" id="salt" name="salt" value="<?php echo $user_salt; ?>"/>
					<div class="col-sm-3">
						<input type="password" id="password" name="password" placeholder="Password" class="form-control input-sm" />
					</div>
					<?php
					if ($id>0){
					?>
					<label class="col-sm-6 control-label">* Kosongkan password bila tidak diubah</label>
					<?php
					}
					?>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Full Name</label>
					<div class="col-sm-4">
						<input type="text" id="fullname" name="fullname" placeholder="Full Name" class="form-control input-sm" required="true" value="<?php echo $user_fullname;?>" />
					</div>
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
				</div> 
			</div> <!-- End box body isi-->
			</div> <!-- End box isi-->
			</div> <!-- End col-md-8 isi-->
		<div class="col-md-3">
			<center><a class="btn btn-default btn-lg" href="javascript:del(<?php echo $user_id;?>)" role="button">Delete My Account</a></center>
			<img id="previewing" src="images/users/<?php echo $user_foto; ?>" class="img-circle img-thumbnail"/>
			<center><a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('profile_change_foto');?>&id=<?php echo paramEncrypt($id);?>" role="button">Change Foto Profile</a></center>
		</div>
		</div> <!-- End box body utama-->
	</form>	
</div><!-- End box utama-->

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/profile_form/profile_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				if(id == "") {
					window.alert("Data Gagal Dihapus");
				} else {
					window.alert("Data Berhasil Dihapus");
				}      
				window.location = '?p=<?php echo paramEncrypt('logout');?>';
			}
		});
	}
}

// Main Function		
$(function(){ 
	$("#btn-submit").click(function(){
		var id	    	= $("#id").val(),
			username   	= $("#username").val(),
			password    = $("#password").val(),
			salt	    = $("#salt").val(),
			fullname   	= $("#fullname").val();
		
		// validasi
		if (!id && !password) {
		   window.alert('Ooops!\nPassword harus diisi');
		   $("#password").focus();
		   return;
		}
		if(!username){
		   window.alert('Ooops!\nUser Name harus diisi');
		   $("#username").focus();
		   return;
		}
		
		if(!fullname){
		   window.alert('Ooops!\nFull Name harus diisi');
		   $("#fullname").focus();
		   return;
		}
			
		var query   =   'type=save'+
        				'&id='+id+
						'&username='+username+
						'&password='+password+
						'&salt='+salt+
						'&fullname='+fullname;
		$.ajax({
			url     : 'modules/profile_form/profile_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
               	window.alert("Data Berhasil Diperbaharui"); 
				window.location = '?p=<?php echo paramEncrypt('home');?>';
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		});

	})
})//ready

</script>