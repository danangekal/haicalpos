<?php
defined('_IEXEC')or die('');
?>

<div class="box box-solid box-info">
	<div class="box-header with-border">
	  <h3 class="box-title">Management <?php echo ucwords($p); ?></h3>
	</div><!-- /.box-header -->
	<div class="box-body">
		<div class="col-md-12"><!-- div col md 12 parameter -->
		<div class="box box-info">
		<div class="box-header">
		  <h3 class="box-title">Parameter Laporan</h3>
		</div><!-- /.box-header parameter -->
		<div class="box-body">
		<div class="form-group">
			<label class="col-sm-2 control-label">Tanggal Mulai</label>
			<div class="col-sm-2">
				<input type="sale" id="startdate" name="startdate" class="datepicker" required="true" />						
			</div>
			
			<label class="col-sm-2 control-label">Tanggal Akhir</label>
			<div class="col-sm-2">
				<input type="sale" id="enddate" name="enddate" class="datepicker" required="true" />						
			</div>

			<div class="col-sm-2">
				<button type="button" class="btn btn-sm btn-info" id="btn-lihat"><i class="fa fa-eye"></i> Lihat</button>
			</div>
		</div>
		</div><!-- end div box-body parameter-->
		</div><!-- end div box data parameter -->
		</div><!-- end div col md 6 parameter -->
		
		<div class="col-md-12" name="report-area" id="report-area" hidden><!-- div col md 12 parameter -->
		<div class="box box-info">
		<div class="box-header">
			<h3 class="box-title">Laporan Pengeluaran</h3>
		</div><!-- /.box-header report -->
		<div class="box-body" name="detail-report" id="detail-report">
		
		</div><!-- end div box-body report-->
		</div><!-- end div box data report -->
		</div><!-- end div col md 6 report -->
		
	</div><!-- /.box-body -->
</div><!-- /.box -->	

<script>
// Main Function		
$(function(){ 
	$(".datepicker").datepicker({
		format: 'yyyy-mm-dd'
	})
	
	$("#btn-lihat").click(function(){
       var	sdate		= 	$("#startdate").val();
			edate		= 	$("#enddate").val();
			query   	=	'sdate='+sdate+
							'&edate='+edate;
		
		//validate
		if (!sdate){
			window.alert('Ooops!\nStart date tidak boleh kosong!');
			$("#startdate").focus();
			return false;
		}
		
		if (!edate){
			window.alert('Ooops!\nEnd date tidak boleh kosong!');
			$("#enddate").focus();
			return false;
		}
		
		$("#report-area").show();
					
		//window.alert(query);
		$.ajax({
			url     : 'modules/lap_pengeluaran/lap_pengeluaran_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				$("#detail-report").html(data);
			},
			error   : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest gagal: "' + errorThrown + '"');
				return false;
			}
		})
    })
	
	
	
})// Main Function
</script>