<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$sdate		= $_POST['sdate'];
	$edate		= $_POST['edate'];
	
	$query="
	SELECT  
		*
	FROM    
		pengeluaran 
	WHERE
		pg_tanggal	>= '$sdate'
		&& pg_tanggal	<= '$edate'
	ORDER BY 
		pg_kode
	ASC	
	";
	$result = mysql_query($query) or die(mysql_error());
	
	$content = '';
    if(mysql_num_rows($result)>0){			
		$content .= '
		<h5>Periode Pengeluaran '.showdt($sdate, 2).' - '.showdt($edate, 2).'</h5>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr class="success">
				<th>#</th>
				<th>Kode</th>
				<th>Tanggal</th>
				<th>Penanggung Jawab</th>
				<th>Jenis Pengeluaran</th>
				<th>Jumlah</th>					
				<th>Keterangan</th>
				<th>Input By</th>
			</tr>
		</thead>
		<tbody>
		';
		
		$no				= 0;
		$jumlah_all		= 0;
		while($r = mysql_fetch_assoc($result)){
			extract($r);
			$no++;
			$content	.='
						<tr>
							<td>'.$no.'</td>
							<td><b>'.$pg_kode.'</b></td>
							<td>'.showdt($pg_tanggal, 2).'</td>
							<td>'.$pg_nama.'</td>
							<td>'.$pg_jenis.'</td>
							<td>'.rupiah($pg_jumlah).'</td>
							<td>'.$pg_keterangan.'</td>
							<td>'.get_fullname($pg_user_id).'</td>
						</tr>
						'; 	
		$jumlah_all	+= $pg_jumlah;
		}			
		
		$content	.= '
		</tbody>
		</table>
		<table class="table table-bordered">
		<tr class="success">
			<td ><b>Total Pengeluaran</b></td>
			<td><b>'.rupiah($jumlah_all).'</b></td>
		</tr>	
		</table>
		</div>
		<br>
		<br>
		<a data-toggle="tooltip" title="Export Laporan Pengeluaran (PDF)" class="btn btn-danger" href="page.php?p='.paramEncrypt('lap_pengeluaran_pdf').'&sdt='.paramEncrypt($sdate).'&edt='.paramEncrypt($edate).'" role="button"><i class="fa fa-download"></i> PDF</a>	
		';
		
    } else {
		$content    .= '
				<h5>Periode Pengeluaran '.showdt($sdate, 2).' - '.showdt($edate, 2).'</h5>
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
				<tbody>
				<tr><td colspan="8" class="text-center"> -- Not Found Content --</td></tr>
				</tbody>
				</table>
				</div>
				';
	}
    echo $content;
?>