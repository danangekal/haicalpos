<?php
defined('_IEXEC')or die('');

if(!isset($_REQUEST['id'])){
	$title 			= 'New';
	$p_id			= '';
	$p_kode			= gen_pid();
	$p_nama			= '';
	$p_harga_jual	= '';
	$p_harga_beli	= '';
	$p_satuan		= '';
	$p_stok			= '';
	$p_jenis		= '';
	$p_tgl_masuk	= '';
	$p_tgl_expire	= '';
	$p_k_id			= '';
	$p_aktif		= '';
}else{
	$title 		= 'Edit';
	$id 		=  paramDecrypt($_REQUEST['id']);
	$query  = "
		SELECT  *
		FROM
			produk
		WHERE
			p_id = '$id'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
	}
}
?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $p_nama;?></h3>
	</div><!-- /.box-header -->

	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-6">
			<div class="box box-info">
			<div class="box-body">
				<div class="form-group">
					<input type="hidden" id="id" name="id" value="<?php echo $p_id;?>"/>
					<label class="col-sm-2 control-label">Kode</label>
					<div class="col-sm-3">
						<input type="text" id="kode" name="kode" class="form-control input-sm" value="<?php echo $p_kode;?>" readonly />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nama</label>
					<div class="col-sm-6">
						<input type="text" id="nama" name="nama" placeholder="Nama Product" class="form-control input-sm" required="true" value="<?php echo $p_nama;?>" autofocus/>						
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Harga Jual</label>
					<div class="col-sm-6">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="hargajual" name="hargajual" placeholder="0" class="form-control input-sm number-mask" required="true" value="<?php echo $p_harga_jual;?>" />
						</div>
					</div>
				</div>  
				<div class="form-group">
					<label class="col-sm-2 control-label">Harga Beli</label>
					<div class="col-sm-6">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="hargabeli" name="hargabeli" placeholder="0" class="form-control input-sm number-mask" required="true" value="<?php echo $p_harga_beli;?>" />
						</div>
					</div>
				</div>  
				<div class="form-group">
					<label class="col-sm-2 control-label">Satuan</label>
					<div class="col-sm-4">
						<select class="form-control input-sm" id="satuan" name="satuan" required="true">	
						<?php opt_satuan($p_satuan);?>
						</select>	
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Stok</label>
					<div class="col-sm-3">
						<input type="text" id="stok" name="stok" placeholder="0" class="form-control input-sm number-mask" required="true" value="<?php echo $p_stok;?>" <?php if($jabatan>3){ echo 'readonly'; }?>/>						
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Jenis</label>
					<div class="col-sm-4">
						<select class="form-control input-sm" id="jenis" name="jenis" required="true">	
						<?php opt_jenis($p_jenis);?>
						</select>	
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Tanggal Masuk</label>
					<div class="col-sm-3">
						<input type="text" id="tglmasuk" name="tglmasuk" class="datepicker" required="true" value="<?php echo $p_tgl_masuk;?>" />						
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Tanggal Expire</label>
					<div class="col-sm-3">
						<input type="text" id="tglexpire" name="tglexpire" class="datepicker" required="true" value="<?php echo $p_tgl_expire;?>" />						
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Kategori</label>
					<div class="col-sm-6">
						<select class="form-control input-sm" id="kategori" name="kategori" required="true">	
						<?php opt_category($p_k_id);?>
						</select>	
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Aktif</label>
					<div class="col-sm-3">
						<select class="form-control input-sm" id="aktif" name="aktif" required="true">	
						<?php opt_aktif($p_aktif);?>
						</select>	
					</div>
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
				</div> 
			</div>
			</div>
			</div>	
		</div>
	</form>
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('produk');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div>
</div><!-- End Box-->

<script>
// Main Function		
$(function(){ 
	//initial
    set_mask();
	
	$(".datepicker").datepicker({
		format: 'yyyy-mm-dd'
	})
	
	$("#btn-submit").click(function(){
		var id	    	= $("#id").val(),
			kode    	= $("#kode").val(),
			nama    	= $("#nama").val(),
			hargajual   = toNumber($("#hargajual").val()),
			hargabeli   = toNumber($("#hargabeli").val()),
			satuan		= $("#satuan").val(),
			stok		= toNumber($("#stok").val()),
			jenis		= $("#jenis").val(),
			tglmasuk	= $("#tglmasuk").val(),
			tglexpire	= $("#tglexpire").val(),
			kategori	= $("#kategori").val(),
			aktif		= $("#aktif").val(),
			query  		= 'type=save'+
						'&id='+id+
						'&kode='+kode+
						'&nama='+nama+
						'&hargajual='+hargajual+
						'&hargabeli='+hargabeli+
						'&satuan='+satuan+
						'&stok='+stok+
						'&jenis='+jenis+
						'&tglmasuk='+tglmasuk+
						'&tglexpire='+tglexpire+
						'&kategori='+kategori+
						'&aktif='+aktif;
		
		// validasi
		if(!nama){
		   window.alert('Ooops!\nNama Product harus diisi');
		   $("#nama").focus();
		   return;
		}
		
		if(!hargajual){
		   window.alert('Ooops!\nHarga Jual Product harus diisi');
		   $("#hargajual").focus();
		   return;
		}
		
		if(!hargabeli){
		   window.alert('Ooops!\nHarga Beli Product harus diisi');
		   $("#hargabeli").focus();
		   return;
		}
		
		if(satuan=="" || !satuan){
		   window.alert('Ooops!\nSatuan harus dipilih');
		   $("#satuan").focus();
		   return;
		}

		if(jenis=="" || !jenis){
		   window.alert('Ooops!\nSatuan harus dipilih');
		   $("#satuan").focus();
		   return;
		}
		
		if(!tglmasuk){
		   window.alert('Ooops!\nTanggal Masuk Product harus diisi');
		   $("#tglmasuk").focus();
		   return;
		}
		
		if(!tglexpire){
		   window.alert('Ooops!\nTanggal Expire Product harus diisi');
		   $("#tglexpire").focus();
		   return;
		}
		
		if(kategori==0 || !kategori){
		   window.alert('Ooops!\nKategori harus dipilih');
		   $("#kategori").focus();
		   return;
		}
		
		if(aktif=="" || !aktif){
		   window.alert('Ooops!\nAktif harus dipilih');
		   $("#aktif").focus();
		   return;
		}
			
		$.ajax({
			url     : 'modules/produk_form/produk_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {      
				if(id == "") {
               		window.alert("Data Berhasil Disimpan");
               	} else {
               		window.alert("Data Berhasil Diperbaharui");
				}        
				window.location = '?p=<?php echo paramEncrypt('produk');?>';
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		});

	})
})//ready

</script>