<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------

    $query="
     SELECT  
		*
     FROM    
		produk
     ORDER BY 
		p_kode 
	 ASC ";
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '<tr><td colspan="8" class="text-center"> -- Not Found Content --</td></tr>';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
			$status_color = ($p_aktif == 'Y')? 'text-green' : 'text-red';
            $p_aktif = ($p_aktif == 'Y')? 'Ya' : 'Tidak';
            $now = date("Y-m-d");
			$date1=date_create($now);
			$date2=date_create($p_tgl_expire);
			$diff=date_diff($date1,$date2);
			$content.='
                <tr>
                    <td>'.$no.'</td>
					<td>'.$p_kode.'</td>
                    <td><b>'.ucwords($p_nama).'</b></td>
                    <td>'.rupiah($p_harga_jual).' / '.$p_satuan.'</td>
					<td>'.$p_stok.'</td>
					<td>'.$p_jenis.'</td>
					<td>'.showdt($p_tgl_masuk, 2).'</td>
					<td>'.showdt($p_tgl_expire, 2).' <b>('.$diff->format("%a hari").')</b></td>
                    <td>'.get_category($p_k_id).'</td>
                    <td class="'.$status_color.'">'.strtoupper($p_aktif).'</td>
                    <td>
						<a data-toggle="tooltip" title="Edit Produk" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('produk_form').'&id='.paramEncrypt($p_id).'" role="button"><i class="fa fa-edit"></i></a>
						<a data-toggle="tooltip" title="Delete Produk" class="btn btn-xs btn-danger" href="javascript:del('.$p_id.')" role="button"><i class="fa fa-trash-o"></i></a>
					</td>
                </tr>
            ';
        }
    }
    echo $content;
?>

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/produk_form/produk_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Gagal Dihapus");
               	} else {
               		window.alert("Data Berhasil Dihapus");
				}      
				window.location = '?p=<?php echo paramEncrypt('produk');?>';
			}
		});
	}
}
</script>