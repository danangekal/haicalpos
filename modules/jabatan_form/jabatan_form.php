<?php
defined('_IEXEC')or die('');

if($jabatan == 1) {
	$result2 = mysql_query("
		SELECT  *
		FROM
			module_menu
		WHERE
			mm_aktif = 'Y'	
	")or die(mysql_error());
} else {
	$result2 = mysql_query("
		SELECT  *
		FROM
			module_menu
		WHERE
			mm_id != 1
			&& mm_aktif = 'Y'	
	")or die(mysql_error());
}

if(!isset($_REQUEST['id'])){
	$title 			= 'New';
	$id 			= '';
	$j_nama			= '';
	$j_aktif		= '';
}else{
	$title 		= 'Edit';
	$id 		=  paramDecrypt($_REQUEST['id']);
	$query  = "
		SELECT  *
		FROM
			jabatan
		WHERE
			j_id = '$id'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
	}
}
?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $j_nama;?></h3>
	</div><!-- /.box-header -->

	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-4">
			<div class="box box-info">
			<div class="box-body">
				<input type="hidden" id="id" name="id" value="<?php echo $id; ?>"/>
				<div class="form-group">
					<label class="col-sm-3 control-label">Nama</label>
					<div class="col-sm-6">
						<input type="text" id="nama" name="nama" placeholder="Nama Jabatan" class="form-control input-sm" required="true" value="<?php echo $j_nama;?>" autofocus />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Aktif</label>
					<div class="col-sm-6">
						<select class="form-control input-sm" id="aktif" name="aktif" required="true">	
						<?php opt_aktif($j_aktif);?>
						</select>	
					</div>
				</div>
				
				<?php
					if (mysql_num_rows($result2)) {
						$no =0;
						$data = '';
						while($row = mysql_fetch_array($result2)){
							extract($row);
							$checked = cek_menu_akses($id, $mm_id)? 'checked' : '';
							$no++;
							$data .= '					
								<tr>
									<td><input type="checkbox" class="data-check" data-id="'.$mm_id.'" '.$checked.'/></td>
									<td><b>'.$no.'. '.ucwords($mm_nama).'</b></td>
							</tr>
							';
						}
					}
					echo '
					<div class="table-responsive">
					<table class="table table-bordered table-hover table-condensed">
					<thead>
						<tr class="info">
							<th><input type="checkbox" class="check-all"/></th>
							<th>HAK AKSES MENU</th>

						</tr>
					</thead>
					<tbody  style="overflow-y: scroll;">
						'.$data .'
					</tbody>
					</table>
					</div>
					';
				?>
					
				<div class="panel-footer">
					<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
				</div> 
			</div>
			</div>
			</div>
		</div>
	</form>
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('jabatan');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div>
</div> <!-- End Box-->
<script>
//fungsi seleksi
function is_selected(){
	var selected = false;
	$(".data-check").each(function(index) {
		if($(this).is(":checked")){
			selected = true;
			return false;
		}
	});
	return selected;
}

function createArrayId(){
	// this function will call after data verify, so the data should be selected before.
	var ArrayId = '';
	$('.data-check').each(function(index) {
		if($(this).is(':checked')){
			ArrayId += $(this).attr('data-id')+"|";
		}
	});
	if(ArrayId != '') ArrayId=ArrayId.slice(0,-1); // remove | char at the last
	return ArrayId;
}

// Main Function		
$(function(){ 
	$(".check-all").change(function(){
		if($(this).is(':checked')){
			$(".data-check, .check-all").prop('checked',true);
		}else{
			$(".data-check, .check-all").prop('checked',false);
		}
	})
	
	$("#btn-submit").click(function(e){
		e.preventDefault();
		var id	    = $("#id").val(),
			nama    = $("#nama").val(),
			aktif	= $("#aktif").val(),
			query   =   'type=save'+
        				'&id='+id+
						'&nama='+nama+
						'&data='+createArrayId()+
						'&aktif='+aktif;
		
		// validasi
		if(!nama){
		   window.alert('Ooops!\nNama Jabatan harus diisi');
		   $("#nama").focus();
		   return;
		}
		
		if(!is_selected()){
			alert('Menu tidak ada yang dipilih.\nAnda harus memilih minimal 1 menu.\n');
			return false;
		}
		
		if(aktif=="" || !aktif){
		   window.alert('Ooops!\nAktif harus dipilih');
		   $("#aktif").focus();
		   return;
		}
			
		$.ajax({
			url     : 'modules/jabatan_form/jabatan_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Berhasil Disimpan");
               	} else {
               		window.alert("Data Berhasil Diperbaharui");
				}      
				window.location = '?p=<?php echo paramEncrypt('jabatan');?>';
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		});

	})
})//End Main Function

</script>