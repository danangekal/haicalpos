<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
      
    $query="
     SELECT  
		*
     FROM    
		module 
     ORDER BY 
		m_id 
	 ASC ";
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '<tr><td colspan="8" class="text-center"> -- Not Found Content --</td></tr>';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
			$status_color = ($m_aktif == 'Y')? 'text-green' : 'text-red';
            $m_aktif = ($m_aktif == 'Y')? 'Ya' : 'Tidak';
            $content.='
                <tr>
                    <td>'.$no.'</td>
                    <td><b>'.ucwords($m_nama).'</b></td>
                    <td>'.$m_folder.'/'.$m_file.'</td>
					<td>'.$m_ikon.'/'.$m_warna_ikon.'</td>
                    <td class="'.$status_color.'">'.strtoupper($m_aktif).'</td>
					<td>'.get_menu($m_mm_id).'</td>
                    <td>
					<a data-toggle = "tooltip" title = "Edit Module" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('module_form').'&id='.paramEncrypt($m_id).'" role="button"><i class="fa fa-edit"></i></a>
					<a data-toggle = "tooltip" title = "Delete Module" class="btn btn-xs btn-danger" href="javascript:del('.$m_id.')" role="button"><i class="fa fa-trash-o"></i></a>
					</td>
                </tr>
            ';
        }
    }
    echo $content;
?>

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/module_form/module_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Gagal Dihapus");
               	} else {
               		window.alert("Data Berhasil Dihapus");
				}      
				window.location = '?p=<?php echo paramEncrypt('module');?>';
			}
		});
	}
}
</script>