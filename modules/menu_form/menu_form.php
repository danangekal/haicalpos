<?php
defined('_IEXEC')or die('');

if(!isset($_REQUEST['id'])){
	$title 			= 'New';
	$id 			= '';
	$mm_nama		= '';
	$mm_ikon		= '';
	$mm_warna_ikon	= '';
	$mm_aktif		= '';
}else{
	$title 		= 'Edit';
	$id 		=  paramDecrypt($_REQUEST['id']);
	$query  = "
		SELECT  *
		FROM
			module_menu
		WHERE
			mm_id = '$id'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
	}
}
?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $mm_nama;?></h3>
	</div><!-- /.box-header -->

	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-6">
			<div class="box box-info">
			<div class="box-body">
				<input type="hidden" id="id" name="id" value="<?php echo $id; ?>"/>
				<div class="form-group">
					<label class="col-sm-3 control-label">Nama</label>
					<div class="col-sm-4">
						<input type="text" id="nama" name="nama" placeholder="Nama Menu" class="form-control input-sm" required="true" value="<?php echo $mm_nama;?>" autofocus/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Ikon</label>
					<div class="col-sm-4">
						<input type="text" id="ikon" name="ikon" placeholder="Menu Ikon" class="form-control input-sm" required="true" value="<?php echo $mm_ikon;?>" />
					</div>
				</div>    
				<div class="form-group">
					<label class="col-sm-3 control-label">Warna Ikon</label>
					<div class="col-sm-4">
						<input type="text" id="warnaikon" name="warnaikon" placeholder="Warna Ikon Menu" class="form-control input-sm" value="<?php echo $mm_warna_ikon;?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Aktif</label>
					<div class="col-sm-4">
						<select class="form-control input-sm" id="aktif" name="aktif" required="true">	
						<?php opt_aktif($mm_aktif);?>
						</select>	
					</div>
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
				</div> 
			</div>
			</div>
			</div>	
		</div>
	</form>
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('menu');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div>
</div><!-- End Box-->


<script>
// Main Function		
$(function(){ 
	$("#btn-submit").click(function(){
		var id	    	= $("#id").val(),
			nama    	= $("#nama").val(),
			ikon	    = $("#ikon").val(),
			warnaikon	= $("#warnaikon").val(),
			aktif		= $("#aktif").val();
		
		// validasi
		if(!nama){
		   window.alert('Ooops!\nNama Menu harus diisi');
		   $("#nama").focus();
		   return;
		}
		
		if(!ikon){
		   window.alert('Ooops!\nIkon harus diisi');
		   $("#ikon").focus();
		   return;
		}
		
		if(aktif=="" || !aktif){
		   window.alert('Ooops!\nAktif harus dipilih');
		   $("#aktif").focus();
		   return;
		}
			
		var query   =   'type=save'+
        				'&id='+id+
						'&nama='+nama+
						'&ikon='+ikon+
						'&warnaikon='+warnaikon+
						'&aktif='+aktif;
		$.ajax({
			url     : 'modules/menu_form/menu_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Berhasil Disimpan");
               	} else {
               		window.alert("Data Berhasil Diperbaharui");
				}      
				window.location = '?p=<?php echo paramEncrypt('menu');?>';
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		});

	})
})//ready

</script>