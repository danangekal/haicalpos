<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$month  = $_POST['m'];
	$year   = $_POST['y'];
	$show	= $_POST['show'];      // jumlah record per page
	$limit  = ($show)? ' LIMIT '.$show:''; // limit pada seleksi data
    
	$WhereMonth='';
	if(intval($month) > 0){
		$WhereMonth =" && MONTH(pg_tanggal) = ".$month." ";
	}

	$WhereYear='';
	if(intval($year) > 0){
		$WhereYear=" YEAR(pg_tanggal) = ".$year." ";
	}
	
	$query="
		SELECT  
			*
		FROM    
			pengeluaran 
		WHERE
			".$WhereYear.$WhereMonth."
		ORDER BY 
			pg_kode 
		DESC ". $limit;
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '<tr><td colspan="8" class="text-center"> -- Not Found Content --</td></tr>';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
            $content.='
                <tr>
                    <td>'.$no.'</td>
                    <td><b>'.$pg_kode.'</b></td>
                    <td>'.showdt($pg_tanggal, 2).'</td>
					<td>'.$pg_nama.'</td>
					<td>'.$pg_jenis.'</td>
                    <td>'.rupiah($pg_jumlah).'</td>
					<td>'.$pg_keterangan.'</td>
					<td>'.get_fullname($pg_user_id).'</td>
                </tr>
            '; 
			//tombol edit dan delete
			//<a class="btn btn-xs btn-warning" href="?p='.paramEncrypt('sale_form').'&kd='.paramEncrypt($sl_kode).'" role="button"><i class="fa fa-edit"></i></a>
			//<a class="btn btn-xs btn-danger" href="javascript:del('.$sl_kode.')" role="button"><i class="fa fa-trash-o"></i></a>
        }
    }
    echo $content;
?>

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/pengeluaran_form/pengeluaran_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Gagal Dihapus");
               	} else {
               		window.alert("Data Berhasil Dihapus");
				}      
				window.location = '?p=<?php echo paramEncrypt('pengeluaran');?>';
			}
		});
	}
}
</script>