<?php
defined('_IEXEC')or die('');

if(!isset($_REQUEST['id'])){
	$title 			= 'New';
	$id 			= '';
	$m_nama			= '';
	$m_folder		= '';
	$m_file			= '';
	$m_ikon			= '';
	$m_warna_ikon	= '';
	$m_aktif		= '';
	$m_mm_id		= '';
}else{
	$title 		= 'Edit';
	$id 		=  paramDecrypt($_REQUEST['id']);
	$query  = "
		SELECT  *
		FROM
			module
		WHERE
			m_id = '$id'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));
	}
}
?>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $m_nama;?></h3>
	</div><!-- /.box-header -->

	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-6">
			<div class="box box-info">
			<div class="box-body">				
				<input type="hidden" id="id" name="id" value="<?php echo $id; ?>"/>
				<div class="form-group">
					<label class="col-sm-3 control-label">Nama</label>
					<div class="col-sm-6">
						<input type="text" id="nama" name="nama" placeholder="Nama Module" class="form-control input-sm" required="true" value="<?php echo $m_nama;?>" autofocus />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Folder</label>
					<div class="col-sm-6">
						<input type="text" id="folder" name="folder" placeholder="Folder Module" class="form-control input-sm" required="true" value="<?php echo $m_folder;?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">File</label>
					<div class="col-sm-6">
						<input type="text" id="file" name="file" placeholder="File Module" class="form-control input-sm" required="true" value="<?php echo $m_file;?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Ikon</label>
					<div class="col-sm-6">
						<input type="text" id="ikon" name="ikon" placeholder="Ikon Module" class="form-control input-sm" required="true" value="<?php echo $m_ikon;?>" />
					</div>
				</div>    
				<div class="form-group">
					<label class="col-sm-3 control-label">Warna Ikon</label>
					<div class="col-sm-6">
						<input type="text" id="warnaikon" name="warnaikon" placeholder="Warna Ikon Module" class="form-control input-sm" value="<?php echo $m_warna_ikon;?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Aktif</label>
					<div class="col-sm-6">
						<select class="form-control input-sm" id="aktif" name="aktif" required="true">	
						<?php opt_aktif($m_aktif);?>
						</select>	
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Menu</label>
					<div class="col-sm-6">
						<select class="form-control input-sm" id="menu" name="menu" required="true">	
						<?php opt_menu($m_mm_id);?>
						</select>	
					</div>
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
				</div>
			</div>
			</div>
			</div>
		</div>
	</form>
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('module');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div>
</div><!-- End Box-->

<script>
// Main Function		
$(function(){ 
	$("#btn-submit").click(function(){
		var id	    	= $("#id").val(),
			nama    	= $("#nama").val(),
			folder    	= $("#folder").val(),
			file    	= $("#file").val(),
			ikon	    = $("#ikon").val(),
			warnaikon	= $("#warnaikon").val(),
			aktif		= $("#aktif").val(),
			menu	    = $("#menu").val();
		
		// validasi
		if(!nama){
		   window.alert('Ooops!\nNama Module harus diisi');
		   $("#nama").focus();
		   return;
		}
		
		if(!folder){
		   window.alert('Ooops!\nFolder Module harus diisi');
		   $("#folder").focus();
		   return;
		}
		
		if(!file){
		   window.alert('Ooops!\File Module harus diisi');
		   $("#file").focus();
		   return;
		}
		
		if(!ikon){
		   window.alert('Ooops!\nIkon harus diisi');
		   $("#ikon").focus();
		   return;
		}
		
		if(aktif=="" || !aktif){
		   window.alert('Ooops!\nAktif harus dipilih');
		   $("#aktif").focus();
		   return;
		}
		
		if(menu==0 || !menu){
		   window.alert('Ooops!\nMenu harus dipilih');
		   $("#menu").focus();
		   return;
		}
			
		var query   =   'type=save'+
        				'&id='+id+
						'&nama='+nama+
						'&folder='+folder+
						'&file='+file+
						'&ikon='+ikon+
						'&warnaikon='+warnaikon+
						'&aktif='+aktif+
						'&menu='+menu;
		$.ajax({
			url     : 'modules/module_form/module_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Berhasil Disimpan");
               	} else {
               		window.alert("Data Berhasil Diperbaharui");
				}      
				window.location = '?p=<?php echo paramEncrypt('module');?>';
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		});

	})
})//ready

</script>