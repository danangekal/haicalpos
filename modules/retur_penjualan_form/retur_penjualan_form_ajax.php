<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$type				= $_POST['type'];
	
	//data retur penjualan	
	$rpj_kode			= $_POST['koderetur'];
	$rpj_pj_kode		= $_POST['kodejual'];
	$rpj_jumlah			= $_POST['jtotal'];
	$rpj_potongan		= $_POST['potongan'];
	$rpj_total			= $_POST['gtotal'];
	$rpj_user_id		= $_POST['userid'];
		
	//data retur penjualan detail
	$rpjd_id			= $_POST['rpjdid'];
	$rpjd_pjd_id		= $_POST['pjdid'];
	$rpjd_p_k_id		= $_POST['kategori'];
	$rpjd_p_kode		= $_POST['kodeprod'];
	$rpjd_p_nama		= $_POST['nama'];
	$rpjd_p_harga_jual	= $_POST['hargajual'];
	$rpjd_p_harga_beli	= $_POST['hargabeli'];
	$rpjd_p_satuan		= $_POST['satuan'];
	$rpjd_beli			= $_POST['beli'];	
	$rpjd_retur			= $_POST['retur'];
	$rpjd_jumlah		= $_POST['jumlah'];
	
	//data produk
	$p_stok				= $_POST['stok'];
	
	switch ($type) {
		case 'save':
				$r = mysql_query("
					SELECT  *
					FROM
						retur_penjualan
					WHERE
						rpj_kode = '$rpj_kode'
				")or die(mysql_error());
				if(mysql_num_rows($r)){
					//Update ke retur penjualan
					mysql_query("
						UPDATE  
							retur_penjualan 
						SET
							rpj_tanggal		= NOW(),
							rpj_potongan	= '$rpj_potongan',
							rpj_total		= '$rpj_total',
							rpj_user_id		= '$rpj_user_id'
						WHERE
							rpj_kode		= '$rpj_kode'
					")or die(mysql_error());
					
					//cari saldo keuangan terakhir
					$result2 = mysql_query("
						SELECT 
							keu_saldo
						FROM 
							keuangan 
						ORDER BY keu_id DESC
						LIMIT 1 
					")or die(mysql_error());
					if(mysql_num_rows($result2)){
						extract(mysql_fetch_assoc($result2));	
					}
					
					$saldo = $keu_saldo-$rpj_total;
					
					//simpan ke rekening keuangan
					mysql_query("
						INSERT INTO 
							keuangan 
						SET
							keu_tanggal			= NOW(),
							keu_kode			= '$rpj_kode',
							keu_transaksi		= 'Retur Penjualan',
							keu_mutasi_debet	= '$rpj_total',
							keu_saldo			= '$saldo',
							keu_keterangan		= 'Pengurangan',
							keu_user_id			= '$rpj_user_id'
					")or die(mysql_error());
					
					//cari saldo keuangan terakhir company
					$result3 = mysql_query("
						SELECT 
							c_keuangan
						FROM 
							company
						WHERE
							c_id =1
					")or die(mysql_error());
					if(mysql_num_rows($result3)){
						extract(mysql_fetch_assoc($result3));	
					}
					
					$keuangan = $c_keuangan-$rpj_total;
					
					//Update ke saldo keuangan company
					mysql_query("
						UPDATE  
							company
						SET
							c_keuangan	= '$keuangan'
						WHERE
							c_id		= 1
					")or die(mysql_error());
				}
		break;
				
		case 'save-item':
			$r = mysql_query("
				SELECT  *
				FROM
					retur_penjualan
				WHERE
					rpj_kode = '$rpj_kode'
			")or die(mysql_error());
			if(!mysql_num_rows($r)){				
				$stok = $p_stok+$rpjd_retur; 
				
				// update stok produk
				mysql_query("
					UPDATE produk SET p_stok='$stok' WHERE p_kode='$rpjd_p_kode'
				")or die(mysql_error());
				
				//simpan ke retur penjualan detail
				mysql_query("
					INSERT INTO 
						retur_penjualan_detail 
					SET
						rpjd_rpj_kode		= '$rpj_kode',
						rpjd_pjd_id			= '$rpjd_pjd_id',
						rpjd_p_k_id			= '$rpjd_p_k_id',
						rpjd_p_kode			= '$rpjd_p_kode',
						rpjd_p_nama			= '$rpjd_p_nama',
						rpjd_p_harga_jual	= '$rpjd_p_harga_jual',
						rpjd_p_harga_beli	= '$rpjd_p_harga_beli',
						rpjd_p_satuan		= '$rpjd_p_satuan',
						rpjd_beli			= '$rpjd_beli',
						rpjd_retur			= '$rpjd_retur',
						rpjd_jumlah			= '$rpjd_jumlah'
				")or die(mysql_error());
					
				//cari jumlah total retur penjualan detail
				$result3 = mysql_query("
					SELECT 
						SUM(rpjd_jumlah) as jtotal
					FROM 
						retur_penjualan_detail 
					WHERE 
						rpjd_rpj_kode = '$rpj_kode' 
				")or die(mysql_error());
				if(mysql_num_rows($result3)){
					extract(mysql_fetch_assoc($result3));	
				}
				
				//simpan ke penjualan
				mysql_query("
					INSERT INTO 
						retur_penjualan 
					SET
						rpj_kode		= '$rpj_kode',
						rpj_pj_kode		= '$rpj_pj_kode',
						rpj_tanggal		= NOW(),
						rpj_jumlah		= '$jtotal',
						rpj_user_id		= '$rpj_user_id'
				")or die(mysql_error());
			} else { // kalau kode retur sudah ada
				$stok = $p_stok+$rpjd_retur; 
				
				// update stok produk
				mysql_query("
					UPDATE produk SET p_stok='$stok' WHERE p_kode='$rpjd_p_kode'
				")or die(mysql_error());
				
				//simpan ke retur penjualan detail
				mysql_query("
					INSERT INTO 
						retur_penjualan_detail 
					SET
						rpjd_rpj_kode		= '$rpj_kode',
						rpjd_pjd_id			= '$rpjd_pjd_id',
						rpjd_p_k_id			= '$rpjd_p_k_id',
						rpjd_p_kode			= '$rpjd_p_kode',
						rpjd_p_nama			= '$rpjd_p_nama',
						rpjd_p_harga_jual	= '$rpjd_p_harga_jual',
						rpjd_p_harga_beli	= '$rpjd_p_harga_beli',
						rpjd_p_satuan		= '$rpjd_p_satuan',
						rpjd_beli			= '$rpjd_beli',
						rpjd_retur			= '$rpjd_retur',
						rpjd_jumlah			= '$rpjd_jumlah'
				")or die(mysql_error());
					
				//cari jumlah total retur penjualan detail
				$result3 = mysql_query("
					SELECT 
						SUM(rpjd_jumlah) as jtotal
					FROM 
						retur_penjualan_detail 
					WHERE 
						rpjd_rpj_kode = '$rpj_kode' 
				")or die(mysql_error());
				if(mysql_num_rows($result3)){
					extract(mysql_fetch_assoc($result3));	
				}
				
				//simpan ke retur penjualan
				mysql_query("
					UPDATE 
						retur_penjualan 
					SET
						rpj_pj_kode		= '$rpj_pj_kode',
						rpj_tanggal		= NOW(),
						rpj_jumlah		= '$jtotal',
						rpj_user_id		= '$rpj_user_id'
					WHERE
						rpj_kode		= '$rpj_kode'
				")or die(mysql_error());
			} 	
		break;
		
		case 'delete-item':						
			//cari total yang dikurangi/dihapus dan kode penjualan
			$result = mysql_query("
				SELECT 
					rpjd_p_kode,
					rpjd_retur as retur_awal,
					rpjd_jumlah 
				FROM 
					retur_penjualan_detail 
				WHERE 
					rpjd_id = '$rpjd_id' 
			")or die(mysql_error());
			if(mysql_num_rows($result)){
				extract(mysql_fetch_assoc($result));	
			}
			
			//cari jumlah total dari penjualan
			$result2 = mysql_query("
				SELECT 
					rpj_jumlah 
				FROM 
					retur_penjualan 
				WHERE 
					rpj_kode = '$rpj_kode'
			")or die(mysql_error());
			if(mysql_num_rows($result2)){
				extract(mysql_fetch_assoc($result2));	
			}
			
			$sisajumlah = $rpj_jumlah-$rpjd_jumlah;
			
			//update ke retur penjualan
			mysql_query("
				UPDATE  
					retur_penjualan 
				SET
					rpj_tanggal		= NOW(),
					rpj_jumlah		= '$sisajumlah',
					rpj_user_id		= '$rpj_user_id'
				WHERE
					rpj_kode		= '$rpj_kode'
			")or die(mysql_error());
			
			//cari stok produk terakhir
			$result3 = mysql_query("
				SELECT 
					p_stok as stok_akhir
				FROM 
					produk 
				WHERE 
					p_kode = '$rpjd_p_kode'
			")or die(mysql_error());
			if(mysql_num_rows($result3)){
				extract(mysql_fetch_assoc($result3));	
			}
			
			$stok = $stok_akhir-$retur_awal; 
			
			// update stok produk
			mysql_query("
				UPDATE produk SET p_stok='$stok' WHERE p_kode='$rpjd_p_kode'
			")or die(mysql_error());
			
			//hapus penjualan detail
			mysql_query("
				DELETE FROM retur_penjualan_detail WHERE rpjd_id='$rpjd_id'
			")or die(mysql_error());		
			
			//cari retur penjualan ada atau tidak
			$result4 = mysql_query("
				SELECT 
					*
				FROM 
					retur_penjualan_detail 
				WHERE 
					rpjd_rpj_kode = '$rpj_kode'
			")or die(mysql_error());
			if(!mysql_num_rows($result4)){
				//hapus penjualan detail
				mysql_query("
					DELETE FROM retur_penjualan WHERE rpj_kode='$rpj_kode'
				")or die(mysql_error());
			}
		break;
		
	}
?>