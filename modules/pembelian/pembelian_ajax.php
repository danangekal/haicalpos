<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$month  = $_POST['m'];
	$year   = $_POST['y'];
	$show	= $_POST['show'];      // jumlah record per page
	$limit  = ($show)? ' LIMIT '.$show:''; // limit pada seleksi data
    
	$WhereMonth='';
	if(intval($month) > 0){
		$WhereMonth =" && MONTH(pb_tanggal) = ".$month." ";
	}

	$WhereYear='';
	if(intval($year) > 0){
		$WhereYear=" YEAR(pb_tanggal) = ".$year." ";
	}

	
	$query="
		SELECT  
			*
		FROM    
			pembelian 
		WHERE
			".$WhereYear.$WhereMonth."
		ORDER BY 
			pb_kode 
		DESC ". $limit;
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '<tr><td colspan="10" class="text-center"> -- Not Found Content --</td></tr>';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
            $content.='
                <tr>
                    <td>'.$no.'</td>
                    <td><b>'.$pb_kode.'</b></td>
                    <td>'.showdt($pb_tanggal, 2).'</td>
					<td>'.$pb_suplier.'</td>
					<td>'.$pb_nota.'</td>
					<td>'.rupiah($pb_total).'</td>
					<td>'.rupiah($pb_bayar).'</td>
					<td>'.rupiah($pb_sisa_bayar).'</td>
					<td>'.get_fullname($pb_user_id).'</td>
                    ';
			
				//cari retur penjualan sudah ada atau belum
				$result2 = mysql_query("
					SELECT 
						*
					FROM 
						retur_pembelian
					WHERE rpb_pb_kode = '$pb_kode' 
				")or die(mysql_error());
				if(!mysql_num_rows($result2)){					
					if($pb_bayar==0 && $pb_sisa_bayar==0) {
						$content .='
								<td>
									<a data-toggle="tooltip" title="Edit Pembelian" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('pembelian_form').'&kd='.paramEncrypt($pb_kode).'&pbdid='.paramEncrypt(0).'" role="button"><i class="fa fa-edit"></i></a>
								</td>
							</tr>
						'; 
					} elseif($pb_status==0) {
						$content .='
								<td>
									<a data-toggle="tooltip" title="Lunasi Pembelian" class="btn btn-xs btn-primary" href="#" onclick="lunasi(\'' .$pb_kode. '\', '.$pb_total.', '.$pb_sisa_bayar.', '.$pb_user_id.')" role="button"><i class="fa fa-check-circle"></i></a>
								</td>
							</tr>
						'; 
					} else {
						$content .='
								<td>
									...
								</td>
							</tr>
						'; 
					}		
				} else {
					/*
					extract(mysql_fetch_assoc($result2));
					
					$content.= '
							<td>
								<a data-toggle="tooltip" title="Retur Pembelian" class="btn btn-xs btn-danger" href="?p='.paramEncrypt('retur_pembelian_form').'&kdpb='.paramEncrypt($pb_kode).'" role="button"><i class="fa fa-hand-lizard-o"></i></a>
							</td>
						</tr>
					';
					*/
				}					 
			//tombol delete
			//<a class="btn btn-xs btn-danger" href="javascript:del('.$pb_kode.')" role="button"><i class="fa fa-trash-o"></i></a>
        }
    }
    echo $content;
?>

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/pembelian_form/pembelian_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
               	window.alert("Data Berhasil Dihapus");    
				window.location = '?p=<?php echo paramEncrypt('pembelian');?>';
			}
		});
	}
}

function lunasi(kode, total, sisabayar, userid) {
	var query		= 'type=lunasi'+
					  '&kodebeli='+kode+
					  '&gtotal='+total+
					  '&sisabayar='+sisabayar+
					  '&userid='+userid;

	$.ajax({
		url     : 'modules/pembelian_form/pembelian_form_ajax.php',
		type    : 'post',
		data    : query,
		cache   : false,
		//dataType:'json',
		success : function(data) {
			window.alert("Data Berhasil Dilunasi");    
			window.location = '?p=<?php echo paramEncrypt('pembelian');?>';
		}
	}); 
}
</script>