<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$sdate		= $_POST['sdate'];
	$edate		= $_POST['edate'];
	
	$q1 = mysql_query("
		SELECT SUM(pj_total) as pj_total
		FROM
			penjualan
		WHERE
			pj_bayar>0
			&& pj_tanggal	>= '$sdate'
			&& pj_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q1)){
		extract(mysql_fetch_array($q1));
	} else {
		$pj_total = 0;
	}
	
	$q2a = mysql_query("
		SELECT SUM(pb_bayar) as bayar1
		FROM
			pembelian
		WHERE
			pb_status = 1
			&& pb_sisa_bayar = 0
			&& pb_tanggal	>= '$sdate'
			&& pb_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q2a)){
		extract(mysql_fetch_array($q2a));
	} else {
		$bayar1 = 0;
	}

	$q2b = mysql_query("
		SELECT SUM(pb_sisa_bayar) as bayar2
		FROM
			pembelian
		WHERE
			pb_status = 1
			&& pb_sisa_bayar > 0
			&& pb_tanggal	>= '$sdate'
			&& pb_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q2b)){
		extract(mysql_fetch_array($q2b));
	} else {
		$bayar2 = 0;
	}

	$q2c = mysql_query("
		SELECT SUM(pb_bayar) as bayar3
		FROM
			pembelian
		WHERE
			pb_status = 0
			&& pb_sisa_bayar > 0
			&& pb_tanggal	>= '$sdate'
			&& pb_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q2c)){
		extract(mysql_fetch_array($q2c));
	} else {
		$bayar3 = 0;
	}

	$pb_bayar = $bayar1+$bayar2+$bayar3;

	$q3 = mysql_query("
		SELECT SUM(pn_total_bayar) as pn_total_bayar
		FROM
			penitipan
		WHERE
			pn_status = 1
			&& pn_tanggal	>= '$sdate'
			&& pn_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q3)){
		extract(mysql_fetch_array($q3));
	} else {
		$pn_total_bayar = 0;
	}

	$q4 = mysql_query("
		SELECT SUM(pg_jumlah) as pg_jumlah
		FROM
			pengeluaran
		WHERE
			pg_tanggal	>= '$sdate'
			&& pg_tanggal	<= '$edate'
	")or die(mysql_error());
	if(mysql_num_rows($q4)){
		extract(mysql_fetch_array($q4));
	} else {
		$pg_jumlah = 0;
	}

	$tpengeluaran	= $pb_bayar + $pn_total_bayar + $pg_jumlah;
	$saldo			= $pj_total - $tpengeluaran;
	
	$content = '';
	$content .= '
	<h5>Periode Laba Rugi '.showdt($sdate, 2).' - '.showdt($edate, 2).'</h5>
	<div class="table-responsive">
	<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr class="success">
			<th colspan="2">Pendapatan Usaha</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Penjualan</td>
			<td>'.rupiah($pj_total).'</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th>Jumlah Pendapatan Usaha</th>
			<th>'.rupiah($pj_total).'</th>
		</tr>
	</tfoot>
	</table>
	<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr class="success">
			<th colspan="2">Pengeluaran Operasional</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Pembelian</td>
			<td>'.rupiah($pb_bayar).'</td>
		</tr>
		<tr>
			<td>Pembayaran Penitipan</td>
			<td>'.rupiah($pn_total_bayar).'</td>
		</tr>
		<tr>
			<td>Pengeluaran Umum</td>
			<td>'.rupiah($pg_jumlah).'</td>
		</tr>
		<tr>
			<td>Pembelian + Pembayaran Penitipan + Pengeluaran Umum ('.rupiah($pb_bayar).' + '.rupiah($pn_total_bayar).' + '.rupiah($pg_jumlah).')</td>
			<td>'.rupiah($tpengeluaran).'</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th>Jumlah Pengeluaran Operasional</th>
			<th>'.rupiah($tpengeluaran).'</th>
		</tr>
	</tfoot>
	</table>
	<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr class="success">
			<th colspan="2">Saldo Pendapatan</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Pendapatan Usaha - Pengeluaran Operasional ('.rupiah($pj_total).' - '.rupiah($tpengeluaran).')</td>
			<td>'.rupiah($saldo).'</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th>Jumlah Saldo Pendapatan</th>
			<th>'.rupiah($saldo).'</th>
		</tr>
	</tfoot>
	</table>
	</div>
	<br>
	<br>
	<a data-toggle="tooltip" title="Export Laporan Laba Rugi (PDF)" class="btn btn-danger" href="page.php?p='.paramEncrypt('lap_laba_rugi_pdf').'&sdt='.paramEncrypt($sdate).'&edt='.paramEncrypt($edate).'" target="_blank" role="button"><i class="fa fa-download"></i> PDF</a>	
	';
    echo $content;
?>