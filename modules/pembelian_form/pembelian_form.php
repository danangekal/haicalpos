<?php
defined('_IEXEC')or die('');

if(!isset($_REQUEST['kd']) && !isset($_REQUEST['pbdid'])){
	$title 			= 'New';
	
	//data pembelian
	$pb_kode			= gen_pbid();
	$pb_suplier			= '';
	$pb_nota			= '';
	$pb_tanggal			= '';
	$pb_jumlah			= '';
	$pb_persen_diskon	= 0;
	$pb_diskon			= '';
	$pb_persen_ppn		= 0;
	$pb_ppn				= '';
	$pb_total			= '';
	$pb_bayar			= '';
	$pb_sisa_bayar		= '';
	
	$cari			= '';
	$idp			= '';
	$kategori		= '';
	$kodep			= '';
	$namap			= '';
	$harga_beli		= '';
	$satuan			= '';
	$expire			= '';
	$beli			= '';
	$jumlah			= '';
	
	$stok			= '';
	
}else{
	$title 		= 'Edit';
	$pb_kode	= paramDecrypt($_REQUEST['kd']);
	$pbd_id		= paramDecrypt($_REQUEST['pbdid']);
	
	//data pembelian
	$query  = "
		SELECT  
			*
		FROM
			pembelian
		WHERE
			pb_kode = '$pb_kode'
	";
	$result = mysql_query($query)or die(mysql_error());
	if(mysql_num_rows($result)){
		extract(mysql_fetch_assoc($result));		
	}
	
	//data item pembelian detail
	if($pbd_id > 0){
		$query3  = "
			SELECT  
				pbd_id as idp, 
				pbd_p_k_id as kategori,
				pbd_p_kode as kodep, 
				pbd_p_nama as namap, 
				pbd_p_harga_beli as harga_beli,
				pbd_p_satuan as satuan,
				pbd_p_tgl_expire as expire,
				pbd_beli as beli,
				pbd_jumlah as jumlah,
				p_stok as stok
			FROM
				pembelian_detail
			INNER JOIN
				produk
			ON
				pbd_p_kode = p_kode
			WHERE
				pbd_id = '$pbd_id'
		";
		$result3 = mysql_query($query3)or die(mysql_error());
		if(mysql_num_rows($result3)){
			extract(mysql_fetch_assoc($result3));
			$cari = $kodep.' - '.ucwords($namap).' - '.rupiah($harga_beli).' / '.$satuan;
		}
	} else {
		$cari			= '';
		$idp			= '';
		$kategori		= '';
		$kodep			= '';
		$namap			= '';
		$harga_beli		= '';
		$satuan			= '';
		$expire			= '';
		$beli			= '';
		$jumlah			= '';
		
		$stok			= '';
	}	
}
	// loading data untuk auto complete
    $q = mysql_query("
        SELECT 
			*
        FROM 
			produk
        WHERE 
			p_aktif = 'Y'
			&& p_jenis = 'CASH'
        ORDER BY
			p_id ASC         
    ")or die(mysql_error());
    
    $data       = '[';
    $num_data   = mysql_num_rows($q);
    $no         = 0;
    while($r = mysql_fetch_array($q)){
        $no++;
        extract($r);

        $data .= '
            {
                "value":"'.$p_kode.' - '.ucwords($p_nama).' - '.rupiah($p_harga_beli).' / '.$p_satuan.'",
                "kode":"'.$p_kode.'",
                "nama":"'.ucwords($p_nama).'",
				"hargabeli":"'.$p_harga_beli.'",
				"satuan":"'.$p_satuan.'",
				"stok":"'.$p_stok.'",
				"expire":"'.$p_tgl_expire.'",
				"kategori":"'.$p_k_id.'"
            }    
        ';
               
        if($no < $num_data){
            $data .=',';    
        }
    }
    $data .=']';
?>

<style>
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>

<div class="box box-solid box-warning">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo $title;?> <?php echo ucwords($p);?> <?php echo $pb_kode;?></h3>
	</div><!-- /.box-header utama -->
	<form class="form-horizontal">
		<div class="box-body">
			<div class="col-md-8"><!-- div col md 8 data pembelian -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Data Pembelian</h3>
			</div><!-- /.box-header data pembelian -->
			<div class="box-body">	
				<div class="form-group">
					<label class="col-sm-3 control-label">Kode Pembelian</label>
					<div class="col-sm-4">
						<input type="text" id="kodebeli" name="kodebeli" class="form-control input-sm" value="<?php echo $pb_kode;?>" readonly />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Suplier</label>
					<div class="col-sm-4">
						<input type="text" id="suplier" name="suplier" class="form-control input-sm" value="<?php echo $pb_suplier;?>" autofocus />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Nota Pembelian</label>
					<div class="col-sm-4">
						<input type="text" id="nota" name="nota" class="form-control input-sm" value="<?php echo $pb_nota;?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Cari Product</label>
					<div class="col-sm-8">
						<input type="text" id="cari" name="cari" class="form-control input-sm" onclick="clearInput(this)" placeholder="Nama Product" value="<?php echo $cari;?>" />
					</div>
				</div>
			</div><!-- end div box-body data pembelian-->
			</div><!-- end div box data pembelian -->
			</div><!-- end div col md 8 data pembelian -->
			
			<div class="col-md-12"><!-- div col md 12 item pembelian -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Item Pembelian Product</h3>
			</div><!-- /.box-header item pembelian -->
			<div class="box-body">
				<div class="form-group">
					<div class="col-sm-14">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" style="margin-bottom:2px;">
							<thead>
								<tr class="success">
								<th></th>
								<th></th>
								<th>Kode Product</th>
								<th>Nama Product</th>
								<th>Harga Product/Satuan</th>
								<th></th>
								<th>Tanggal Expire</th>
								<th>Beli Product</th>
								<th>Jumlah Harga</th>
								<th></th>
								</tr>
								<tr>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="hidden" id="pbd_id" name="pbd_id" value="<?php echo $idp;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="hidden" id="kategori" name="kategori" value="<?php echo $kategori;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="text" id="kodeprod" name="kodeprod" value="<?php echo $kodep;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm" type="text" id="nama" name="nama" value="<?php echo $namap;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><span class="input-group-addon">Rp</span><input class="form-control input-sm number-mask" type="text" id="hargabeli" name="hargabeli" value="<?php echo $harga_beli;?>" readonly /><span class="input-group-addon">/</span><span class="input-group-addon" id="satuan" name="satuan"><?php echo $satuan;?></span></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="hidden" id="stok" name="stok" value="<?php echo $stok;?>" readonly /></div></th>
								<th><div class="input-group input-group-sm"><input class="datepicker" type="text" id="tglexpire" name="tglexpire" placeholder="yyyy-mm-dd" value="<?php echo $expire;?>" /></div></th>
								<th><div class="input-group input-group-sm"><input class="form-control input-sm number-mask" type="text" id="beli" name="" value="<?php echo $expire;?>" /></div></th>
								<th><div class="input-group input-group-sm"><span class="input-group-addon">Rp</span><input class="form-control input-sm number-mask" type="text" id="jumlah" name="jumlah" value="<?php echo $jumlah;?>" readonly /></div></th>
								<th>
									<?php 
									if($idp==0 || !$idp){
										echo '
											<button type="button" class="btn btn-success btn-xs" id="btn-add" name="btn-add">Add</button>
											<button type="button" class="btn btn-danger btn-xs" id="btn-del" name="btn-del">Remove</button>
										';
									} else {
										echo '
											<button type="button" class="btn btn-success btn-xs" id="btn-add" name="btn-add">Update</button>
											<button type="button" class="btn btn-danger btn-xs" id="btn-can" name="btn-can">Cancel</button>
										';
									}
									?>
								</th>     
								<tr>
							</thead>
							<tbody id="item-pembelian">
							</tbody>
						</table>
					</div>	
					</div>
				</div>
			</div><!-- end div box body item pembelian -->
			</div><!-- end div box item pembelian -->
			</div><!-- end div col md 12 item pembelian -->
			
			<div class="col-md-12"><!-- div col md 12 daftar pembelian -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Daftar Pembelian Product</h3>
			</div><!-- /.box-header item pembelian -->
			<div class="box-body">
				<div class="form-group">
					<div class="col-sm-14">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover" style="margin-bottom:2px;">
								<tr class="success">
								<th>No</th>
								<th>Kode Product</th>
								<th>Nama Product</th>
								<th>Harga Product/Satuan</th>
								<th>Tanggal Expire</th>
								<th>Beli Product</th>
								<th>Jumlah</th>
								<th>...</th>
								</tr>
							<tbody>
								<?php
									//data pembelian detail
									$query2  = "
										SELECT  
											*
										FROM
											pembelian_detail
										WHERE
											pbd_pb_kode = '$pb_kode'
									";
									$result2 = mysql_query($query2)or die(mysql_error());
	
									if(mysql_num_rows($result2)>0){
										$no = 0;
										while($r = mysql_fetch_array($result2)){
											extract($r);
											$no++;
											echo '
												<tr>
													<td>'.$no.'</td>
													<td>'.$pbd_p_kode.'</td>
													<td>'.$pbd_p_nama.'</td>
													<td>'.rupiah($pbd_p_harga_beli).' / '.$pbd_p_satuan.'</td>
													<td>'.showdt($pbd_p_tgl_expire, 2).'</td>
													<td>'.$pbd_beli.'</td>
													<td>'.rupiah($pbd_jumlah).'</td>
													<td>
														<a class="btn btn-xs btn-warning" data-toggle="tooltip" title="Edit Item Pembelian" href="?p='.paramEncrypt('pembelian_form').'&kd='.paramEncrypt($pb_kode).'&pbdid='.paramEncrypt($pbd_id).'"><i class="fa fa-edit"></i></a>
														<a class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete Item Pembelian" href="javascript:ItemDel('.$pbd_id.')" role="button"><i class="fa fa-trash-o"></i></a>
													</td>
												</tr>
											';
										}
									} else {
										echo '<tr><td colspan="8" class="text-center">Data Kosong</td></tr>';
									}									
								?>
							</tbody>							
						</table>
					</div>	
					</div>
				</div>
			</div><!-- end div box body daftar pembelian -->
			</div><!-- end div box daftar pembelian -->
			</div><!-- end div col md 12 daftar pembelian -->
			
			<div class="col-md-6"><!-- div col md 4 detail pembayaran -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Detail Pembayaran</h3>
			</div><!-- /.box-header detail pembayaran -->
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-3 control-label">Jumlah Total</label>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="jtotal" name="jtotal"  class="form-control input-sm number-mask" value="<?php echo $pb_jumlah;?>" readonly />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Diskon</label>
					<div class="col-sm-3">
						<div class="input-group input-group-sm">
							<select class="form-control input-sm" id="persendiskon" name="persendiskon" required="true">	
							<?php opt_diskon($pb_persen_diskon);?>
							</select><span class="input-group-addon">%</span>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="diskon" name="diskon" class="form-control input-sm number-mask" value="<?php echo $pb_diskon;?>" readonly />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">PPn</label>
					<div class="col-sm-3">
						<div class="input-group input-group-sm">
							<select class="form-control input-sm" id="persenppn" name="persenppn" required="true">	
							<?php opt_ppn($pb_persen_ppn);?>
							</select><span class="input-group-addon">%</span>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="ppn" name="ppn" class="form-control input-sm number-mask" value="<?php echo $pb_ppn;?>" readonly />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Grand Total</label>
					<div class="col-sm-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="gtotal" name="gtotal" class="form-control input-sm number-mask" value="<?php echo $pb_total;?>" readonly />
						</div>
					</div>
				</div>
			</div><!-- end div box-body detail pembayaran -->
			</div><!-- end div box detail pembayaran -->
			</div><!-- end div col md 6 detail pembayaran -->
			
			<div class="col-md-4"><!-- div col md 4 pembayaran -->
			<div class="box box-info">
			<div class="box-header">
			  <h3 class="box-title">Pembayaran</h3>
			</div><!-- /.box-header detail pembayaran -->
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-4 control-label">Bayar</label>
					<div class="col-sm-6">
						<div class="input-group input-group-sm">	
							<span class="input-group-addon">Rp</span>
							<input type="text" id="bayar" name="bayar" class="form-control input-sm number-mask" value="<?php echo $pb_bayar;?>" placeholder="0"/>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Sisa Bayar</label>
					<div class="col-sm-6">
						<div class="input-group input-group-sm">
							<span class="input-group-addon">Rp</span>
							<input type="text" id="sisabayar" name="sisabayar" class="form-control input-sm number-mask" value="<?php echo $pb_sisa_bayar;?>" readonly />
						</div>
					</div>
				</div>
			</div><!-- end div box-body pembayaran -->
			</div><!-- end div box pembayaran -->
			</div><!-- end div col md 4 pembayaran -->
			
			<div class="panel-footer">
				<button type="button" class="btn btn-default btn-lg btn-block btn-danger" id="btn-submit">Submit</button>
			</div> 
			
		</div><!-- end div body utama -->
	</form><!-- end form-->
	<div class="box-footer clearfix">
		<a class="btn btn-default btn-lg btn-success" href="?p=<?php echo paramEncrypt('pembelian');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back</a>
	</div> <!-- end div box-footer utama -->
</div> <!-- End box utama-->

<script>
//fungsi hapus item pembelian
function ItemDel(id) {
	var id		= id,
		kodebeli= $("#kodebeli").val(),
		query	= 'type=delete-item'+
				  '&pbdid='+id;
	var pilih	= confirm('Yakin akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/pembelian_form/pembelian_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
               	//window.alert("Data Berhasil Dihapus");      
				location.reload();
			}
		});
	}
}

// Main Function		
$(function(){ 	
	var	userid  	= <?php echo $userid;?>,
		hasil		= <?php echo $data;?>;
	//initial
    set_mask();
	
	$(".datepicker").datepicker({
		format: 'yyyy-mm-dd'
	})
	
	$('#cari').autocomplete({
		lookup: hasil,
		onSelect: function (suggestion) {			
				$("#kategori").val(suggestion.kategori);
				$("#kodeprod").val(suggestion.kode);
				$("#nama").val(suggestion.nama);
				$("#hargabeli").val(suggestion.hargabeli);
				$("#stok").val(suggestion.stok);
				$("#tglexpire").val(suggestion.expire);
				$("#beli").val(1);
				$("#satuan").text(suggestion.satuan);
			
				var hargabeli	= toNumber($("#hargabeli").val()),
					beli   		= toNumber($("#beli").val()),
					jumlah 		= hargabeli*(beli/1);
					
				$("#jumlah").val(jumlah);
				$("#beli").focus();				
		}
	});
		
	$("#beli").keyup(function(){
		var hargabeli	= toNumber($("#hargabeli").val()),
			beli   		= toNumber($("#beli").val()),
			stok   		= toNumber($("#stok").val()),
			jumlah		= hargabeli*(beli/1);
		
		$("#jumlah").val(jumlah);
	});
	
	$("#btn-del").click(function(e){
        e.preventDefault();
		$("#kategori, #kodeprod, #nama, #hargabeli, #stok, #tglexpire, #beli, #jumlah, #cari").val('');
		$("#cari").focus();
    })
	
	$("#btn-can").click(function(e){
        e.preventDefault();
		window.location = '?p=<?php echo paramEncrypt('pembelian_form');?>&kd=<?php echo paramEncrypt($pb_kode);?>&pbdid=<?php echo paramEncrypt(0);?>';
    })
	
	$("#btn-add").click(function(e){
		e.preventDefault();
		
		var kodebeli		= $("#kodebeli").val(),
			suplier			= $("#suplier").val(),
			nota			= $("#nota").val(),
			pbdid			= $("#pbd_id").val(),
			kategori		= $("#kategori").val(),
			kodeprod		= $("#kodeprod").val(),
			nama   			= $("#nama").val(),
			hargabeli		= toNumber($("#hargabeli").val()),
			satuan			= $("#satuan").text(),
			stok   			= toNumber($("#stok").val()),
			tglexpire		= $("#tglexpire").val(),
			beli   			= toNumber($("#beli").val()),
			jumlah 			= toNumber($("#jumlah").val()),
			
			query			= 'userid='+userid+
							'&type=save-item'+
							'&suplier='+suplier+
							'&nota='+nota+
							'&kodebeli='+kodebeli+
							'&pbdid='+pbdid+
							'&kategori='+kategori+
							'&kodeprod='+kodeprod+
							'&nama='+nama+
							'&hargabeli='+hargabeli+
							'&satuan='+satuan+
							'&stok='+stok+
							'&tglexpire='+tglexpire+
							'&beli='+beli+
							'&jumlah='+jumlah;
							
		// validate 
		if(!kategori && !kodeprod && !nama && !hargabeli && !jumlah){
			window.alert('Ooops!\nItem Pembelian Belum ada!');
			$("#cari").focus();
			exit;
		}
		
		if(!tglexpire){
			window.alert('Ooops!\nTanggal Expire tidak boleh kosong!');
			$("#tglexpire").focus();
			exit;
		}
		
		if(beli==0 || !beli){
			window.alert('Ooops!\nBeli tidak boleh kosong Minimanl 1');
			$("#beli").focus();
			exit;
		}
		
		// save via ajax
        $.ajax({
			url     : 'modules/pembelian_form/pembelian_form_ajax.php',
			data    : query,
			cache   : false,
			type    : 'post',
			success : function(data) {
				//window.alert("Data Berhasil Disimpan");		
				window.location = '?p=<?php echo paramEncrypt('pembelian_form');?>&kd=<?php echo paramEncrypt($pb_kode);?>&pbdid=<?php echo paramEncrypt(0);?>';					
			},
			error : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
				return false; //exit();
			}
		}); 
	})
	
	$("#persendiskon").change(function(){
		var	jtotal			= toNumber($("#jtotal").val()),
			persendiskon	= toNumber($("#persendiskon").val()),
			diskon			= persendiskon/100*jtotal;
		
		$("#diskon").val(diskon);
		
		var	jtotal		= toNumber($("#jtotal").val()),
			diskon		= toNumber($("#diskon").val()),
			jtotal2		= jtotal-diskon,
			persenppn	= toNumber($("#persenppn").val()),
			ppn			= persenppn/100*jtotal2,
			gtotal		= jtotal2+ppn;
		
		$("#ppn").val(ppn);	
		$("#gtotal").val(gtotal);
		$("#bayar").focus();
    })
	
	$("#persenppn").change(function(){		
		var	jtotal		= toNumber($("#jtotal").val()),
			diskon		= toNumber($("#diskon").val()),
			jtotal2		= jtotal-diskon,
			persenppn	= toNumber($("#persenppn").val()),
			ppn			= persenppn/100*jtotal2,
			gtotal		= jtotal2+ppn;
		
		$("#ppn").val(ppn);	
		$("#gtotal").val(gtotal);
		$("#bayar").focus();
    })
	
	$("#bayar").keyup(function(){
		var	gtotal		= toNumber($("#gtotal").val()),
			bayar		= toNumber($("#bayar").val()),
			sisabayar	= bayar-gtotal;
		
		if(bayar>gtotal){
			window.alert('Bayar tidak boleh lebih dari grand total');
			$("#bayar").val('');
		}
		
		$("#sisabayar").val(sisabayar);
	})
	
	$("#btn-submit").click(function(e){
		e.preventDefault();
		
        var kodebeli		= $("#kodebeli").val(),
			suplier			= $("#suplier").val(),
			nota			= $("#nota").val(),
			jtotal			= toNumber($("#jtotal").val()),
			persendiskon	= toNumber($("#persendiskon").val()),
			diskon			= toNumber($("#diskon").val()),
			persenppn		= toNumber($("#persenppn").val()),
			ppn				= toNumber($("#ppn").val()),
			gtotal			= toNumber($("#gtotal").val()),
			bayar			= toNumber($("#bayar").val()),
			sisabayar		= toNumber($("#sisabayar").val());
			
            query		= 'userid='+userid+
						'&type=save'+
						'&kodebeli='+kodebeli+
						'&suplier='+suplier+
						'&nota='+nota+
						'&jtotal='+jtotal+
						'&persendiskon='+persendiskon+
						'&diskon='+diskon+
						'&persenppn='+persenppn+
						'&ppn='+ppn+
						'&gtotal='+gtotal+
						'&bayar='+bayar+
						'&sisabayar='+sisabayar;

		// validate	
		if(!suplier){
			window.alert('Ooops!\nSuplier tidak boleh kosong!');
			$("#suplier").focus();
			exit;
		}
		
		if(!nota){
			window.alert('Ooops!\nNo nota tidak boleh kosong!');
			$("#nota").focus();
			exit;
		}
		
		if(!jtotal && !ppn && !gtotal){
			window.alert('Ooops!\nItem Pembelian Belum ada!');
			$("#cari").focus();
			exit;
		}
		
		if(jtotal==0 && ppn==0 && gtotal==0){
			window.alert('Ooops!\nItem Pembelian Belum ada!');
			$("#cari").focus();
			exit;
		}

        // save via ajax
        $.ajax({
            url     : 'modules/pembelian_form/pembelian_form_ajax.php',
            data    : query,
            cache   : false,
            type    : 'post',
            success : function(data) {
				//window.alert("Data Berhasil Disimpan");
				window.location = '?p=<?php echo paramEncrypt('pembelian');?>';
            },
            error : function(xhr, textStatus, errorThrown) {
                alert(textStatus + '\nrequest failed: "' + errorThrown + '"');
                return false; //exit();
            }
        });
    })
	
})// End Main Function

</script>