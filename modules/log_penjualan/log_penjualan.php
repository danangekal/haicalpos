<?php
defined('_IEXEC')or die('');
?>

<div class="box box-solid box-info">
	<div class="box-header with-border">
	  <h3 class="box-title">Management <?php echo ucwords($p); ?></h3>
	</div><!-- /.box-header -->
	<div class="box-body">
		<div class="input-group col-md-6">
			<span class="input-group-addon">Bulan:</span>
		<select id="selectmonth" class="form-control input-sm">
		<?php
			foreach($namabulan as $key=>$value){
				if($key == 0) echo '<option value="'.$key.'">-- Semua --</option>';
				else if($key==date("n")) echo '<option value="'.$key.'" selected>'.$value.'</option>';
				else echo '<option value="'.$key.'">'.$value.'</option>';
			}
		?>
		</select>
		<span class="input-group-addon">Tahun:</span>
		<select id="selectyear" class="form-control input-sm">
			<option value="0" selected>-- Semua --</option>
		<?php
			$YearFirst  = 2000;
			$YearEnd    = date("Y");
			for($year=$YearFirst; $year<=$YearEnd; $year++){
				if($year == date("Y")) echo '<option value="'.$year.'" selected>'.$year.'</option>';
				else echo '<option value="'.$year.'">'.$year.'</option>';
			}
		?>
		</select>
		<span class="input-group-addon">Lihat:</span>
		<select id="selectshow" class="form-control input-sm">
		<option value="0" selected>-- Semua --</option>
		<?php
			foreach ($ArrNumRec as $key => $value) {
				if($value){
					if($key == 0) echo '<option selected>'.$value.'</option>';
					else echo '<option>'.$value.'</option>';
				}
			}
		?>
		</select>
		</div>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr class="success">
					<th>#</th>
					<th>Customer</th>
					<th>Kode</th>
					<th>Tanggal</th>
					<th>Jumlah</th>
					<th>Diskon (%)</th>
					<th>PPn (%)</th>
					<th>Total</th>
					<th>Casier</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="contents">
			<!-- Isi-->
			</tbody>
		</table>
		</div>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<a data-toggle="tooltip" title="Kembali ke Penjualan" class="btn btn-success" href="?p=<?php echo paramEncrypt('penjualan');?>" role="button"><i class="fa fa-arrow-circle-left"></i> Back Penjualan</a>
		<a data-toggle="tooltip" title="Log Retur Penjualan" class="btn btn-primary" href="page.php?p=<?php echo paramEncrypt('log_retur_penjualan');?>" role="button"><i class="fa fa-bars"></i> Log Retur Penjualan</a>
	</div><!-- /.box-footer -->
</div><!-- /.box -->	

<script>
// Main Function		
$(function(){ 
	
	function load_data(){
		$("#contents").html('<tr><td colspan="13" class="text-center"><i class="fa fa-spinner fa-spin fa-lg"></i></td></tr>');
		var idresult	='contents';
			query   	='m='+$("#selectmonth").val()+'&y='+$("#selectyear").val()+'&show='+$("#selectshow").val();
		
		//alert(query);
		$.ajax({
			url     : 'modules/log_penjualan/log_penjualan_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				var result ="#"+idresult;
				$(result).html(data);
			},
			error   : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest gagal: "' + errorThrown + '"');
				return false;
			}
		})
	}
	
	load_data();
	$("#selectmonth, #selectyear, #selectshow").change(function(){
        load_data();
    })
	
})// Main Function
</script>