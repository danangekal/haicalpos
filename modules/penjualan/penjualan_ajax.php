<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$d = date("d"); //tanggal sekarang
	$m = date("m"); //bulan sekarang
	$y = date("Y"); //tahun sekarang
	
	$Day='';
	$Day =" && DAY(pj_tanggal) = ".$d." ";
	
	$Month='';
	$Month =" && MONTH(pj_tanggal) = ".$m." ";

	$Year='';
	$Year=" YEAR(pj_tanggal) = ".$y." ";

	
	$query="
		SELECT  
			*
		FROM    
			penjualan 
		WHERE
			".$Year.$Month.$Day."
		ORDER BY 
			pj_kode 
		DESC ";
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
            $content.='
                <tr>
                    <td>'.$no.'</td>
					<td><b>'.$pj_customer.'</b></td>
                    <td><b>'.$pj_kode.'</b></td>
                    <td>'.showdt($pj_tanggal, 2).'</td>
					<td>'.rupiah($pj_total).'</td>
					<td>'.rupiah($pj_bayar).'</td>
					<td>'.rupiah($pj_kembali).'</td>
					<td>'.get_fullname($pj_user_id).'</td>
            ';
					
			if($pj_bayar==0) {
			$content .='
					<td>
						<a data-toggle="tooltip" title="Edit Penjualan" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('penjualan_form').'&kd='.paramEncrypt($pj_kode).'&pjdid='.paramEncrypt(0).'" role="button"><i class="fa fa-edit"></i></a>
					</td>
                </tr>
            '; 
			} else {
			$content .='
					<td>
						<a data-toggle="tooltip" title="Export Struk Penjualan (PDF)" class="btn btn-xs btn-danger" href="?p='.paramEncrypt('struk_penjualan_pdf').'&kd='.paramEncrypt($pj_kode).'" role="button"><i class="fa fa-download"></i></a>
					</td>
                </tr>
            ';		
			}		 
			//tombol delete
			//<a class="btn btn-xs btn-danger" href="javascript:del('.$pj_kode.')" role="button"><i class="fa fa-trash-o"></i></a>
        }
    }
    echo $content;
?>

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/penjualan_form/penjualan_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
               	window.alert("Data Berhasil Dihapus");    
				window.location = '?p=<?php echo paramEncrypt('penjualan');?>';
			}
		});
	}
}
</script>