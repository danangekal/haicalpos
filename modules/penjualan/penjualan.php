<?php
defined('_IEXEC')or die('');

$d = date("d"); //tanggal sekarang
$m = date("m"); //bulan sekarang
$y = date("Y"); //tahun sekarang
	
//data penjualan
$Daypj	='';
$Daypj	=" && DAY(pj_tanggal) = ".$d." ";

$Monthpj='';
$Monthpj=" && MONTH(pj_tanggal) = ".$m." ";

$Yearpj='';
$Yearpj=" && YEAR(pj_tanggal) = ".$y." ";

$q1 = mysql_query("
	SELECT SUM(pj_total) as pj_total
	FROM
		penjualan
	WHERE
		pj_bayar>0
		".$Yearpj.$Monthpj.$Daypj."
")or die(mysql_error());
if(mysql_num_rows($q1)){
	extract(mysql_fetch_array($q1));
} else {
	$pj_total = 0;
}

//data pembelian
$Daypb	='';
$Daypb	=" && DAY(pb_tanggal) = ".$d." ";

$Monthpb='';
$Monthpb=" && MONTH(pb_tanggal) = ".$m." ";

$Yearpb='';
$Yearpb=" && YEAR(pb_tanggal) = ".$y." ";

$q2a = mysql_query("
	SELECT SUM(pb_bayar) as bayar1
	FROM
		pembelian
	WHERE
		pb_status = 1
		&& pb_sisa_bayar = 0
		".$Yearpb.$Monthpb.$Daypb."
")or die(mysql_error());
if(mysql_num_rows($q2a)){
	extract(mysql_fetch_array($q2a));
} else {
	$bayar1 = 0;
}

$q2b = mysql_query("
	SELECT SUM(pb_sisa_bayar) as bayar2
	FROM
		pembelian
	WHERE
		pb_status = 1
		&& pb_sisa_bayar > 0
		".$Yearpb.$Monthpb.$Daypb."
")or die(mysql_error());
if(mysql_num_rows($q2b)){
	extract(mysql_fetch_array($q2b));
} else {
	$bayar2 = 0;
}

$q2c = mysql_query("
	SELECT SUM(pb_bayar) as bayar3
	FROM
		pembelian
	WHERE
		pb_status = 0
		&& pb_sisa_bayar > 0
		".$Yearpb.$Monthpb.$Daypb."
")or die(mysql_error());
if(mysql_num_rows($q2c)){
	extract(mysql_fetch_array($q2c));
} else {
	$bayar3 = 0;
}

$pb_bayar = $bayar1+$bayar2+$bayar3;

//data penitipan
$Daypn	='';
$Daypn	=" && DAY(pn_tanggal) = ".$d." ";

$Monthpn='';
$Monthpn=" && MONTH(pn_tanggal) = ".$m." ";

$Yearpn='';
$Yearpn=" && YEAR(pn_tanggal) = ".$y." ";

$q3 = mysql_query("
	SELECT SUM(pn_total_bayar) as pn_total_bayar
	FROM
		penitipan
	WHERE
		pn_status = 1
		".$Yearpn.$Monthpn.$Daypn."
")or die(mysql_error());
if(mysql_num_rows($q3)){
	extract(mysql_fetch_array($q3));
} else {
	$pn_total_bayar = 0;
}

//data pengeluaran
$Daypg	='';
$Daypg	=" && DAY(pg_tanggal) = ".$d." ";

$Monthpg='';
$Monthpg=" && MONTH(pg_tanggal) = ".$m." ";

$Yearpg='';
$Yearpg=" YEAR(pg_tanggal) = ".$y." ";

$q4 = mysql_query("
	SELECT SUM(pg_jumlah) as pg_jumlah
	FROM
		pengeluaran
	WHERE
		".$Yearpg.$Monthpg.$Daypg."
")or die(mysql_error());
if(mysql_num_rows($q4)){
	extract(mysql_fetch_array($q4));
} else {
	$pg_jumlah = 0;
}

$tpengeluaran	= $pb_bayar + $pn_total_bayar + $pg_jumlah;
$saldo			= $pj_total - $tpengeluaran;
$tglskrg 		= date("Y-m-d");
?>
<div id="laba-rugi-info" class="modal" data-backdrop="static">
	<div id="modal-content" class="modal-dialog modal-lg" role="document">        
						
			<div class="box box-solid box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Informasi <b>Laba Rugi <?php echo showdt($tglskrg, 2);?></b></h3>
					<div class="box-tools pull-right">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
					</div>
				</div>			
				<div class="box-body">
					<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr class="success">
							<th colspan="2">Pendapatan Usaha</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Penjualan</td>
							<td><?php echo rupiah($pj_total);?></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>Jumlah Pendapatan Usaha</th>
							<th><?php echo rupiah($pj_total);?></th>
						</tr>
					</tfoot>
					</table>
					<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr class="success">
							<th colspan="2">Pengeluaran Operasional</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Pembelian</td>
							<td><?php echo rupiah($pb_bayar);?></td>
						</tr>
						<tr>
							<td>Pembayaran Penitipan</td>
							<td><?php echo rupiah($pn_total_bayar);?></td>
						</tr>
						<tr>
							<td>Pengeluaran Umum</td>
							<td><?php echo rupiah($pg_jumlah);?></td>
						</tr>
						<tr>
							<td>Pembelian + Pembayaran Penitipan + Pengeluaran Umum (<?php echo rupiah($pb_bayar);?> + <?php echo rupiah($pn_total_bayar);?> + <?php echo rupiah($pg_jumlah);?>)</td>
							<td><?php echo rupiah($tpengeluaran);?></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>Jumlah Pengeluaran Operasional</th>
							<th><?php echo rupiah($tpengeluaran);?></th>
						</tr>
					</tfoot>
					</table>
					<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr class="success">
							<th colspan="2">Saldo Pendapatan</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Pendapatan Usaha - Pengeluaran Operasional (<?php echo rupiah($pj_total);?> - <?php echo rupiah($tpengeluaran);?>)</td>
							<td><?php echo rupiah($saldo);?></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>Jumlah Saldo Pendapatan</th>
							<th><?php echo rupiah($saldo);?></th>
						</tr>
					</tfoot>
					</table>
					</div>
				</div>
				<div class="box-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				</div>
			</div>			
					
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
	
<div class="box box-solid box-info">
	<div class="box-header with-border">
	  <h3 class="box-title">Management <?php echo ucwords($p); ?> (Harian)</h3>
	</div><!-- /.box-header -->
	<div class="box-body">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-hover" id="mytable">
			<thead>
				<tr class="success">
					<th>No</th>
					<th>Customer</th>
					<th>Kode</th>
					<th>Tanggal</th>
					<th>Total</th>
					<th>Bayar</th>
					<th>Kembali</th>
					<th>Casier</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="contents">
			<!-- Isi-->
			</tbody>
		</table>
		</div>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<a data-toggle="tooltip" title="Tambah Penjualan" class="btn btn-success" href="page.php?p=<?php echo paramEncrypt('penjualan_form');?>" role="button"><i class="fa fa-plus"></i> Penjualan</a>
		<a data-toggle="tooltip" title="Log Penjualan" class="btn btn-primary" href="page.php?p=<?php echo paramEncrypt('log_penjualan');?>" role="button"><i class="fa fa-bars"></i> Log Penjualan</a>
		<a data-toggle="tooltip" title="Piutang Penjualan" class="btn btn-primary" href="page.php?p=<?php echo paramEncrypt('piutang_penjualan');?>" role="button"><i class="fa fa-money"></i> Piutang Penjualan</a>
		<a data-toggle="modal" class="btn btn-primary" href="#laba-rugi-info" role="button"><i class="fa fa-pie-chart"></i> Detail Laba Rugi</a>		
	</div><!-- /.box-footer -->
</div><!-- /.box -->	

<script>
// Main Function		
$(function(){ 
	
	function load_data(){
		$("#contents").html('<tr><td colspan="13" class="text-center"><i class="fa fa-spinner fa-spin fa-lg"></i></td></tr>');
		var idresult	= 'contents';
			query   	= '';
		
		//alert(query);
		$.ajax({
			url     : 'modules/penjualan/penjualan_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				var result ="#"+idresult;
				$(result).html(data);
				$("#mytable").dataTable();
				
			},
			error   : function(xhr, textStatus, errorThrown) {
				alert(textStatus + '\nrequest gagal: "' + errorThrown + '"');
				return false;
			}
		})
	}
	
	load_data();
	
})// Main Function
</script>