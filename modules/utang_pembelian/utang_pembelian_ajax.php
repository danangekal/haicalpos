<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
	
	$query="
		SELECT  
			*
		FROM    
			pembelian 
		WHERE
			pb_status = 0
		ORDER BY 
			pb_kode 
		DESC ";
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
            $content.='
                <tr>
                    <td>'.$no.'</td>
                    <td><b>'.$pb_kode.'</b></td>
                    <td>'.showdt($pb_tanggal, 2).'</td>
					<td>'.$pb_suplier.'</td>
					<td>'.$pb_nota.'</td>
					<td>'.rupiah($pb_total).'</td>
					<td>'.rupiah($pb_bayar).'</td>
					<td>'.rupiah($pb_sisa_bayar).'</td>
					<td>'.get_fullname($pb_user_id).'</td>
					<td>
						<a data-toggle="tooltip" title="Lunasi Pembelian" class="btn btn-xs btn-primary" href="#" onclick="lunasi(\'' .$pb_kode. '\', '.$pb_total.', '.$pb_sisa_bayar.', '.$pb_user_id.')" role="button"><i class="fa fa-check-circle"></i></a>
					</td>
                </tr>
				';
			//tombol delete
			//<a class="btn btn-xs btn-danger" href="javascript:del('.$pj_kode.')" role="button"><i class="fa fa-trash-o"></i></a>
        }
    }
    echo $content;
?>

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/pembelian_form/pembelian_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
               	window.alert("Data Berhasil Dihapus");    
				window.location = '?p=<?php echo paramEncrypt('penjualan');?>';
			}
		});
	}
}

function lunasi(kode, total, sisabayar, userid) {
	var query		= 'type=lunasi'+
					  '&kodebeli='+kode+
					  '&gtotal='+total+
					  '&sisabayar='+sisabayar+
					  '&userid='+userid;

	$.ajax({
		url     : 'modules/pembelian_form/pembelian_form_ajax.php',
		type    : 'post',
		data    : query,
		cache   : false,
		//dataType:'json',
		success : function(data) {
			window.alert("Data Berhasil Dilunasi");    
			window.location = '?p=<?php echo paramEncrypt('pembelian');?>';
		}
	}); 
}
</script>