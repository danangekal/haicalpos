<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
	$type				= $_POST['type'];
	
	//data penjualan	
	$pj_kode			= $_POST['kodejual'];
	$pj_customer		= $_POST['customer'];
	$pj_customer_umum	= $_POST['customerumum'];
	$pj_jumlah			= $_POST['jtotal'];
	$pj_persendiskon	= $_POST['persendiskon'];
	$pj_diskon			= $_POST['diskon'];
	$pj_persenppn		= $_POST['persenppn'];
	$pj_ppn				= $_POST['ppn'];
	$pj_total			= $_POST['gtotal'];
	$pj_bayar			= $_POST['bayar'];
	$pj_kembali			= $_POST['kembali'];
	$pj_user_id			= $_POST['userid'];
		
	//data penjualan detail
	$pjd_id				= $_POST['pjdid'];
	$pjd_p_k_id			= $_POST['kategori'];
	$pjd_p_kode			= $_POST['kodeprod'];
	$pjd_p_nama			= $_POST['nama'];
	$pjd_p_harga_jual	= $_POST['hargajual'];
	$pjd_p_harga_beli	= $_POST['hargabeli'];
	$pjd_p_satuan		= $_POST['satuan'];
	$pjd_beli			= $_POST['beli'];
	$pjd_jumlah			= $_POST['jumlah'];
	
	//data produk
	$p_stok				= $_POST['stok'];
	
	switch ($type) {
		case 'save':
				$r = mysql_query("
					SELECT  *
					FROM
						penjualan
					WHERE
						pj_kode = '$pj_kode'
				")or die(mysql_error());
				if(mysql_num_rows($r)){
					//cari total profit penjualan detail
					$result = mysql_query("
						SELECT 
							SUM(pjd_profit) as jtprofit
						FROM 
							penjualan_detail 
						WHERE 
							pjd_pj_kode = '$pj_kode' 
					")or die(mysql_error());
					if(mysql_num_rows($result)){
						extract(mysql_fetch_assoc($result));	
					}
				
					//Update ke penjualan
					mysql_query("
						UPDATE  
							penjualan 
						SET
							pj_customer		= '$pj_customer',
							pj_customer_umum= '$pj_customer_umum',
							pj_tanggal		= NOW(),
							pj_jumlah		= '$pj_jumlah',
							pj_persen_diskon= '$pj_persendiskon',
							pj_diskon		= '$pj_diskon',
							pj_persen_ppn	= '$pj_persenppn',
							pj_ppn			= '$pj_ppn',
							pj_total		= '$pj_total',
							pj_bayar		= '$pj_bayar',
							pj_kembali		= '$pj_kembali',
							pj_profit		= '$jtprofit',
							pj_user_id		= '$pj_user_id'
						WHERE
							pj_kode			= '$pj_kode'
					")or die(mysql_error());
					
					//cari saldo keuangan terakhir
					$result2 = mysql_query("
						SELECT 
							keu_saldo
						FROM 
							keuangan 
						ORDER BY keu_id DESC
						LIMIT 1 
					")or die(mysql_error());
					if(mysql_num_rows($result2)){
						extract(mysql_fetch_assoc($result2));	
					}
					
					$saldo = $keu_saldo+$jtprofit;
					
					//simpan ke rekening keuangan
					mysql_query("
						INSERT INTO 
							keuangan 
						SET
							keu_tanggal			= NOW(),
							keu_kode			= '$pj_kode',
							keu_transaksi		= 'Penjualan',
							keu_mutasi_kredit	= '$jtprofit',
							keu_saldo			= '$saldo',
							keu_keterangan		= 'Penambahan',
							keu_user_id			= '$pj_user_id'
					")or die(mysql_error());
					
					//cari saldo keuangan terakhir company
					$result3 = mysql_query("
						SELECT 
							c_keuangan
						FROM 
							company
						WHERE
							c_id =1
					")or die(mysql_error());
					if(mysql_num_rows($result3)){
						extract(mysql_fetch_assoc($result3));	
					}
					
					$keuangan = $c_keuangan+$jtprofit;
					
					//Update ke saldo keuangan company
					mysql_query("
						UPDATE  
							company
						SET
							c_keuangan	= '$keuangan'
						WHERE
							c_id		= 1
					")or die(mysql_error());
				}
		break;
		
		case 'delete': // fungsi belum ada tapi sudah disiapkan.. tinggal edit keuangan bila penjualan dihapus
			$r = mysql_query("
				SELECT  *
				FROM
					penjualan
				WHERE
					pj_kode = '$pj_kode'
			")or die(mysql_error());
			if(mysql_num_rows($r)){
				//hapus penjualan dahulu
				mysql_query("
					DELETE FROM 
						penjualan 
					WHERE
						pj_kode			= '$pj_kode'
				")or die(mysql_error());
				
				// hapus penjualan detail kemudian
				mysql_query("DELETE FROM penjualan_detail WHERE pjd_pj_kode='$pj_kode'")or die(mysql_error());
			}
		break;
		
		case 'save-item':
			$r = mysql_query("
				SELECT  *
				FROM
					penjualan
				WHERE
					pj_kode = '$pj_kode'
			")or die(mysql_error());
			if(!mysql_num_rows($r)){
				//proses penjualan detail
				if(!$pjd_id || $pjd_id == '' || $pjd_id==0) {	
				
					$stok = $p_stok-$pjd_beli; 
					
					// update stok produk
					mysql_query("
						UPDATE produk SET p_stok='$stok' WHERE p_kode='$pjd_p_kode'
					")or die(mysql_error());
					
					$profit = ($pjd_p_harga_jual-$pjd_p_harga_beli)*$pjd_beli;
					
					//simpan ke penjualan detail
					mysql_query("
						INSERT INTO 
							penjualan_detail 
						SET
							pjd_pj_kode			= '$pj_kode',
							pjd_p_k_id			= '$pjd_p_k_id',
							pjd_p_kode			= '$pjd_p_kode',
							pjd_p_nama			= '$pjd_p_nama',
							pjd_p_harga_jual	= '$pjd_p_harga_jual',
							pjd_p_harga_beli	= '$pjd_p_harga_beli',
							pjd_p_satuan		= '$pjd_p_satuan',
							pjd_beli			= '$pjd_beli',
							pjd_jumlah			= '$pjd_jumlah',
							pjd_profit			= '$profit'
					")or die(mysql_error());
				} 
				
				//cari jumlah total penjualan detail
				$result3 = mysql_query("
					SELECT 
						SUM(pjd_jumlah) as jtotal
					FROM 
						penjualan_detail 
					WHERE 
						pjd_pj_kode = '$pj_kode' 
				")or die(mysql_error());
				if(mysql_num_rows($result3)){
					extract(mysql_fetch_assoc($result3));	
				}
				
				$total	= $jtotal;
				
				//simpan ke penjualan
				mysql_query("
					INSERT INTO 
						penjualan 
					SET
						pj_kode			= '$pj_kode',
						pj_customer		= '$pj_customer',
						pj_customer_umum= '$pj_customer_umum',
						pj_tanggal		= NOW(),
						pj_jumlah		= '$jtotal',
						pj_total		= '$total',
						pj_user_id		= '$pj_user_id'
				")or die(mysql_error());
			} else { //kalau save-item kode jual sudah ada
				// proses penjualan detail
				if(!$pjd_id || $pjd_id == '' || $pjd_id==0) {
				
					$stok = $p_stok-$pjd_beli; 
					
					// update stok produk
					mysql_query("
						UPDATE produk SET p_stok='$stok' WHERE p_kode='$pjd_p_kode'
					")or die(mysql_error()); 
					
					$profit = ($pjd_p_harga_jual-$pjd_p_harga_beli)*$pjd_beli;
					
					//simpan ke penjualan detail
					mysql_query("
						INSERT INTO 
							penjualan_detail 
						SET
							pjd_pj_kode			= '$pj_kode',
							pjd_p_k_id			= '$pjd_p_k_id',
							pjd_p_kode			= '$pjd_p_kode',
							pjd_p_nama			= '$pjd_p_nama',
							pjd_p_harga_jual	= '$pjd_p_harga_jual',
							pjd_p_harga_beli	= '$pjd_p_harga_beli',
							pjd_p_satuan		= '$pjd_p_satuan',
							pjd_beli			= '$pjd_beli',
							pjd_jumlah			= '$pjd_jumlah',
							pjd_profit			= '$profit'
					")or die(mysql_error());
				} else {					
					//cari jumlah beli sebelumnya
					$result2 = mysql_query("
						SELECT 
							pjd_beli  as beli_awal 
						FROM 
							penjualan_detail 
						WHERE 
							pjd_id = '$pjd_id'
					")or die(mysql_error());
					if(mysql_num_rows($result2)){
						extract(mysql_fetch_assoc($result2));	
					}
					
					$stok_awal = $p_stok+$beli_awal; 
					
					// update stok produk
					mysql_query("
						UPDATE produk SET p_stok='$stok_awal' WHERE p_kode='$pjd_p_kode'
					")or die(mysql_error());

					$stok = $stok_awal-$pjd_beli;
					
					// update stok produk
					mysql_query("
						UPDATE produk SET p_stok='$stok' WHERE p_kode='$pjd_p_kode'
					")or die(mysql_error());
					
					$profit = ($pjd_p_harga_jual-$pjd_p_harga_beli)*$pjd_beli;
					
					//update ke penjualan detail
					mysql_query("
						UPDATE  
							penjualan_detail 
						SET
							pjd_pj_kode			= '$pj_kode',
							pjd_p_k_id			= '$pjd_p_k_id',
							pjd_p_kode			= '$pjd_p_kode',
							pjd_p_nama			= '$pjd_p_nama',
							pjd_p_harga_jual	= '$pjd_p_harga_jual',
							pjd_p_harga_beli	= '$pjd_p_harga_beli',
							pjd_p_satuan		= '$pjd_p_satuan',
							pjd_beli			= '$pjd_beli',
							pjd_jumlah			= '$pjd_jumlah',
							pjd_profit			= '$profit'
						WHERE
							pjd_id				= '$pjd_id'
					")or die(mysql_error());
				}
				
				//cari jumlah total penjualan detail 
				$result3 = mysql_query("
					SELECT 
						SUM(pjd_jumlah) as jtotal 
					FROM 
						penjualan_detail 
					WHERE 
						pjd_pj_kode = '$pj_kode' 
				")or die(mysql_error());
				if(mysql_num_rows($result3)){
					extract(mysql_fetch_assoc($result3));	
				}
				
				$total	= $jtotal;
				
				//update ke penjualan
				mysql_query("
					UPDATE  
						penjualan 
					SET
						pj_tanggal		= NOW(),
						pj_customer		= '$pj_customer',
						pj_customer_umum= '$pj_customer_umum',
						pj_jumlah		= '$jtotal',
						pj_total		= '$total',
						pj_user_id		= '$pj_user_id'
					WHERE
						pj_kode			= '$pj_kode'
				")or die(mysql_error());
			}
		break;
		
		case 'delete-item':						
			//cari total yang dikurangi/dihapus dan kode penjualan
			$result = mysql_query("
				SELECT 
					pjd_pj_kode,
					pjd_p_kode,
					pjd_beli as beli_awal,
					pjd_jumlah 
				FROM 
					penjualan_detail 
				WHERE 
					pjd_id = '$pjd_id' 
			")or die(mysql_error());
			if(mysql_num_rows($result)){
				extract(mysql_fetch_assoc($result));	
			}
			
			//cari jumlah total dari penjualan
			$result2 = mysql_query("
				SELECT 
					pj_jumlah 
				FROM 
					penjualan 
				WHERE 
					pj_kode = '$pjd_pj_kode'
			")or die(mysql_error());
			if(mysql_num_rows($result2)){
				extract(mysql_fetch_assoc($result2));	
			}
			
			$sisajumlah = $pj_jumlah-$pjd_jumlah;
			$sisatotal	= $sisajumlah;
			
			//update ke penjualan
			mysql_query("
				UPDATE  
					penjualan 
				SET
					pj_tanggal		= NOW(),
					pj_jumlah		= '$sisajumlah',
					pj_total		= '$sisatotal',
					pj_user_id		= '$pj_user_id'
				WHERE
					pj_kode			= '$pjd_pj_kode'
			")or die(mysql_error());
			
			//cari stok produk terakhir
			$result3 = mysql_query("
				SELECT 
					p_stok as stok_akhir
				FROM 
					produk 
				WHERE 
					p_kode = '$pjd_p_kode'
			")or die(mysql_error());
			if(mysql_num_rows($result3)){
				extract(mysql_fetch_assoc($result3));	
			}
			
			$stok = $stok_akhir+$beli_awal; 
			
			// update stok produk
			mysql_query("
				UPDATE produk SET p_stok='$stok' WHERE p_kode='$pjd_p_kode'
			")or die(mysql_error());
			
			//hapus penjualan detail
			mysql_query("
				DELETE FROM penjualan_detail WHERE pjd_id='$pjd_id'
			")or die(mysql_error());		
		break;
	}
?>