<?php
defined('_IEXEC')or die('');
?>

<div class="box box-solid box-info">
	<div class="box-header with-border">
	  <h3 class="box-title">Management <?php echo ucwords($p); ?></h3>
	</div><!-- /.box-header -->
	<div class="box-body">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-hover" id="mytable">
			<thead>
				<tr class="success">
					<th>No</th>
					<th>Username</th>
					<th>Password</th>
					<th>Fullname</th>
					<th>Jabatan</th>
					<th>Aktif</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="contents">
			<!-- Isi-->
			</tbody>
		</table>
		</div>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<a data-toggle = "tooltip" title = "Tambah User" class="btn btn-success" href="page.php?p=<?php echo paramEncrypt('user_form');?>" role="button"><i class="fa fa-plus"></i> User</a>
	</div><!-- /.box-footer -->
</div><!-- /.box -->	

<script>		
$(function(){ 
	load_data();
})//ready

function load_data(){
	$("#contents").html('<tr><td colspan="13" class="text-center"><i class="fa fa-spinner fa-spin fa-lg"></i></td></tr>');
	var idresult 	= 'contents';
	var query   	= '';
	//alert(query);
	$.ajax({
		url     : 'modules/user/user_ajax.php',
		type    : 'post',
		data    : query,
		cache   : false,
		//dataType:'json',
		success : function(data) {
			var result ="#"+idresult;
            $(result).html(data);
			$("#mytable").dataTable();
		},
		error   : function(xhr, textStatus, errorThrown) {
			alert(textStatus + '\nrequest gagal: "' + errorThrown + '"');
			return false;
		}
	})
}

</script>