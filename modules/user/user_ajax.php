<?php
    //Database Connection
	include('../../core/cukang.inc.php');
	//Core
	include('../../core/core.php');
    //-----------------------------------------------
   
    $query="
	 SELECT  
		*
	 FROM    
		user
	 INNER JOIN user_to_jabatan ON user_id = utj_user_id
	 WHERE 
		user_id !=1
	 ORDER BY 
		user_id 
	 ASC ";
	$result = mysql_query($query) or die(mysql_error());
	
	$content    = '<tr><td colspan="8" class="text-center"> -- Not Found Content --</td></tr>';
    if(mysql_num_rows($result)>0){
        $content = '';
		$no=0;
        while($row = mysql_fetch_array($result)){
            extract($row);
            $no++;
			$status_color = ($user_aktif == 'Y')? 'text-green' : 'text-red';
            $user_aktif = ($user_aktif == 'Y')? 'Ya' : 'Tidak';
            $content.='
                <tr>
                    <td>'.$no.'</td>
                    <td><b>'.$user_name.'</b></td>
                    <td>xxx</td>
                    <td>'.ucwords($user_fullname).'</td>
					<td>'.get_jabatan($utj_j_id).'</td>
					<td class="'.$status_color.'">'.strtoupper($user_aktif).'</td>
                    <td>
						<a data-toggle = "tooltip" title = "Edit User" class="btn btn-xs btn-warning" href="?p='.paramEncrypt('user_form').'&id='.paramEncrypt($user_id).'" role="button"><i class="fa fa-edit"></i></a>
						<a data-toggle = "tooltip" title = "Delete User" class="btn btn-xs btn-danger" href="javascript:del('.$user_id.')" role="button"><i class="fa fa-trash-o"></i></a>
					</td>
                </tr>
            ';
        }
    }
    echo $content;
?>

<script>
function del(id) {
	var id		= id;
	var query	= 'type=delete'+
				  '&id='+id;
	var pilih	= confirm('Yakin data dengan id '+id+ ' akan dihapus?');
	
	if (pilih==true) {
		$.ajax({
			url     : 'modules/user_form/user_form_ajax.php',
			type    : 'post',
			data    : query,
			cache   : false,
			//dataType:'json',
			success : function(data) {
				if(id == "") {
               		window.alert("Data Gagal Dihapus");
               	} else {
               		window.alert("Data Berhasil Dihapus");
				}      
				window.location = '?p=<?php echo paramEncrypt('user');?>';
			}
		});
	}
}
</script>