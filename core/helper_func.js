//fungsi clear input pada textbox
function clearInput(element){
	element.value="";
}

//fungsi number mask
function set_mask(){
	$(".number-mask").inputmask(
		"decimal",
		{
			radixPoint: ".",
			autoGroup: true,
			groupSeparator: ",",
			groupSize:3,
			rightAlignNumerics: true,
			"oncleared":function(){
				$(this).val('0');
			}
		}
	);
}

//fungsi toNumber
function toNumber(x){
	var text = String(x).replace(/[^0-9.]/g,'');
	return parseFloat(text);
}

//mendapatkan nilai checkbox
function get_CheckValue(name){
	if($("#"+name).is(':checked')) return 1;
	else return 0;
}