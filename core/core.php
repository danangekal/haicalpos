<?php
require_once('AES.class.php');
date_default_timezone_set("Asia/Jakarta"); //setinggan untuk timezone
//Constanta
$namabulan		= array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
$bulansekarang	= $namabulan[date('n')];
$namahari		= array('','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');
$harisekarang	= $namahari[date('N')];

$sekarang		= $harisekarang.', '.date("d").' '.$bulansekarang.' '.date('Y');
$tanggalsekarang = date("d").' '.$bulansekarang.' '.date('Y');

$ArrNumRec = array(0,10,20,40,80,100);

function paramEncrypt($x)
{
   $Cipher = new AES();
   // kunci enkripsi (Anda bisa memodifikasi kuncinya)
   //$key_128bit = '2b7e151628aed2a6abf7158809cf4f3c';
   $key_128bit = '2b7e151628aed2a6abf7158809cf4f3c';

   // membagi panjang string yang akan dienkripsi dengan panjang 16 karakter
   $n = ceil(strlen($x)/16);
   $encrypt = "";

   for ($i=0; $i<=$n-1; $i++)
   {
      // mengenkripsi setiap 16 karakter
      $cryptext = $Cipher->encrypt($Cipher->stringToHex(substr($x, $i*16, 16)), $key_128bit);
	  // menggabung hasil enkripsi setiap 16 karakter menjadi satu string enkripsi utuh
      $encrypt .= $cryptext;   
   } 

   return $encrypt;
}

function paramDecrypt($x)
{
   $Cipher = new AES();
   // kunci dekripsi (kunci ini harus sama dengan kunci enkripsi)
   $key_128bit = '2b7e151628aed2a6abf7158809cf4f3c';

   // karena string hasil enkripsi memiliki panjang 32 karakter, maka untuk proses dekripsi ini panjang string dipotong2 dulu menjadi 32 karakter
      
   $n = ceil(strlen($x)/32);
   $decrypt = "";

   for ($i=0; $i<=$n-1; $i++)
   {
      // mendekrip setiap 32 karakter hasil enkripsi
      $result = $Cipher->decrypt(substr($x, $i*32, 32), $key_128bit);
	  // menggabung hasil dekripsi 32 karakter menjadi satu string dekripsi utuh
      $decrypt .= $Cipher->hexToString($result);
   }
   return $decrypt; 
}

function decode($x)
{
  // proses decoding: memecah parameter dan masing-masing value yang terkait

  $pecahURI = explode('?', $x);
  $parameter = $pecahURI[1];

  $pecahParam = explode('&', paramDecrypt($parameter));

  for ($i=0; $i <= count($pecahParam)-1; $i++)
  {
     $decode = explode('=', $pecahParam[$i]);
     $var[$decode[0]] = $decode[1];  
  }

  return $var;
}

function sidebar_menu($userid){
	//Main menu (Menu) sesuai grant
	$q_m=mysql_query("
		SELECT 
			* 
		FROM module_menu
		WHERE 
			mm_aktif='Y' 
			&& mm_id in (
			SELECT
				ug_mm_id
			FROM
				user_grant
			WHERE
				ug_j_id in(
				SELECT
					utj_j_id
				FROM
					user_to_jabatan
				WHERE
					utj_user_id='$userid'		
			)
		ORDER BY mm_id ASC 
		)
	")or die(mysql_error());
	$sidebar_menu ='
						<ul class="sidebar-menu">
							<li class="header">MAIN NAVIGATION</li>
							<li class="treeview"><a href="?p='.paramEncrypt('home').'"><i class="fa fa-home"></i><span>Home</span></a></li>
							<li class="treeview"><a href="?p='.paramEncrypt('profile_form').'&id='.paramEncrypt($userid).'"><i class="fa fa-user"></i><span>Profile</span></a></li>
							<li class="treeview"><a href="?p='.paramEncrypt('logout').'"><i class="fa fa-power-off"></i><span>Log out</span></a></li>
	';
	while($r_m=mysql_fetch_array($q_m)){
		$sidebar_menu.='
							<li class="treeview"><a href="#"><i class="fa fa-'.$r_m["mm_ikon"].' text-'.$r_m["mm_warna_ikon"].'"></i><span>'.ucwords($r_m["mm_nama"]).'</span><i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
		';
		//pilih sub menu module
		$mm_id=$r_m['mm_id'];
		$q_sm=mysql_query("
			SELECT 
				*
			FROM 
				module
			WHERE 
				m_aktif='Y'
				&& m_mm_id='$mm_id'
			ORDER BY m_id ASC
		")or die(mysql_error());
		while($r_sm=mysql_fetch_array($q_sm)){
			$ref=$r_sm['m_file'];
			$sidebar_menu.='
								<li><a href="?p='.paramEncrypt($ref).'"><i class="fa fa-'.$r_sm["m_ikon"].' text-'.$r_sm["m_warna_ikon"].'"></i><span>'.ucwords($r_sm["m_nama"]).'</span></a>
			';		
		}
	
		$sidebar_menu.='
							</ul>
		';
	}
	$sidebar_menu.='
							<li class="treeview"><a href="?p='.paramEncrypt('information').'"><i class="fa fa-info"></i><span>Information</span></a></li>
							</li>
						</ul>
	';
	
	
	echo $sidebar_menu;
}

function cek_hak_akses($jabatan, $page){
	if ($page === 'home' || $page === 'information' || $page === 'profile_form' || $page === 'profile_change_foto' || $page === 'user_change_foto' || $page === 'log_penjualan' || $page === 'piutang_penjualan' || $page === 'log_retur_penjualan' || $page === 'retur_penjualan_form' || $page === 'struk_penjualan_print' || $page === 'struk_penjualan_pdf' || $page === 'lap_penjualan_pdf' || $page === 'log_retur_pembelian' || $page === 'retur_pembelian_form' || $page === 'utang_pembelian' || $page === 'lap_pembelian_pdf'  || $page === 'log_retur_penitipan' || $page === 'retur_penitipan_form' || $page === 'utang_penitipan' || $page === 'lap_penitipan_pdf' || $page === 'lap_pengeluaran_pdf' || $page === 'lap_keuangan_pdf' || $page === 'lap_laba_rugi_pdf') return TRUE;
	
	if($page === 'company_form' || $page === 'lap_penjualan' || $page === 'lap_pembelian' || $page === 'lap_penitipan' || $page === 'lap_pengeluaran' || $page === 'lap_keuangan' || $page === 'lap_laba_rugi') {
		$page = $page;
	} else {
		$pagex	= explode('_', $page);	
		$pagex	= $pagex[0];
		$pagex1	= $pagex[1];
		if($pagex1) {
			$page = $pagex; 		
		} else {
			$page = $pagex;
		}
	}
	
	$cari = mysql_query("
		SELECT 
			m_file 
		FROM 
			user_grant 
		INNER JOIN
			module_menu
		ON ug_mm_id = mm_id
		INNER JOIN
			module
		ON mm_id = m_mm_id
		WHERE 
			ug_j_id='$jabatan' 
			&& m_file='$page'
	");
	return (mysql_num_rows($cari))? TRUE : FALSE;
}

function cek_menu_akses($ug_j_id, $ug_mm_id){
	$cari = mysql_query("SELECT * FROM user_grant WHERE ug_j_id='$ug_j_id' && ug_mm_id='$ug_mm_id'");
	return (mysql_num_rows($cari))? TRUE : FALSE;
}

function list_menu_akses($j_id){
	$cari = mysql_query("
		SELECT 
			mm_nama 
		FROM 
			module_menu
		INNER JOIN
			user_grant 
		ON
			mm_id = ug_mm_id
		WHERE 
			ug_j_id='$j_id'
	");
	
	$result = '';
	if(mysql_num_rows($cari)){
		while($row = mysql_fetch_array($cari)){
			extract($row);
			$result .='- '.ucwords($mm_nama).'<br>';
		}
	}
	return $result;
	
}

function gen_pid(){
	$cari = mysql_query("SELECT max(p_kode) as kode FROM produk");
	$r    = mysql_fetch_assoc($cari);
	extract($r);

	$kode_akhir	= (int) substr($kode, 1, 4);
	$kode_akhir++;
	$kode = 'P'. sprintf('%04s',$kode_akhir);
	return $kode;
}

function gen_tmid(){
	$cari = mysql_query("SELECT max(keu_kode) as kode FROM keuangan WHERE keu_kode LIKE '%HC-TM%' ");
	$r    = mysql_fetch_assoc($cari);
	extract($r);

	$kode_akhir	= (int) substr($kode, 5, 7);
	$kode_akhir++;
	$kode = 'HC-'.'TM'. sprintf('%07s',$kode_akhir);
	return $kode;
}

function gen_kmid(){
	$cari = mysql_query("SELECT max(keu_kode) as kode FROM keuangan WHERE keu_kode LIKE '%HC-KM%' ");
	$r    = mysql_fetch_assoc($cari);
	extract($r);

	$kode_akhir	= (int) substr($kode, 5, 7);
	$kode_akhir++;
	$kode = 'HC-'.'KM'. sprintf('%07s',$kode_akhir);
	return $kode;
}

function gen_pjid(){
	$cari = mysql_query("SELECT max(pj_kode) as kode FROM penjualan");
	$r    = mysql_fetch_assoc($cari);
	extract($r);

	$kode_akhir	= (int) substr($kode, 5, 7);
	$kode_akhir++;
	$kode = 'HC-'.'PJ'. sprintf('%07s',$kode_akhir);
	return $kode;
}

function gen_rjid(){
	$cari = mysql_query("SELECT max(rpj_kode) as kode FROM retur_penjualan");
	$r    = mysql_fetch_assoc($cari);
	extract($r);

	$kode_akhir	= (int) substr($kode, 5, 7);
	$kode_akhir++;
	$kode = 'HC-'.'RJ'. sprintf('%07s',$kode_akhir);
	return $kode;
}

function gen_rbid(){
	$cari = mysql_query("SELECT max(rpb_kode) as kode FROM retur_pembelian");
	$r    = mysql_fetch_assoc($cari);
	extract($r);

	$kode_akhir	= (int) substr($kode, 5, 7);
	$kode_akhir++;
	$kode = 'HC-'.'RB'. sprintf('%07s',$kode_akhir);
	return $kode;
}

function gen_rnid(){
	$cari = mysql_query("SELECT max(rpn_kode) as kode FROM retur_penitipan");
	$r    = mysql_fetch_assoc($cari);
	extract($r);

	$kode_akhir	= (int) substr($kode, 5, 7);
	$kode_akhir++;
	$kode = 'HC-'.'RN'. sprintf('%07s',$kode_akhir);
	return $kode;
}

function gen_pbid(){
	$cari = mysql_query("SELECT max(pb_kode) as kode FROM pembelian");
	$r    = mysql_fetch_assoc($cari);
	extract($r);

	$kode_akhir	= (int) substr($kode, 5, 7);
	$kode_akhir++;
	$kode = 'HC-'.'PB'. sprintf('%07s',$kode_akhir);
	return $kode;
}

function gen_pnid(){
	$cari = mysql_query("SELECT max(pn_kode) as kode FROM penitipan");
	$r    = mysql_fetch_assoc($cari);
	extract($r);

	$kode_akhir	= (int) substr($kode, 5, 7);
	$kode_akhir++;
	$kode = 'HC-'.'PN'. sprintf('%07s',$kode_akhir);
	return $kode;
}

function gen_pgid(){
	$cari = mysql_query("SELECT max(pg_kode) as kode FROM pengeluaran");
	$r    = mysql_fetch_assoc($cari);
	extract($r);

	$kode_akhir	= (int) substr($kode, 5, 7);
	$kode_akhir++;
	$kode = 'HC-'.'PG'. sprintf('%07s',$kode_akhir);
	return $kode;
}

function get_fullname($id){
	if ($id==0) {
		$user_fullname = '';
	} else {
		$q=mysql_query("SELECT user_fullname FROM user WHERE user_id='$id'")or die(mysql_error());
		$r=mysql_fetch_assoc($q);
		extract($r);
	}
	return $user_fullname;
}

function opt_fullname($id){
	$q=mysql_query("
		SELECT 
			*
		FROM
			user
		WHERE 
			user_id !=1
	")or die(mysql_error());
	
	echo '<option value="0">-- Pilih --</option>';
	while($r=mysql_fetch_array($q)){
		extract($r);
		switch ($id) {
			case 0:
				echo '<option value="'.$user_id.'">'.ucwords($user_fullname).'</option>';
			break;
			
			default:
				if ($user_id == $id) {
					echo '<option value="'.$user_id.'" selected>'.ucwords($user_fullname).'</option>';
				} else {
					echo '<option value="'.$user_id.'">'.ucwords($user_fullname).'</option>';
				}
			break;
		}
	}
}

function get_menu($id){
	if ($id==0) {
		$mm_nama = '';
	} else {
		$q=mysql_query("SELECT mm_nama FROM module_menu WHERE mm_id='$id'")or die(mysql_error());
		$r=mysql_fetch_assoc($q);
		extract($r);
	}
	return $mm_nama;
}

function opt_menu($id){
	$q=mysql_query("
		SELECT 
			*
		FROM
			module_menu
		WHERE 
			mm_aktif='Y'
	")or die(mysql_error());
	
	echo '<option value="0">-- Pilih --</option>';
	while($r=mysql_fetch_array($q)){
		extract($r);
		switch ($id) {
			case 0:
				echo '<option value="'.$mm_id.'">'.ucwords($mm_nama).'</option>';
			break;
			
			default:
				if ($mm_id == $id) {
					echo '<option value="'.$mm_id.'" selected>'.ucwords($mm_nama).'</option>';
				} else {
					echo '<option value="'.$mm_id.'">'.ucwords($mm_nama).'</option>';
				}
			break;
		}
	}
}

function opt_category($id){
	$q=mysql_query("
		SELECT 
			*
		FROM
			kategori
		WHERE 
			k_aktif='Y'
	")or die(mysql_error());
	
	echo '<option value="0">-- Pilih --</option>';
	while($r=mysql_fetch_array($q)){
		extract($r);
		if ($k_id == $id) {
			echo '<option value="'.$k_id.'" selected>'.ucwords($k_nama).'</option>';
		} else {
			echo '<option value="'.$k_id.'">'.ucwords($k_nama).'</option>';
		}
	}
}

function get_category($id){
	$q=mysql_query("SELECT k_nama FROM kategori WHERE k_id='$id'")or die(mysql_error());
	if(!mysql_num_rows($q)){
		$k_nama = '';
	} else {
		$r=mysql_fetch_assoc($q);
		extract($r);
	}	
	return ucwords($k_nama);
}


function opt_category2(){
	$q=mysql_query("
		SELECT 
			*
		FROM
			kategori
		WHERE 
			k_aktif='Y'
	")or die(mysql_error());
	
	echo '<option value="">-- Pilih --</option>';
	echo '<option value="0">Semua Kategori</option>';
	while($r=mysql_fetch_array($q)){
		extract($r);			
		echo '<option value="'.$k_id.'">'.ucwords($k_nama).'</option>';
	}
}

function opt_category3(){
	$q=mysql_query("
		SELECT 
			*
		FROM
			kategori
		WHERE 
			k_aktif='Y'
	")or die(mysql_error());
	
	echo '<option value="0">-- Semua --</option>';
	while($r=mysql_fetch_array($q)){
		extract($r);			
		echo '<option value="'.$k_id.'">'.ucwords($k_nama).'</option>';
	}
}

function get_jabatan($id){
	if ($id==0) {
		$j_nama = '';
	} else {
		$q=mysql_query("SELECT j_nama FROM jabatan WHERE j_id='$id'")or die(mysql_error());
		$r=mysql_fetch_array($q);
		extract($r);
	}
	return $j_nama;
}

function opt_jabatan($jabatan, $id){
	if($jabatan == 1){
		$q=mysql_query("
			SELECT 
				*
			FROM
				jabatan
			WHERE 
				j_aktif='Y'
		")or die(mysql_error());
	} else {
		$q=mysql_query("
			SELECT 
				*
			FROM
				jabatan
			WHERE
				j_id != 1
				&& j_aktif='Y'
		")or die(mysql_error());
	} 
	
	echo '<option value="0">-- Pilih --</option>';
	while($r=mysql_fetch_array($q)){
		extract($r);
		switch ($id) {
			case 0:
				echo '<option value="'.$j_id.'">'.ucwords($j_nama).'</option>';
			break;
			
			default:
				if ($j_id == $id) {
					echo '<option value="'.$j_id.'" selected>'.ucwords($j_nama).'</option>';
				} else {
					echo '<option value="'.$j_id.'">'.ucwords($j_nama).'</option>';
				}
			break;
		}
	}
}

function opt_aktif($default){
	$ArrAktif=array(''=>'-- Pilih --', 'Y'=>'Ya', 'N'=>'Tidak');
	foreach($ArrAktif as $key=>$value){
		if($key==$default) echo '<option value="'.$key.'" selected>'.$value.'</option>';
		else echo '<option value="'.$key.'">'.$value.'</option>';
	}
}

function opt_jenis($default){
	$ArrJenis=array(''=>'-- Pilih --', 'CASH'=>'CASH', 'TITIPAN'=>'TITIPAN');
	foreach($ArrJenis as $key=>$value){
		if($key==$default) echo '<option value="'.$key.'" selected>'.$value.'</option>';
		else echo '<option value="'.$key.'">'.$value.'</option>';
	}
}

function opt_satuan($default){
	$ArrSatuan=array(''=>'-- Pilih --', 'PCS'=>'PCS', 'PCK'=>'PCK', 'KG'=>'KG', 'LUSIN'=>'LUSIN', 'KODI'=>'KODI');
	foreach($ArrSatuan as $key=>$value){
		if($key==$default) echo '<option value="'.$key.'" selected>'.$value.'</option>';
		else echo '<option value="'.$key.'">'.$value.'</option>';
	}
}

function opt_ppn($default){
	$ArrPPn=array(0=>'0', 5=>'5', 10=>'10', 15=>'15', 20=>'20');
	foreach($ArrPPn as $key=>$value){
		if($key==$default) echo '<option value="'.$key.'" selected>'.$value.'</option>';
		else echo '<option value="'.$key.'">'.$value.'</option>';
	}
}

function opt_diskon(){
	$ArrDiskon=array(0=>'0', 5=>'5', 10=>'10', 15=>'15', 20=>'20');
	foreach($ArrDiskon as $key=>$value){
		echo '<option value="'.$key.'">'.$value.'</option>';
	}
}

function error_404(){
	echo '
		<div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
              <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
              <p>
                We could not find the page you were looking for.
                Meanwhile, you may contact your Administrator.
              </p>
            </div><!-- /.error-content -->
        </div><!-- /.error-page -->
	';
}

function access_denied(){
	echo '
		<div class="error-page">
            <h2 class="headline text-yellow"> Access Denied</h2>
            <div class="error-content">
              <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not Grant.</h3>
              <p>
                Your request page not grant.
                Meanwhile, you may contact your Administrator.
              </p>
            </div><!-- /.error-content -->
        </div><!-- /.error-page -->
	';
}

function persen($number){
    $number = php_toNumber($number);
    return '( '. number_format($number). '%)';
}

function showdt($date,$format){
	if($date != ''){
		$bulannumber = array(10,11,12,1,2,3,4,5,6,7,8,9);
		$bulan = array('Oktober','November','Desember','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September');
		$bulan2 = array('Okt','Nov','Des','Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep');

		if( $date == 'now' ){ $date = date('Y').'-'.date('m').'-'.date('d'); }
		if( $format == 1 ){
			$parsedate = explode( '-',$date );
			$parsedate[0] = intval( $parsedate[0] );
			$parsedate[1] = intval( $parsedate[1] );
			$parsedate[2] = intval( $parsedate[2] );
			return $parsedate[2].' '.str_replace( $bulannumber,$bulan,$parsedate[1] ).' '.$parsedate[0];
		}

		if( $format == 2 ){
			$parsedate = explode( '-',$date );
			$parsedate[0] = intval( $parsedate[0] );
			$parsedate[1] = intval( $parsedate[1] );
			$parsedate[2] = intval( $parsedate[2] );
			return $parsedate[2].' '.str_replace( $bulannumber,$bulan2,$parsedate[1] ).' '.$parsedate[0];
		}
	}//if date != '';
}

function terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}

function kekata($x){
	$x = abs($x);
	$angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	if($x < 12){
		$temp = " ". $angka[$x];
	}else if($x < 20){
		$temp = kekata($x - 10)." belas";
	}else if($x < 100){
		$temp = kekata($x/10)." puluh". kekata($x % 10);
	}else if($x < 200){
		$temp = " seratus". kekata($x -100);
	}else if($x < 1000){
		$temp = kekata($x/100)." ratus". kekata($x % 100);
	}else if($x < 2000){
		$temp = " seribu".kekata($x - 100);
	}else if($x < 1000000){
		$temp = kekata($x / 1000)." ribu". kekata($x % 1000);
	}else if($x < 1000000000){
		$temp = kekata($x / 1000000)." juta".kekata($x % 1000000);
	}else if($x < 1000000000000){
		$temp = kekata($x / 1000000000)." milyar". kekata(fmod($x,1000000000));
	}else if($x < 1000000000000000){
		$temp = kekata($x/1000000000000)." trilyun". kekata(fmod($x,1000000000000));
	}
	return $temp;
}

function tkoma($x){
	$x		= stristr($x,'.');
	$angka 	= array("nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan");
	$temp	= " ";
	$pjg	= strlen($x);
	$pos	= 1;
	while($pos < $pjg){
		$char	= substr($x, $pos, 1);
		$pos++;
		$temp	.= " ".$angka[$char];
	}
	return $temp;
}

function terbilangfloat($x, $style=4){
	if($x<0){
		$hasil = "minus ". trim(kekata($x));
	}else{
		$point	= trim(tkoma($x));
		$dpnkoma = str_replace((stristr($x,'.')),'',$x);//depan koma
		if(intval($dpnkoma == 0)){
			$hasil 	= 'nol '. trim(kekata($x));
		}else{
			$hasil	= trim(kekata($x));
		}
	}
	switch($style){
		case 1:
			if($point){
				$hasil = strtoupper($hasil). ' KOMA '. strtoupper($point);
			}else{
				$hasil =strtoupper($hasil);
			}
			break;
		case 2:
			if($point){
				$hasil = strtolower($hasil). ' koma '. strtolower($point);
			}else{
				$hasil =strtolower($hasil);
			}
			break;
		case 3:
			if($point){
				$hasil = ucwords($hasil). ' Koma '. ucwords($point);
			}else{
				$hasil = ucwords($hasil);
			}
			break;
		default:
			if($point){
				$hasil = ucfirst($hasil).' koma '. $point;
			}else{
				$hasil = ucfirst($hasil);
			}
			break;
	}
	return $hasil;
}

//------------------
function left($str, $length) {
     return substr($str, 0, $length);
}

function right($str, $length) {
     return substr($str, -$length);
}

function php_toNumber($text){
	return floatval(preg_replace('/[^0-9\-\.]/','',$text));
}

function Mdisplay($Rp){
	if( $Rp > 0){
		if( $Rp <= 1000000000 ) $Rp = ($Rp/1000000).' Jt';
		if( $Rp > 1000000000 && $Rp < 1000000000000 ) $Rp = ($Rp/1000000000).' M';
	}else $Rp = 0;
	return $Rp;
}

function RoundUp($n) {//round 2 digit after comma
	return round(ceil($n * 1000)/1000,2);
}

function rupiah($number){
    $number = php_toNumber($number);
    return 'Rp '. number_format($number, 2, '.', ',');
}



?>