  <aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
	  <!-- Sidebar user panel -->
	  <div class="user-panel">
		<div class="pull-left image">
			<img src="images/users/<?php echo $foto;?>" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
		  <p><?php echo $username; ?><br>
          <center><small><?php echo get_jabatan($jabatan);?></small></center>
		  </p>
		</div>
	  </div>
	 
	  <!-- sidebar menu: : style can be found in sidebar.less -->
	  <?php sidebar_menu($userid);?>
	</section>
	<!-- /.sidebar -->
  </aside>