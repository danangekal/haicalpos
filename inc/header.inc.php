	<header class="main-header">

        <!-- Logo -->
        <a href="?p=<?php echo paramEncrypt('home');?>" class="logo" style="background-color: rgb(244,244,245);">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini" style="color: #000000;"><b>HS</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg" style="color: #000000;"><img src="images/dashboard-logo.jpg" alt="logo"/><b>HAICAL STORE</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          
		  <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="images/users/<?php echo $foto;?>" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $username; ?></span> &nbsp;
                  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="images/users/<?php echo $foto;?>" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $username; ?>
                      <small><?php echo get_jabatan($jabatan);?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="?p=<?php echo paramEncrypt('profile_form');?>&id=<?php echo paramEncrypt($userid);?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="?p=<?php echo paramEncrypt('logout');?>" class="btn btn-default btn-flat">Log out</a>
                    </div>
                  </li>
                </ul>
              </li>
			  
            </ul>
          </div>

        </nav>
      </header>