-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2016 at 04:16 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `haicaldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `c_id` tinyint(1) NOT NULL,
  `c_nama` varchar(100) NOT NULL,
  `c_alamat` text NOT NULL,
  `c_kontak` varchar(50) NOT NULL,
  `c_slogan` varchar(100) NOT NULL,
  `c_keuangan` decimal(30,0) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`c_id`, `c_nama`, `c_alamat`, `c_kontak`, `c_slogan`, `c_keuangan`) VALUES
(1, 'haical store', 'Jalan percobaan no 10 Kelurahan Coba Kecamatan Testing Bandung, Jawa Barat\nKode Pos 30213', 'Telp. 022388xx / Hp. 08976jdbfxx', 'oleh-oleh garut ya haikal store aja', '375500');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `j_id` int(11) NOT NULL,
  `j_nama` varchar(50) NOT NULL,
  `j_aktif` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`j_id`, `j_nama`, `j_aktif`) VALUES
(1, 'IT & Web Developer', 'Y'),
(2, 'Administrator', 'Y'),
(3, 'Owner', 'Y'),
(4, 'Cashier', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `k_id` int(11) NOT NULL,
  `k_nama` varchar(100) NOT NULL,
  `k_aktif` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`k_id`, `k_nama`, `k_aktif`) VALUES
(1, 'coklat', 'Y'),
(2, 'dodol', 'Y'),
(3, 'dorokdok', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `keuangan`
--

CREATE TABLE `keuangan` (
  `keu_id` bigint(20) NOT NULL,
  `keu_tanggal` date NOT NULL,
  `keu_kode` varchar(15) NOT NULL,
  `keu_transaksi` varchar(20) NOT NULL,
  `keu_mutasi_debet` decimal(30,0) NOT NULL,
  `keu_mutasi_kredit` decimal(30,0) NOT NULL,
  `keu_saldo` decimal(30,0) NOT NULL,
  `keu_keterangan` varchar(50) NOT NULL,
  `keu_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keuangan`
--

INSERT INTO `keuangan` (`keu_id`, `keu_tanggal`, `keu_kode`, `keu_transaksi`, `keu_mutasi_debet`, `keu_mutasi_kredit`, `keu_saldo`, `keu_keterangan`, `keu_user_id`) VALUES
(1, '2016-02-10', 'HC-TM0000001', 'Penambahan Modal', '0', '3000000', '3000000', 'Penambahan', 1),
(2, '2016-02-10', 'HC-KM0000001', 'Pengurangan Modal', '1000000', '0', '2000000', 'Pengurangan', 1),
(3, '2016-02-11', 'HC-PJ0000001', 'Penjualan', '0', '2000', '2002000', 'Penambahan', 1),
(4, '2016-02-14', 'HC-PJ0000002', 'Penjualan', '0', '20500', '2022500', 'Penambahan', 1),
(5, '2016-02-17', 'HC-PJ0000003', 'Penjualan', '0', '4000', '2026500', 'Penambahan', 1),
(6, '2016-02-17', 'HC-PJ0000004', 'Penjualan', '0', '10000', '2036500', 'Penambahan', 1),
(7, '2016-02-17', 'HC-PJ0000004', 'Penjualan', '0', '10000', '2046500', 'Penambahan', 1),
(8, '2016-02-17', 'HC-PJ0000004', 'Penjualan', '0', '10000', '2056500', 'Penambahan', 1),
(9, '2016-02-17', 'HC-PJ0000004', 'Penjualan', '0', '10000', '2066500', 'Penambahan', 1),
(10, '2016-02-17', 'HC-PJ0000004', 'Penjualan', '0', '10000', '2076500', 'Penambahan', 1),
(11, '2016-02-17', 'HC-PJ0000004', 'Penjualan', '0', '10000', '2086500', 'Penambahan', 1),
(12, '2016-02-18', 'HC-PJ0000005', 'Penjualan', '0', '2000', '2088500', 'Penambahan', 1),
(13, '2016-02-25', 'HC-PJ0000006', 'Penjualan', '0', '6000', '2094500', 'Penambahan', 1),
(14, '2016-02-25', 'HC-PJ0000007', 'Penjualan', '0', '10000', '2104500', 'Penambahan', 1),
(21, '2016-02-28', 'HC-PN0000001', 'Penitipan', '70000', '0', '2034500', 'Pengurangan', 1),
(22, '2016-02-28', 'HC-PN0000001', 'Penitipan', '54000', '0', '1980500', 'Pengurangan', 1),
(23, '2016-02-28', 'HC-PN0000002', 'Penitipan', '70000', '0', '1910500', 'Pengurangan', 1),
(24, '2016-02-28', 'HC-PN0000002', 'Penitipan', '144000', '0', '1766500', 'Pengurangan', 1),
(25, '2016-02-28', 'HC-PJ0000008', 'Penjualan', '0', '4000', '1770500', 'Penambahan', 1),
(26, '2016-03-09', 'HC-PB0000002', 'Pembelian', '100000', '0', '1670500', 'Pengurangan', 1),
(27, '2016-03-09', 'HC-PB0000003', 'Pembelian', '90000', '0', '1580500', 'Pengurangan', 1),
(28, '2016-03-09', 'HC-PB0000004', 'Pembelian', '130000', '0', '1450500', 'Pengurangan', 1),
(29, '2016-03-09', 'HC-PB0000001', 'Pembelian', '160000', '0', '1290500', 'Pengurangan (sisa bayar)', 1),
(30, '2016-03-09', 'HC-PB0000002', 'Pembelian', '190000', '0', '1100500', 'Pengurangan (sisa bayar)', 1),
(31, '2016-04-15', 'HC-PG0000001', 'Pengeluaran', '120000', '0', '980500', 'Pengurangan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `m_id` int(11) NOT NULL,
  `m_nama` varchar(50) NOT NULL,
  `m_folder` varchar(50) NOT NULL,
  `m_file` varchar(50) NOT NULL,
  `m_ikon` varchar(50) NOT NULL,
  `m_warna_ikon` varchar(50) NOT NULL,
  `m_aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  `m_mm_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`m_id`, `m_nama`, `m_folder`, `m_file`, `m_ikon`, `m_warna_ikon`, `m_aktif`, `m_mm_id`) VALUES
(1, 'menu', 'menu', 'menu', 'bars', 'red', 'Y', 1),
(2, 'module', 'module', 'module', 'clone', 'green', 'Y', 1),
(3, 'company', 'company_form', 'company_form', 'institution', 'red', 'Y', 2),
(4, 'jabatan', 'jabatan', 'jabatan', 'user-plus', 'green', 'Y', 2),
(5, 'kategori', 'kategori', 'kategori', 'bars', 'red', 'Y', 3),
(6, 'produk', 'produk', 'produk', 'cutlery', 'green', 'Y', 3),
(7, 'user', 'user', 'user', 'users', 'yellow', 'Y', 2),
(8, 'penjualan', 'penjualan', 'penjualan', 'shopping-cart', 'red', 'Y', 5),
(9, 'laporan penjualan', 'lap_penjualan', 'lap_penjualan', 'line-chart', 'red', 'Y', 4),
(10, 'pembelian', 'pembelian', 'pembelian', 'shopping-cart', 'green', 'Y', 5),
(11, 'laporan pembelian', 'lap_pembelian', 'lap_pembelian', 'line-chart', 'green', 'Y', 4),
(12, 'penitipan', 'penitipan', 'penitipan', 'shopping-cart', 'yellow', 'Y', 5),
(13, 'laporan penitipan', 'lap_penitipan', 'lap_penitipan', 'line-chart', 'yellow', 'Y', 4),
(14, 'pengeluaran', 'pengeluaran', 'pengeluaran', 'check-square', 'blue', 'Y', 5),
(15, 'laporan pengeluaran', 'lap_pengeluaran', 'lap_pengeluaran', 'line-chart', 'blue', 'Y', 4),
(16, 'laporan keuangan', 'lap_keuangan', 'lap_keuangan', 'balance-scale', 'purple', 'Y', 4),
(17, 'Laporan Laba Rugi', 'lap_laba_rugi', 'lap_laba_rugi', 'pie-chart', 'black', 'Y', 4);

-- --------------------------------------------------------

--
-- Table structure for table `module_menu`
--

CREATE TABLE `module_menu` (
  `mm_id` int(11) NOT NULL,
  `mm_nama` varchar(30) NOT NULL,
  `mm_ikon` varchar(50) NOT NULL,
  `mm_warna_ikon` varchar(50) NOT NULL,
  `mm_aktif` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_menu`
--

INSERT INTO `module_menu` (`mm_id`, `mm_nama`, `mm_ikon`, `mm_warna_ikon`, `mm_aktif`) VALUES
(1, 'pengaturan sistem', 'cog', '', 'Y'),
(2, 'pengaturan umum', 'cogs', '', 'Y'),
(3, 'data master', 'book', '', 'Y'),
(4, 'laporan', 'sticky-note', '', 'Y'),
(5, 'transaksi', 'cart-plus', '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `pb_kode` varchar(12) NOT NULL,
  `pb_tanggal` date NOT NULL,
  `pb_suplier` varchar(50) NOT NULL,
  `pb_nota` varchar(25) NOT NULL,
  `pb_jumlah` decimal(10,0) NOT NULL,
  `pb_persen_diskon` int(5) NOT NULL,
  `pb_diskon` decimal(10,0) NOT NULL,
  `pb_persen_ppn` int(5) NOT NULL,
  `pb_ppn` decimal(10,0) NOT NULL,
  `pb_total` decimal(10,0) NOT NULL,
  `pb_bayar` decimal(10,0) NOT NULL,
  `pb_sisa_bayar` decimal(10,0) NOT NULL,
  `pb_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'status 0: blm lunas, 1: lunas',
  `pb_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`pb_kode`, `pb_tanggal`, `pb_suplier`, `pb_nota`, `pb_jumlah`, `pb_persen_diskon`, `pb_diskon`, `pb_persen_ppn`, `pb_ppn`, `pb_total`, `pb_bayar`, `pb_sisa_bayar`, `pb_status`, `pb_user_id`) VALUES
('HC-PB0000001', '2016-03-09', 'tes', '001/tes/ex', '160000', 0, '0', 0, '0', '160000', '0', '160000', 1, 1),
('HC-PB0000002', '2016-03-09', 'Pendi', '001', '290000', 0, '0', 0, '0', '290000', '100000', '190000', 1, 1),
('HC-PB0000003', '2016-03-09', 'Tes', '002', '90000', 0, '0', 0, '0', '90000', '90000', '0', 1, 1),
('HC-PB0000004', '2016-03-09', 'tes', '003', '130000', 0, '0', 0, '0', '130000', '130000', '0', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `pbd_id` bigint(20) NOT NULL,
  `pbd_pb_kode` varchar(12) NOT NULL,
  `pbd_p_k_id` int(11) NOT NULL,
  `pbd_p_kode` varchar(5) NOT NULL,
  `pbd_p_nama` varchar(100) NOT NULL,
  `pbd_p_harga_beli` decimal(10,0) NOT NULL,
  `pbd_p_satuan` enum('PCS','PCK','KG','LUSIN','KODI') NOT NULL,
  `pbd_p_tgl_masuk` date NOT NULL,
  `pbd_p_tgl_expire` date NOT NULL,
  `pbd_beli` float(4,1) NOT NULL DEFAULT '0.0',
  `pbd_jumlah` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`pbd_id`, `pbd_pb_kode`, `pbd_p_k_id`, `pbd_p_kode`, `pbd_p_nama`, `pbd_p_harga_beli`, `pbd_p_satuan`, `pbd_p_tgl_masuk`, `pbd_p_tgl_expire`, `pbd_beli`, `pbd_jumlah`) VALUES
(1, 'HC-PB0000001', 2, 'P0003', 'Dodol Picnic', '40000', 'KG', '2016-02-10', '2016-10-03', 4.0, '160000'),
(2, 'HC-PB0000002', 2, 'P0003', 'Dodol Picnic', '40000', 'KG', '2016-03-09', '2016-10-03', 5.0, '200000'),
(3, 'HC-PB0000002', 1, 'P0001', 'Coklat Anti Galau', '18000', 'PCS', '2016-03-09', '2016-12-08', 5.0, '90000'),
(4, 'HC-PB0000003', 1, 'P0002', 'Coklat Ganteng', '18000', 'PCS', '2016-03-09', '2017-12-10', 5.0, '90000'),
(5, 'HC-PB0000004', 3, 'P0005', 'Dorokdok Nyentrik', '13000', 'PCK', '2016-03-09', '2016-09-08', 10.0, '130000');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

CREATE TABLE `pengeluaran` (
  `pg_kode` varchar(12) NOT NULL,
  `pg_tanggal` date NOT NULL,
  `pg_nama` varchar(50) NOT NULL,
  `pg_jenis` varchar(100) NOT NULL,
  `pg_jumlah` decimal(10,0) NOT NULL,
  `pg_keterangan` varchar(100) NOT NULL,
  `pg_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran`
--

INSERT INTO `pengeluaran` (`pg_kode`, `pg_tanggal`, `pg_nama`, `pg_jenis`, `pg_jumlah`, `pg_keterangan`, `pg_user_id`) VALUES
('HC-PG0000001', '2016-04-15', 'Dea', 'Biaya operasional listrik', '120000', 'biaya rutin listrik...', 1);

-- --------------------------------------------------------

--
-- Table structure for table `penitipan`
--

CREATE TABLE `penitipan` (
  `pn_kode` varchar(12) NOT NULL,
  `pn_tanggal` date NOT NULL,
  `pn_suplier` varchar(50) NOT NULL,
  `pn_nota` varchar(25) NOT NULL,
  `pn_status` tinyint(4) NOT NULL DEFAULT '0',
  `pn_total_bayar` decimal(10,0) NOT NULL,
  `pn_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penitipan`
--

INSERT INTO `penitipan` (`pn_kode`, `pn_tanggal`, `pn_suplier`, `pn_nota`, `pn_status`, `pn_total_bayar`, `pn_user_id`) VALUES
('HC-PN0000001', '2016-02-28', 'coba', '001/tex/ex', 1, '124000', 1),
('HC-PN0000002', '2016-02-28', 'Tes2', '002/tes2/ext', 1, '214000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `penitipan_detail`
--

CREATE TABLE `penitipan_detail` (
  `pnd_id` bigint(20) NOT NULL,
  `pnd_pn_kode` varchar(12) NOT NULL,
  `pnd_p_k_id` int(11) NOT NULL,
  `pnd_p_kode` varchar(5) NOT NULL,
  `pnd_p_nama` varchar(100) NOT NULL,
  `pnd_p_harga_beli` decimal(10,0) NOT NULL,
  `pnd_p_satuan` enum('PCS','PCK','KG','LUSIN','KODI') NOT NULL,
  `pnd_p_tgl_masuk` date NOT NULL,
  `pnd_p_tgl_expire` date NOT NULL,
  `pnd_titip` float(4,1) NOT NULL DEFAULT '0.0',
  `pnd_jumlah` decimal(10,0) NOT NULL,
  `pnd_terjual` float(4,1) NOT NULL,
  `pnd_bayar` decimal(10,0) NOT NULL,
  `pnd_sisa` float(4,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penitipan_detail`
--

INSERT INTO `penitipan_detail` (`pnd_id`, `pnd_pn_kode`, `pnd_p_k_id`, `pnd_p_kode`, `pnd_p_nama`, `pnd_p_harga_beli`, `pnd_p_satuan`, `pnd_p_tgl_masuk`, `pnd_p_tgl_expire`, `pnd_titip`, `pnd_jumlah`, `pnd_terjual`, `pnd_bayar`, `pnd_sisa`) VALUES
(1, 'HC-PN0000001', 3, 'P0006', 'Dorokdok Gaul', '17500', 'PCK', '2016-02-10', '2016-07-15', 10.0, '175000', 4.0, '70000', 6.0),
(2, 'HC-PN0000001', 3, 'P0007', 'Dorokdok Hejo', '18000', 'PCK', '2016-02-25', '2016-11-15', 10.0, '180000', 8.0, '144000', 2.0),
(3, 'HC-PN0000001', 3, 'P0008', 'Dorokdok Beureum', '18000', 'PCK', '2016-02-25', '2016-12-05', 10.0, '180000', 0.0, '0', 10.0),
(4, 'HC-PN0000002', 3, 'P0006', 'Dorokdok Gaul', '17500', 'PCK', '2016-02-28', '2016-07-15', 10.0, '175000', 0.0, '0', 0.0),
(5, 'HC-PN0000002', 3, 'P0007', 'Dorokdok Hejo', '18000', 'PCK', '2016-02-28', '2016-11-15', 10.0, '180000', 0.0, '0', 0.0),
(6, 'HC-PN0000002', 3, 'P0008', 'Dorokdok Beureum', '18000', 'PCK', '2016-02-28', '2016-12-05', 10.0, '180000', 0.0, '0', 0.0);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `pj_kode` varchar(12) NOT NULL,
  `pj_customer` varchar(25) NOT NULL DEFAULT 'Umum',
  `pj_customer_umum` tinyint(4) NOT NULL DEFAULT '1',
  `pj_tanggal` date NOT NULL,
  `pj_jumlah` decimal(10,0) NOT NULL,
  `pj_persen_diskon` int(5) NOT NULL,
  `pj_diskon` decimal(10,0) NOT NULL,
  `pj_persen_ppn` int(5) NOT NULL,
  `pj_ppn` decimal(10,0) NOT NULL,
  `pj_total` decimal(10,0) NOT NULL,
  `pj_bayar` decimal(10,0) NOT NULL,
  `pj_kembali` decimal(10,0) NOT NULL,
  `pj_profit` decimal(10,0) NOT NULL,
  `pj_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`pj_kode`, `pj_customer`, `pj_customer_umum`, `pj_tanggal`, `pj_jumlah`, `pj_persen_diskon`, `pj_diskon`, `pj_persen_ppn`, `pj_ppn`, `pj_total`, `pj_bayar`, `pj_kembali`, `pj_profit`, `pj_user_id`) VALUES
('HC-PJ0000001', 'Umum', 1, '2016-02-11', '20000', 0, '0', 0, '0', '20000', '20000', '0', '2000', 1),
('HC-PJ0000002', 'Umum', 1, '2016-02-14', '140000', 10, '14000', 10, '12600', '138600', '140000', '1400', '20500', 1),
('HC-PJ0000003', 'Umum', 1, '2016-02-17', '40000', 10, '4000', 10, '3600', '39600', '40000', '400', '4000', 1),
('HC-PJ0000004', 'Umum', 1, '2016-02-17', '50000', 0, '5000', 10, '4500', '49500', '50000', '500', '10000', 1),
('HC-PJ0000005', 'Umum', 1, '2016-02-18', '20000', 0, '0', 0, '0', '20000', '20000', '0', '2000', 1),
('HC-PJ0000006', 'Umum', 1, '2016-02-25', '60000', 0, '0', 0, '0', '60000', '100000', '40000', '6000', 1),
('HC-PJ0000007', 'Umum', 1, '2016-02-25', '80000', 0, '0', 0, '0', '80000', '100000', '20000', '10000', 1),
('HC-PJ0000008', 'Umum', 1, '2016-02-28', '40000', 0, '0', 0, '0', '40000', '50000', '10000', '4000', 1),
('HC-PJ0000009', 'Umum', 1, '2016-10-07', '50000', 0, '0', 0, '0', '50000', '0', '0', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `pjd_id` bigint(20) NOT NULL,
  `pjd_pj_kode` varchar(12) NOT NULL,
  `pjd_p_k_id` int(11) NOT NULL,
  `pjd_p_kode` varchar(5) NOT NULL,
  `pjd_p_nama` varchar(100) NOT NULL,
  `pjd_p_harga_jual` decimal(10,0) NOT NULL,
  `pjd_p_harga_beli` decimal(10,0) NOT NULL,
  `pjd_p_satuan` enum('PCS','PCK','KG','LUSIN','KODI') NOT NULL,
  `pjd_beli` float(4,1) NOT NULL DEFAULT '0.0',
  `pjd_jumlah` decimal(10,0) NOT NULL,
  `pjd_profit` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`pjd_id`, `pjd_pj_kode`, `pjd_p_k_id`, `pjd_p_kode`, `pjd_p_nama`, `pjd_p_harga_jual`, `pjd_p_harga_beli`, `pjd_p_satuan`, `pjd_beli`, `pjd_jumlah`, `pjd_profit`) VALUES
(1, 'HC-PJ0000001', 1, 'P0001', 'Coklat Anti Galau', '20000', '18000', 'PCS', 1.0, '20000', '2000'),
(2, 'HC-PJ0000002', 3, 'P0005', 'Dorokdok Nyentriksekali', '15000', '13000', 'PCK', 1.0, '15000', '2000'),
(3, 'HC-PJ0000002', 1, 'P0001', 'Coklat Anti Galau', '20000', '18000', 'PCS', 1.0, '20000', '2000'),
(4, 'HC-PJ0000002', 1, 'P0002', 'Coklat Ganteng', '20000', '18000', 'PCS', 1.0, '20000', '2000'),
(5, 'HC-PJ0000002', 2, 'P0004', 'Dodol Nyaah', '15000', '13000', 'KG', 1.0, '15000', '2000'),
(6, 'HC-PJ0000002', 2, 'P0003', 'Dodol Picnic', '50000', '40000', 'KG', 1.0, '50000', '10000'),
(7, 'HC-PJ0000002', 3, 'P0006', 'Dorokdok Gaul', '20000', '17500', 'PCK', 1.0, '20000', '2500'),
(8, 'HC-PJ0000003', 1, 'P0002', 'Coklat Ganteng', '20000', '18000', 'PCS', 2.0, '40000', '4000'),
(9, 'HC-PJ0000004', 2, 'P0003', 'Dodol Picnic', '50000', '40000', 'KG', 1.0, '50000', '10000'),
(10, 'HC-PJ0000005', 1, 'P0001', 'Coklat Anti Galau', '20000', '18000', 'PCS', 1.0, '20000', '2000'),
(11, 'HC-PJ0000006', 3, 'P0007', 'Dorokdok Hejo', '20000', '18000', 'PCK', 3.0, '60000', '6000'),
(12, 'HC-PJ0000007', 3, 'P0006', 'Dorokdok Gaul', '20000', '17500', 'PCK', 4.0, '80000', '10000'),
(13, 'HC-PJ0000008', 1, 'P0002', 'Coklat Ganteng', '20000', '18000', 'PCS', 1.0, '20000', '2000'),
(14, 'HC-PJ0000008', 1, 'P0001', 'Coklat Anti Galau', '20000', '18000', 'PCS', 1.0, '20000', '2000'),
(15, 'HC-PJ0000009', 2, 'P0003', 'Dodol Picnic', '50000', '40000', 'KG', 1.0, '50000', '10000');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `p_id` int(11) NOT NULL,
  `p_kode` varchar(5) NOT NULL,
  `p_nama` varchar(100) NOT NULL,
  `p_harga_jual` decimal(10,0) NOT NULL,
  `p_harga_beli` decimal(10,0) NOT NULL,
  `p_satuan` enum('PCS','PCK','KG','LUSIN','KODI') NOT NULL DEFAULT 'PCS',
  `p_stok` float(4,1) NOT NULL DEFAULT '0.0',
  `p_jenis` enum('CASH','TITIPAN') NOT NULL,
  `p_tgl_masuk` date NOT NULL,
  `p_tgl_expire` date NOT NULL,
  `p_k_id` int(11) NOT NULL,
  `p_aktif` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`p_id`, `p_kode`, `p_nama`, `p_harga_jual`, `p_harga_beli`, `p_satuan`, `p_stok`, `p_jenis`, `p_tgl_masuk`, `p_tgl_expire`, `p_k_id`, `p_aktif`) VALUES
(1, 'P0001', 'Coklat Anti Galau', '20000', '18000', 'PCS', 15.0, 'CASH', '2016-03-09', '2016-12-08', 1, 'Y'),
(2, 'P0002', 'Coklat Ganteng', '20000', '18000', 'PCS', 16.0, 'CASH', '2016-03-09', '2017-12-10', 1, 'Y'),
(3, 'P0003', 'Dodol Picnic', '50000', '40000', 'KG', 18.0, 'CASH', '2016-03-09', '2016-10-03', 2, 'Y'),
(4, 'P0004', 'Dodol Nyaah', '15000', '13000', 'KG', 14.0, 'CASH', '2016-02-01', '2016-07-10', 2, 'Y'),
(5, 'P0005', 'Dorokdok Nyentrik', '15000', '13000', 'PCK', 24.0, 'CASH', '2016-03-09', '2016-09-08', 3, 'Y'),
(6, 'P0006', 'Dorokdok Gaul', '20000', '17500', 'PCK', 0.0, 'TITIPAN', '2016-02-28', '2016-07-15', 3, 'Y'),
(7, 'P0007', 'Dorokdok Hejo', '20000', '18000', 'PCK', 0.0, 'TITIPAN', '2016-02-28', '2016-11-15', 3, 'Y'),
(8, 'P0008', 'Dorokdok Beureum', '20000', '18000', 'PCK', 0.0, 'TITIPAN', '2016-02-28', '2016-12-05', 3, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `retur_pembelian`
--

CREATE TABLE `retur_pembelian` (
  `rpb_kode` varchar(12) NOT NULL,
  `rpb_pb_kode` varchar(12) NOT NULL,
  `rpb_tanggal` date NOT NULL,
  `rpb_jumlah` decimal(10,0) NOT NULL,
  `rpb_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_pembelian_detail`
--

CREATE TABLE `retur_pembelian_detail` (
  `rpbd_id` bigint(20) NOT NULL,
  `rpbd_rpb_kode` varchar(12) NOT NULL,
  `rpbd_pbd_id` bigint(20) NOT NULL,
  `rpbd_p_k_id` int(11) NOT NULL,
  `rpbd_p_kode` varchar(5) NOT NULL,
  `rpbd_p_nama` varchar(100) NOT NULL,
  `rpbd_p_harga_jual` decimal(10,0) NOT NULL,
  `rpbd_p_harga_beli` decimal(10,0) NOT NULL,
  `rpbd_p_satuan` enum('PCS','PCK','KG','LUSIN','KODI') NOT NULL,
  `rpbd_beli` float(4,1) NOT NULL,
  `rpbd_retur` float(4,1) NOT NULL DEFAULT '0.0',
  `rpbd_jumlah` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_penitipan`
--

CREATE TABLE `retur_penitipan` (
  `rnj_kode` varchar(12) NOT NULL,
  `rpn_pn_kode` varchar(12) NOT NULL,
  `rpn_tanggal` date NOT NULL,
  `rpn_jumlah` decimal(10,0) NOT NULL,
  `rpn_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_penitipan_detail`
--

CREATE TABLE `retur_penitipan_detail` (
  `rpnd_id` bigint(20) NOT NULL,
  `rpnd_rpn_kode` varchar(12) NOT NULL,
  `rpnd_pnd_id` bigint(20) NOT NULL,
  `rpnd_p_k_id` int(11) NOT NULL,
  `rpnd_p_kode` varchar(5) NOT NULL,
  `rpnd_p_nama` varchar(100) NOT NULL,
  `rpnd_p_harga_jual` decimal(10,0) NOT NULL,
  `rpnd_p_harga_beli` decimal(10,0) NOT NULL,
  `rpnd_p_satuan` enum('PCS','PCK','KG','LUSIN','KODI') NOT NULL,
  `rpnd_titip` float(4,1) NOT NULL,
  `rpnd_terjual` float(4,1) NOT NULL,
  `rpnd_retur` float(4,1) NOT NULL DEFAULT '0.0',
  `rpjd_jumlah` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_penjualan`
--

CREATE TABLE `retur_penjualan` (
  `rpj_kode` varchar(12) NOT NULL,
  `rpj_pj_kode` varchar(12) NOT NULL,
  `rpj_tanggal` date NOT NULL,
  `rpj_jumlah` decimal(10,0) NOT NULL,
  `rpj_potongan` decimal(10,0) NOT NULL,
  `rpj_total` decimal(10,0) NOT NULL,
  `rpj_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_penjualan_detail`
--

CREATE TABLE `retur_penjualan_detail` (
  `rpjd_id` bigint(20) NOT NULL,
  `rpjd_rpj_kode` varchar(12) NOT NULL,
  `rpjd_pjd_id` bigint(20) NOT NULL,
  `rpjd_p_k_id` int(11) NOT NULL,
  `rpjd_p_kode` varchar(5) NOT NULL,
  `rpjd_p_nama` varchar(100) NOT NULL,
  `rpjd_p_harga_jual` decimal(10,0) NOT NULL,
  `rpjd_p_harga_beli` decimal(10,0) NOT NULL,
  `rpjd_p_satuan` enum('PCS','PCK','KG','LUSIN','KODI') NOT NULL,
  `rpjd_beli` float(4,1) NOT NULL,
  `rpjd_retur` float(4,1) NOT NULL DEFAULT '0.0',
  `rpjd_jumlah` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_password` varchar(64) NOT NULL,
  `user_fullname` varchar(100) NOT NULL,
  `user_foto` varchar(50) NOT NULL,
  `user_salt` varchar(3) NOT NULL,
  `user_aktif` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_password`, `user_fullname`, `user_foto`, `user_salt`, `user_aktif`) VALUES
(1, 'danang', 'cf2a162b91a809c2a889ebef11aa28a12bdb26d106c2ef1f4dfbd3a1b0239bb0', 'Danang Eko Alfianto', 'danang.jpg', 'cb7', 'Y'),
(2, 'admin', '8d78dbd85677118bc270e5d07dd4b7815388fca2dce8307674f5f2f1b4126d08', 'Administrator 1', 'noimage.jpg', '5e9', 'Y'),
(3, 'kasir', '9cfd48a791bee0046bf379d2f409d991fe72ffd75bbc8bb297afa369d199d406', 'Cashier 1', 'noimage.jpg', '074', 'Y'),
(4, 'owner', '2af70982d8b9fdaf2d8bc20b7f8b25d5a3a8502baf17472d48d732e0c6b93c27', 'Owner 1', 'noimage.jpg', '34d', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `user_grant`
--

CREATE TABLE `user_grant` (
  `ug_j_id` int(11) NOT NULL,
  `ug_mm_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_grant`
--

INSERT INTO `user_grant` (`ug_j_id`, `ug_mm_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 3),
(2, 4),
(2, 5),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(4, 3),
(4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `user_to_jabatan`
--

CREATE TABLE `user_to_jabatan` (
  `utj_user_id` int(11) NOT NULL,
  `utj_j_id` int(11) NOT NULL,
  `utj_note` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_to_jabatan`
--

INSERT INTO `user_to_jabatan` (`utj_user_id`, `utj_j_id`, `utj_note`) VALUES
(1, 1, 'Danang Eko Alfianto > IT Developer'),
(2, 2, 'Admin 1 > Administrator'),
(3, 4, 'Cashier 1 > Cashier'),
(4, 3, 'Owner 1 > Owner');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`j_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`k_id`);

--
-- Indexes for table `keuangan`
--
ALTER TABLE `keuangan`
  ADD PRIMARY KEY (`keu_id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `module_menu`
--
ALTER TABLE `module_menu`
  ADD PRIMARY KEY (`mm_id`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`pb_kode`) USING BTREE;

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`pbd_id`);

--
-- Indexes for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD PRIMARY KEY (`pg_kode`);

--
-- Indexes for table `penitipan`
--
ALTER TABLE `penitipan`
  ADD PRIMARY KEY (`pn_kode`) USING BTREE;

--
-- Indexes for table `penitipan_detail`
--
ALTER TABLE `penitipan_detail`
  ADD PRIMARY KEY (`pnd_id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`pj_kode`) USING BTREE;

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`pjd_id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `retur_pembelian`
--
ALTER TABLE `retur_pembelian`
  ADD PRIMARY KEY (`rpb_kode`) USING BTREE;

--
-- Indexes for table `retur_pembelian_detail`
--
ALTER TABLE `retur_pembelian_detail`
  ADD PRIMARY KEY (`rpbd_id`);

--
-- Indexes for table `retur_penitipan`
--
ALTER TABLE `retur_penitipan`
  ADD PRIMARY KEY (`rnj_kode`) USING BTREE;

--
-- Indexes for table `retur_penitipan_detail`
--
ALTER TABLE `retur_penitipan_detail`
  ADD PRIMARY KEY (`rpnd_id`);

--
-- Indexes for table `retur_penjualan`
--
ALTER TABLE `retur_penjualan`
  ADD PRIMARY KEY (`rpj_kode`) USING BTREE;

--
-- Indexes for table `retur_penjualan_detail`
--
ALTER TABLE `retur_penjualan_detail`
  ADD PRIMARY KEY (`rpjd_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `UNIQUE` (`user_name`);

--
-- Indexes for table `user_grant`
--
ALTER TABLE `user_grant`
  ADD UNIQUE KEY `ug_j_id` (`ug_j_id`,`ug_mm_id`) USING BTREE;

--
-- Indexes for table `user_to_jabatan`
--
ALTER TABLE `user_to_jabatan`
  ADD UNIQUE KEY `utj_user_id` (`utj_user_id`,`utj_j_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `c_id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `j_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `k_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `keuangan`
--
ALTER TABLE `keuangan`
  MODIFY `keu_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `module_menu`
--
ALTER TABLE `module_menu`
  MODIFY `mm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `pbd_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `penitipan_detail`
--
ALTER TABLE `penitipan_detail`
  MODIFY `pnd_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `pjd_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `retur_pembelian_detail`
--
ALTER TABLE `retur_pembelian_detail`
  MODIFY `rpbd_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `retur_penitipan_detail`
--
ALTER TABLE `retur_penitipan_detail`
  MODIFY `rpnd_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `retur_penjualan_detail`
--
ALTER TABLE `retur_penjualan_detail`
  MODIFY `rpjd_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
